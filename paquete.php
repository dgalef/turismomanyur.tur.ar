<?php
require_once "admin/php/config.php";

if(isset($_GET["slug"])) $slug = $_GET["slug"];
else die("Slug not found");

$packages_obj   = newClass("packages");
$packages_row   = $packages_obj->get_row(null, "slug", $slug);

// Chequear si existe y si está publicado
if(!$packages_row) die("Row not found");
if($packages_row->status != "published") die("Row not available");

$imgs_data = $packages_obj->get_imgs($packages_row->id, "srcset", "gallery");

///

$cats = $packages_obj->get_tags_by_art($packages_row->id, "destinations");
$cat_id = $cats[0]->id;

$more_packages_data = $packages_obj->get_arts_by_tag($cat_id, "destinations", $packages_row->id);

$destinations_obj = newClass("destinations");
$destination_row = $destinations_obj->get_row($cat_id);

?>

<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = $packages_row->title;
    $page_desc = truncateHtml(strip_tags($packages_row->text), 100, '...', $exact = false);
    require_once "inc/head.php"; 
    ?>
</head>

<body id="package-pg" data-page="package">

    <?php include("inc/header.php"); ?>

    <div class="grid-container">

        <div class="grid-x">
            <div class="large-8 xlarge-9 cell">
               
                <section id="package">
                   
                    <h1><?php echo $packages_row->title; ?></h1>
                    
                    <div class="package-slider-wrapper">
                        <div id="package-slider" class="swiper-container">
                            <div class="swiper-wrapper">
                                <?php
                                foreach($imgs_data as $i) {
                                    echo "<div class='swiper-slide'>".
                                            $i["thum"]
                                        ."</div>";
                                }
                                ?>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                            <div class="swiper-pagination"></div>
                        </div>
                        
                        <?php
                        if ($packages_row->legend || $packages_row->price) {
                            echo "<div class='featured'>
                                    <span class='legend'>$packages_row->legend</span>
                                    <span class='price'>$packages_row->price</span>
                                </div>"    ;
                        }
                        
                        ?>
                    </div>
                    
                    <h2><?php echo $packages_row->subtitle; ?></h2>
                    
                    <div class="package-description">
                        <?php echo $packages_row->text; ?>
                    </div>
                    
                    <div class="package-legals">
                        <?php echo $packages_row->legals; ?>
                    </div>
                    
                    <div class="package-form">
                        
                        <form class="" novalidate data-abide>
                            <div class="grid-x grid-padding-x">
                               
                                <div class="large-12 cell">
                                    <h2>Consultá por este paquete</h2>
                                </div>
                                
                                <div class="large-4 cell">
                                    <label>
                                        <input type="text" name="nombre" placeholder="Nombre" required>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-4 cell">
                                    <label>
                                        <input type="text" name="apellido" placeholder="Apellido" required>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-4 cell">
                                    <label>
                                        <input type="email" name="email" placeholder="Email" required>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-4 cell">
                                    <label>
                                        <input type="number" name="telefono" placeholder="Teléfono" required>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-4 cell">
                                    <label>
                                        <input type="text" name="ciudad" placeholder="Ciudad" required>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-12 cell">
                                    <label>
                                        <textarea name="mensaje" placeholder="Quiero recibir más información de este paquete" required></textarea>
                                        <span class="form-error">Campo requerido</span>
                                    </label>
                                </div>
                                <div class="large-12 cell text-right">
                                    <input type="hidden" name="paquete_nombre" value="<?php echo $packages_row->title . "(ID: $packages_row->id)"; ?>">
                                    <button type="submit" id="package-send" class="button">ENVIAR</button>
                                </div>
                                <div class="large-12 cell">
                                    <div class="response">
                                        
                                    </div>
                                </div>
                            </div>
                        </form>
                        
                    </div>
                    
                </section>
                
            </div>
            
            <div class="large-4 xlarge-3 cell">
                
                <div class="more-packages">
                
                    <div class="more-packages-title">
                        <h4>Otros paquetes</h4>
                    </div>
                        
                    <div class="packages-grid">
                        
                    <?php
                    foreach ( $more_packages_data as $p )
                        include "inc/package-item.php";
                    ?>
                    </div>
                
                    <a class="expanded hollow button" href='/destinos/<?php echo $destination_row->slug; ?>'>Ver todos</a>
                </div>
            </div>
        </div>
        
    </div>
    

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

</body>

</html>
