

<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = null;
    $page_desc = "";
    require_once "inc/head.php"; 
    ?>
</head>

<body id="info-pg" data-page="info">

    <?php include("inc/header.php"); ?>

    <div class="hero">
        <img src="/img/hero-iguazu.jpg">
    </div>

    <section id="packages">
        <div class="grid-container">

            <div class="grid-x grid-padding-x align-center">
                <div class="large-12 cell">

                    <div id="prmkWidget"></div>

                </div>
            </div>

        </div>
    </section>
    
    <?php require_once "inc/newsletter.php"; ?>

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

    <script
            type="text/javascript"
            id="prmkFinder"
            data-token="045df842c82a79a1215f0673c0010336"
            data-maindiv="#prmkWidget"
            data-onsearch=""
            data-onview=""
            src="https://1037224462.rsc.cdn77.org/js/dist/js/amichi-widget.js" >
    </script>
    
</body>

</html>
