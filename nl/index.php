<?php

require_once "../admin/php/config.php";

if(isset($_GET["slug"])) $slug = $_GET["slug"];
else die("Slug not found");

$nl_obj = newClass("newsletters");
$nl_row =  $nl_obj->get_row(null, "slug", $slug);

// Chequear si existe y si está publicado
if(!$nl_row) die("Row not found");
if($nl_row->status != "published") die("Row not available");

$img_row = $nl_obj->get_media($nl_row->id, "flyer", 1);

if ($img_row) {
    $img_el = "<img src='https://turismomanyur.tur.ar/media/images/medium/$img_row->file_name' width='700' alt='Turismo Manyur'>";
}
else {
    $img_el = "";
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
    <title><?php echo $nl_row->title; ?></title>
</head>
<body>
    <table cellspacing='0' cellpadding='0' border='0' style="width:100%;text-align:center;">
       <tr>
            <td>
                <table cellspacing='0' cellpadding='0' border='0' style="width:700px;text-align:center;margin: 0 auto;">
                    <!-- Header -->
                    <tr>
                        <td style="border-bottom:5px solid white;">
                            <table cellspacing='0' cellpadding='0' border='0' class='header' style="width:100%;text-align:center;background:#094a64;">
                                <tr>
                                    <td style="width:330px">
                                        <img src="https://turismomanyur.tur.ar/nl/img/turismo-manyur-logo.png" alt="Turismo Manyur" width="330" height="100">
                                    </td>
                                    <td>&nbsp;</td>
                                    <td style="width:60px">
                                        <a href="https://es-la.facebook.com/pages/category/Travel-Agency/Turismo-Manyur-366740603418566/"><img src="https://turismomanyur.tur.ar/nl/img/icon-fb.png" alt="Facebook" width="60" height="100" style="display:block;"></a>
                                    </td>
                                    <td style="width:60px">
                                        <a href="https://www.instagram.com/turismomanyurok/?hl=es"><img src="https://turismomanyur.tur.ar/nl/img/icon-ig.png" alt="Instagram" width="60" height="100" style="display:block;"></a>
                                    </td>
                                    <td style="width:60px">
                                        <a href="https://wa.me/5491150390861"><img src="https://turismomanyur.tur.ar/nl/img/icon-wapp.png" alt="WhatsApp" width="60" height="100" style="display:block;"></a>
                                    </td>
                                    <td style="width:8px">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Content -->
                    <?php if ($img_el) { ?>
                    <tr>
                        <td style="border-bottom:15px solid white;">
                            <table cellspacing='0' cellpadding='0' border='0' class='content' style="width:100%;text-align:center;">
                                <tr><td><?php echo $img_el; ?></td></tr>
                            </table>
                        </td>
                    </tr>
                    <?php } ?>
                    <!-- Prefooter -->
                    <tr>
                        <td style='border-bottom:2px solid white;'>
                            <table cellspacing='0' cellpadding='0' border='0' class='prefooter' style="width:100%;text-align:center;background:#094a64">
                                <tr><td style="height:20px;">&nbsp;</td></tr>
                                <tr><td style="text-align:center;"><img src="https://turismomanyur.tur.ar/nl/img/turismo-manyur-pie.png" width="270" height="60" alt="Turismo Manyur" style="display:block;margin:0 auto"></td>
                                </tr>
                                <tr><td style="height:20px;">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                    <!-- Footer -->
                    <tr>
                        <td>
                            <table cellspacing='0' cellpadding='0' border='0' class='footer' style="width:100%;text-align:center;background:#111111">
                                <tr><td style="height:20px;" colspan="8">&nbsp;</td></tr>
                                <tr><td style="width:20px">&nbsp;</td>
                                    <td style="width:50px"><img src="https://turismomanyur.tur.ar/nl/img/icon-mail.png" width="50" height="50" alt='Email' style="display:block;"></td>
                                    <td style="vertical-align:middle;padding-left:5px;padding-right:15px;text-align:left" align='left' valign='middle'><span style="font-family: 'Roboto', 'Helvetica', 'Helvetica Neue', Arial, sans-serif;font-size: 15px;line-height: 18px;color: #FFFFFF;"><a style='color:white !important; text-decoration:none !important'>info@turismomanyur.tur.ar</a></span></td>
                                    <td style="width:50px"><img src="https://turismomanyur.tur.ar/nl/img/icon-web.png" width="50" height="50" alt='Web' style="display:block;"></td>
                                    <td style="vertical-align:middle;padding-left:5px;padding-right:15px;text-align:left" align='left' valign='middle'><span style="font-family: 'Roboto', 'Helvetica', 'Helvetica Neue', Arial, sans-serif;font-size: 15px;line-height: 18px;color: #FFFFFF;"><a style='color:white !important; text-decoration:none !important' href='https://turismomanyur.tur.ar'>turismomanyur.tur.ar</a></span></td>
                                    <td style="width:50px"><img src="https://turismomanyur.tur.ar/nl/img/icon-tel.png" width="50" height="50" alt='Teléfono' style="display:block;"></td>
                                    <td style="vertical-align:middle;padding-left:5px;padding-right:15px;text-align:left" align='left' valign='middle'><span style="font-family: 'Roboto', 'Helvetica', 'Helvetica Neue', Arial, sans-serif;font-size: 15px;line-height: 18px;color: #FFFFFF;">11.5039.0861</span></td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr><td style="height:20px;" colspan="8">&nbsp;</td></tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
       </tr>
    </table>
</body>
</html>