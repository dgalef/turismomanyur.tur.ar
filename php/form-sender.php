<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'PHPMailer/src/Exception.php';
require 'PHPMailer/src/PHPMailer.php';
require 'PHPMailer/src/SMTP.php';


// Google reCaptcha secret key
$verifyCaptcha  = true;
$secretKey      = "6LcDqpMbAAAAAAMAP-XXspPYTalwqW4GOMVixV8G";
$resp           = array();
$cv_exists      = false;

if($verifyCaptcha) {
    
    if( !isset($_POST['token']) || !isset($_POST['action']) ) {

        $resp["success"] = 0;
        $resp["message"] = 'Recaptcha vacío.';

        echo json_encode($resp);
        exit;

    }
    
    $token = $_POST['token'];
    $action = $_POST['action'];

    if( function_exists('curl_version')  && (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')) {

        // Usar curl
        // (Tiene que estar instalado en el servidor y tener un SSL válido)
        $ch = curl_init();
        $data = array(
            'secret'    => $secretKey,
            'response'  => $token
        );
        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ]);
        $verifyResponse = curl_exec($ch);
        if (curl_errno ( $ch )) {
            $resp["success"] = 0;
            $resp["message"] = curl_error($ch);
            curl_close ( $ch );
            die($resp["message"]);
        }
        curl_close($ch);
    }

    else {
        //  Usar file_get_contents
        //  (tiene que estar habilitado allow_url_fopen)
        $verifyResponse = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$token");
    }

    $responseData = json_decode($verifyResponse, true);

    // verify the response
    if($responseData["success"] == '1' && 
       $responseData["action"]  == $action && 
       $responseData["score"]   >= 0.5) 
    {
        // valid submission
    } else {
        // spam submission
        $resp["success"] = 0;
        $resp["message"] = 'Robot verification failed, please try again / ' . print_r($responseData);

        echo json_encode($resp);
        exit;
    }
}




$mail = new PHPMailer(true);                 // Passing `true` enables exceptions

try {

    // Enable verbose debug output
    $mail->SMTPDebug = 0;

    // SMTP settings
    
    $mail->isSMTP();
    $mail->CharSet 	= 'UTF-8';
    $mail->Host = 'smtp.gmail.com';
    $mail->Port = 587;
    $mail->SMTPSecure = 'tls';
    $mail->SMTPAuth = true;
    $mail->Username = "webform@turismomanyur.com.ar";
    $mail->Password =  "nzmllxjxicquhnmw"; // (Clave de app). Clave de usuario es "W3bF0rm_";
    

    $mail->isHTML(true);
    
    // Mail admin
    $mail->setFrom('webform@turismomanyur.com.ar', 'Turismo Manyur');
    $mail->addAddress("info@turismomanyur.tur.ar", "Info");
    $mail->addReplyTo($_POST["email"]);
    //$mail->addCC('cc@example.com');
    $mail->addCC("alejandro@estudioestratega.com", "Test");
    if( isset($_FILES['cv']) ) 
        $mail->AddAttachment($_FILES['cv']['tmp_name'], $_FILES['cv']['name']);
    $mail->Subject = "Consulta realizada desde la web";
    $content = get_form_content();
    $mail->Body    = $content;
    $mail->AltBody = strip_tags($content);
    $mail->send();


    /* 
    // Enviar también email al usuario

    $mail->ClearAllRecipients();
    $mail->clearAttachments();
    $mail->addAddress($_POST["email"], $_POST["nombre"]);     // Add a recipient
    if($cv_exists) $mail->AddAttachment($_FILES['cv']['tmp_name'], $_FILES['cv']['name']);
    $mail->Subject = "Recibimos tu consulta";
    $content = get_form_content();
    $mail->Body    = $content;
    $mail->AltBody = strip_tags($content);
    $mail->send();
    */

    $resp["success"] = 1;
    $resp["message"] = "¡Gracias por tu contacto!";
    echo json_encode($resp);
}

catch (Exception $e) {
    //echo 'Message could not be sent. Mailer Error: ', $mail->ErrorInfo;
    $resp["success"] = 0;
    $resp["message"] = 'El formulario no pudo ser enviado. Error: ' . $mail->ErrorInfo;
    echo json_encode($resp);
}


/********************/


function get_form_content() {

    unset($_POST["token"]);
    unset($_POST["action"]);

    $contenido = "";

    foreach($_POST as $k => $v) {
        if (is_array($v)) {
            $contenido .= "<p><b>$k:</b><br>";
            $contenido .= implode(", ", $v);
            $contenido .= "</p>";

        } else
            $contenido .= "<p><b>$k:</b><br>$v</p>";

    }

    return $contenido;
}

?>
