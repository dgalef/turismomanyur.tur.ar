<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = "Contacto";
    $page_desc = "";
    require_once "inc/head.php"; 
    ?>
</head>

<body id="contacto-pg" data-page="contacto">

    <?php include("inc/header.php"); ?>

    <div class="hero">
        <img src="/img/hero-iguazu.jpg">
    </div>

    <section id="packages">
        <div class="grid-container">

            <div class="grid-x grid-padding-x align-center">
                <div class="large-12 cell">
                        
                    <h2 class="underline">
                        Contacto
                    </h2>    

                </div>
            </div>                    
                  
            <div class="grid-x grid-padding-x align-center">
                <div class="medium-5 cell">  
                    <div class="contact-info">
                        <p class="tel">
                            <a href="tel:+5491150390861" target="_blank">11.5039.0861</a>
                        </p>
                        <p class="mail">
                            <a href="mailto:info@turismomanyur.tur.ar" target="_blank">info@turismomanyur.tur.ar</a>
                        </p>
                    </div>
                </div>
                <div class="medium-7 cell">  
                    <form class="" novalidate data-abide>
                        <div class="grid-x grid-padding-x">

                            <div class="medium-6 large-4 cell">
                                <label>
                                    <input type="text" name="nombre" placeholder="Nombre" required>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="medium-6 large-4 cell">
                                <label>
                                    <input type="text" name="apellido" placeholder="Apellido" required>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="medium-6 large-4 cell">
                                <label>
                                    <input type="email" name="email" placeholder="Email" required>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="medium-6 large-4 cell">
                                <label>
                                    <input type="number" name="telefono" placeholder="Teléfono" required>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="medium-6 large-4 cell">
                                <label>
                                    <input type="text" name="ciudad" placeholder="Ciudad" required>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="large-12 cell">
                                <label>
                                    <textarea name="mensaje" placeholder="Quiero recibir información sobre..." required></textarea>
                                    <span class="form-error">Campo requerido</span>
                                </label>
                            </div>
                            <div class="large-12 cell text-right">
                                <button type="submit" id="package-send" class="button">ENVIAR</button>
                            </div>
                            <div class="large-12 cell">
                                <div class="response">

                                </div>
                            </div>
                        </div>
                    </form>

                    
                </div>
            </div>

        </div>
    </section>
    
    <?php require_once "inc/newsletter.php"; ?>

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

</body>

</html>
