<?php
require_once "admin/php/config.php";

if(isset($_GET["slug"])) $slug = $_GET["slug"];
else die("Slug not found");

$static_pages_obj   = newClass("static_pages");
$static_pages_row   = $static_pages_obj->get_row(null, "slug", $slug);

// Chequear si existe y si está publicado
if(!$static_pages_row) die("Row not found");
if($static_pages_row->status != "published") die("Row not available");

$img_el = $static_pages_obj->get_img($static_pages_row->id, "srcset", "banner", true);

?>

<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = $static_pages_row->title;
    $page_desc = "";
    require_once "inc/head.php"; 
    ?>
</head>

<body id="info-pg" data-page="info">

    <?php include("inc/header.php"); ?>

    <div class="hero">
        <?php echo $img_el; ?> 
    </div>

    <section id="packages">
        <div class="grid-container">

            <div class="grid-x grid-padding-x align-center">
                <div class="large-10 cell">
                   
                    <div class="info-content">
                        
                        <h2 class="underline">
                            <?php echo $static_pages_row->title; ?>
                        </h2>    



                        <?php
                        echo $static_pages_row->text
                        ?>
                    </div>
                    
                </div>
            </div>

        </div>
    </section>
    
    <?php require_once "inc/newsletter.php"; ?>

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

</body>

</html>
