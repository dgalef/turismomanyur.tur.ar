<?php

session_start();
require_once "php/config.php";

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="home-pg">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <h1><?php echo _("Hola"); ?></h1>

                    </div>

                    <p>
                        <?php echo _("Este es un panel de administración creado especialmente para gestionar los contenidos de tu sitio web."); ?>
                    </p>

                </div>
            </div>
        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->

</body>

</html>
