<aside id="aside" data-toggler=".expanded">

    <div class="aside-header">
        <a class="menu-toggler" data-toggle="aside"><i class="fal fa-chevron-down"></i></a>
        <div class="logo"><?php echo $adminTitle; ?></div>
    </div>
    <nav>

        <ul class="main-menu vertical menu accordion-menu" data-submenu-toggle="true">
            <li><a href="."><i class="far fa-home fa-fw"></i> <?php echo _("INICIO"); ?></a></li>
            <li><a href="packages"><i class="far fa-bookmark fa-fw"></i> <?php echo _("PAQUETES"); ?></a></li>
            <li><a href="destinations"><i class="far fa-tag fa-fw"></i> <?php echo _("DESTINOS"); ?></a></li>
            <li><a href="slider"><i class="far fa-images fa-fw"></i> <?php echo _("SLIDER"); ?></a></li>
            <li><a href="static_pages"><i class="far fa-info fa-fw"></i> <?php echo _("PÁGINAS ESTÁTICAS"); ?></a></li>
            <li><a href="newsletters"><i class="far fa-newspaper fa-fw"></i> <?php echo _("NEWSLETTERS"); ?></a></li>
            
            
            <?php if ($adminuser->id == 1) { ?>
            <li><a href="admin_users"><i class="far fa-user fa-fw"></i> <?php echo _("ADMIN USERS"); ?></a>
                <ul class="vertical menu">
                    <li><a href="admin_rols.php"><i class="far fa-fw"></i> <?php echo _("Admin Rols"); ?></a></li>
                </ul>
            </li>
            <?php } ?>
        </ul>

    </nav>

    <div class="copyright">
        <?php
        $pathinfo = pathinfo($_SERVER['REQUEST_URI']);
        $f = substr($pathinfo['basename'], 0, strpos($pathinfo['basename'], "?"));

        if (sizeof($available_langs) > 1) {
            if ($lang == "es")
                echo "<p><a href='$f?lang=en'>English version</a></p>";
            else
                echo "<p><a href='$f?lang=es'>Versión en español</a></p>";
        }
        echo "<p>$adminVersion<br>
             $adminCopy</p>";
        ?>
    </div>
</aside>
