<div class="topbar">
    <div class="top-opts">
        <ul class="">
            <li class="user">
                <a><strong><?php echo sprintf(_("Hola %s"), $adminuser->first_name) ?></strong> <i class="hide fal fa-user"></i></a>
            </li>
            <li class="exit">
                <a href="exit" title="<?php echo _("Salir") ?>"><i class="fal fa-sign-out-alt"></i></a>
            </li>
        </ul>
    </div>
</div>
