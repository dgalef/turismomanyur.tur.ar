<footer>

</footer>

<script src="js/vendor/jquery.min.js"></script>
<script src="js/vendor/what-input.min.js"></script>
<script src="js/vendor/foundation.min.js"></script>

<script src="js/jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<script src="js/multisortable/jquery.multisortable.js"></script>

<!-- Plupload -->
<!-- debug -->
<!-- <script src="js/plupload/moxie.js"></script>
<script src="js/plupload/plupload.dev.js"></script> -->

<script src="js/plupload/plupload.full.min.js"></script>

<script src="js/plupload/jquery.ui.plupload/jquery.ui.plupload.js"></script>
<script src="js/plupload/i18n/es.js"></script>
<script src="js/plupload/custom.plupload.js"></script>

<!-- TinyMCE -->
<script src="js/tinymce/tinymce.min.js"></script>

<!-- MagnificPopup -->
<script src="js/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Select2 -->
<script src="js/select2/js/select2.min.js"></script>

<!-- Touch support for jQuery UI-->
<script src="js/jquery.touchpunch/jquery.ui.touch-punch.js"></script>

<!-- Gsap -->
<script src="js/gsap/TweenMax.min.js"></script>
<script src="js/gsap/plugins/ScrollToPlugin.min.js"></script>

<!-- Lightpick -->
<script src="js/moment/moment.min.js"></script>
<script src="js/lightpick/lightpick.js"></script>

<script src="js/app.js"></script>

<script src="js/select2/js/select2-init.js"></script>
<script src="js/plupload/plupload-init.js"></script>

