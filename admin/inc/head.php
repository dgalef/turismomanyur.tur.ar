<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<title><?php echo _("TURISMO MANYUR - My Admin Panel"); ?></title>

<link rel="shortcut icon" href="img/favicon.png" type="image/x-icon">

<link rel="stylesheet" href="js/jquery-ui-1.12.1.custom/jquery-ui.min.css">

<!-- Plupload -->
<link rel="stylesheet" href="js/plupload/jquery.ui.plupload/css/jquery.ui.plupload.css">

<!-- Select2 -->
<link rel="stylesheet" href="js/select2/css/select2.min.css">

<!-- Lightpick -->
<link rel="stylesheet" href="js/lightpick/lightpick.css">

<!-- MagnificPopup -->
<link rel="stylesheet" href="js/magnific-popup/magnific-popup.css">

<link rel="stylesheet" href="css/app.css">

<!-- Project custom CSS
<link rel="stylesheet" href="css/custom.css">
-->
