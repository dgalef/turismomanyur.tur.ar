<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "users";
$page_title             = _("usuario");

$show_back_button       = true;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");

$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    // If there's no ID, a new record is created;
    $result = $obj->create_row(array());

    if($result['success']) {
        $id = $obj->lastAffected;
        $new_location = basename($_SERVER['REQUEST_URI']);
        if($parent_id) $new_location .= "&id=" . $id;
        else $new_location .= "?id=" . $id;
        header("Location: " . $new_location);
    } else {
        die($result['error']);
    }
}

$data 		            = $obj->get_row($id);

$page_title             = ($data->status == 'temp') ? sprintf(_("Agregar %s"), $page_title) : sprintf(_(" Editar %s"), $page_title);

if (!$data) die("No existe el ID");

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Volver al listado de registros
$volver_url   = $class_name;
if($parent_id) $volver_url .= "?parent_id=$parent_id";

$countries_obj = newClass("paises");
$countries_data = $countries_obj->get_data();

$categories_obj = newClass("categories");
$categories_data = $categories_obj->get_data();

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                       
                        <fieldset>
                            <h3><?php echo _("Datos de acceso"); ?></h3>
                            <div class="form-row full">

                                <div class="form-field">
                                    <label><?php echo _("Email"); ?>
                                        <input type="email" class="is-unique" name="email" value="<?php echo escape($data->email); ?>" required>
                                        <span class="form-error"><?php echo _("Ingresar un email válido"); ?></span>
                                    </label>
                                    <p class="help-text"><?php echo _("Será el nombre de usuario para acceder al sistema."); ?></p>
                                </div>

                                <div class="form-field">
                                    <label><?php echo _("Contraseña"); ?>
                                        <input type="text" id="password" name="password" pattern="no_spaces" <?php if($data->status == "temp") echo "required"; ?>>
                                        <span class="form-error"><?php echo _("No se permiten espacios"); ?></span>
                                    </label>
                                    <p class="help-text">
                                        <?php 
                                        if($data->status == "temp") 
                                            echo _("Elegir una contraseña"); 
                                        else
                                            echo _("Dejar en blanco si no se desea cambiar la contraseña"); 
                                        ?>
                                    </p>
                                </div>
                                
                            </div>
                        </fieldset>
                        
                        <fieldset>
                            <h3><?php echo _("Datos personales"); ?></h3>
                            <div class="form-row full">
                                <div class="form-field">
                                    <label><?php echo _("Nombre"); ?>
                                        <input type="text" class="" name="nombre" value="<?php echo escape($data->nombre); ?>" required>
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>
                            
                                <div class="form-field">
                                    <label><?php echo _("Apellido"); ?>
                                        <input type="text" class="" name="apellido" value="<?php echo escape($data->apellido); ?>" required>
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>
                            
                            </div>
                            
                            <div class="form-row">
                                <div class="form-field">
                                    <label><?php echo _("Teléfono"); ?>
                                        <input type="text" class="" name="telefono" value="<?php echo escape($data->telefono); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>
                            </div>
                            
                            <div class="form-row">

                                <label><?php echo _("Documento de identidad"); ?></label>

                                <div class="form-field">
                                    <label><?php echo _("Tipo"); ?>
                                        <select name="documento_tipo" class="" required>
                                            <option value=""><?php echo _("Tipo"); ?></option>
                                            <?php
                                            foreach ( $obj->document_type as $k => $v ) {
                                                $t = $v["es"];
                                                $sel = ($t == $data->documento_tipo) ? "selected" : "";
                                                echo "<option value='$t' $sel>$t</option>";
                                            }
                                            ?>
                                        </select>
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>

                                <div class="form-field">
                                    <label><?php echo _("Número"); ?>
                                        <input type="text" class="" name="documento_numero" value="<?php echo escape($data->documento_numero); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>

                            </div>    
                            
                            <div class="form-row full">
                                <div class="form-field">
                                    <label><?php echo _("País"); ?>
                                        <select name="pais">
                                            <option value=""><?php echo _("Seleccionar"); ?></option>
                                            <?php
                                            foreach ( $countries_data as $country ) {
                                                $countries_obj->current_row = $country;
                                                $sel = ($country->nombre == $data->pais) ? "selected" : "";
                                                echo "<option value='$country->nombre' $sel>$country->nombre</option>";
                                            }
                                            ?>
                                        </select>
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>

                                <div class="form-field">
                                    <label><?php echo _("Provincia"); ?>
                                        <input type="text" class="" name="provincia" value="<?php echo escape($data->provincia); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>

                                <div class="form-field">
                                    <label><?php echo _("Ciudad"); ?>
                                        <input type="text" class="" name="ciudad" value="<?php echo escape($data->ciudad); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                </div>
                            </div>
                               
                        </fieldset>
                        
                        <fieldset>
                            <h3><?php echo _("Información adicional"); ?></h3>
                            <div class="form-field">
                                <label><?php echo _("Notas"); ?>
                                    <textarea class="" name="text"><?php echo $data->text; ?></textarea>
                                </label>
                            </div>
                        </fieldset>
                        
                        <!-- ---------->

                        <?php if($parent_id) echo "<input type='hidden' name='parent_id' value='$parent_id'>"; ?>
                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <a class="button hollow" href="<?php echo $volver_url; ?>"><i class="far fa-arrow-left"></i><?php echo _("Volver"); ?></a>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <div class="bottom-block">
                                    <label class="publicar-label" for=""><?php echo _("Habilitar:"); ?></label>
                                    <?php 
                                    
                                    $status = ($data->status == 'temp') ? "draft" : $data->status;
                                    ?>
                                    <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                                        <input type="hidden" name="status" class="chk-status" value="<?php echo $status; ?>">
                                        <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                        <label for="<?php echo "status-$id"; ?>">
                                            <div class="can-toggle__switch" data-checked="\f00c" data-unchecked="NO"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <button class="button" type="submit"><?php echo _("Guardar"); ?></button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                           <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>"; ?>
                            <a class="popup-btn-otro button hollow" href="<?php echo $add_file; ?>">
                                <?php echo _("Añadir otro"); ?>
                            </a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>">
                                <?php echo _("Aceptar"); ?>
                            </a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()">
                                <?php echo _("Cerrar"); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
