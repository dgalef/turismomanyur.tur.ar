<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once ("php/config.php");

$step = null;

if(isset($_GET['step'])) {
    
    $step = $_GET['step'];

    // Old data info
    $db_old_name        = $_GET['db_old_name'];
    $old_imgs_table     = $_GET['old_imgs_table'];
    $old_parent_col     = $_GET['old_parent_col'];
    $old_title_col      = ($_GET['old_title_col'] != '') ? $_GET['old_title_col'] : null; 
    $old_imgs_order     = $_GET['old_imgs_order']; 
    $src_folder         = $_GET['src_folder'];
    $limit_desde        = $_GET['limit_desde'];
    $limit_hasta        = $_GET['limit_hasta'];   

    // New data info
    $parent_class       = $_GET['parent_class'];
    $label              = $_GET['label'];
    $file_type          = $_GET['file_type'];
    
    $dest_obj           = newClass($parent_class);
    $img_sizes          = $dest_obj->images_sizes;
    
    // Old DB Connection
    $driver_options = array( PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" );
    $db_old = new PDO("mysql:host=localhost;dbname=$db_old_name", $db_user, $db_pass, $driver_options);
    $db_old->exec("SET NAMES 'utf8'; SET lc_messages = 'es_ES'");
    
    $old_imgs_obj           = newClass($old_imgs_table, $db_old);
    $old_imgs_obj->order    =  "ORDER BY id ASC";
    
    $old_imgs               = $old_imgs_obj->get_data("all");
    
    $old_imgs_obj->limit    = "LIMIT $limit_desde, $limit_hasta";
    $old_imgs_range               = $old_imgs_obj->get_data("all");
    
    $regs_total = sizeof($old_imgs);
    $regs_range = sizeof($old_imgs_range);

    $files_inexistent   = array();
    
    foreach($old_imgs_range as $o) {

        $img_src_file = $src_folder . $o->img_uni_name;

        if ( !file_exists($img_src_file) ) {

            array_push($files_inexistent, array("id"=>$o->id, "name" => $o->img_uni_name));

        } 
    }
}


//

if($step == 'confirm') {

    
    
    $folder		            = str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . "/media/src");
    if (!file_exists($folder)) mkdir($folder, 0777, true);

    $files_inexistent   = array();
    $files_moved        = array();
    $db_log_ok             = array();
    $db_log_error             = array();

    foreach($old_imgs_range as $o) {

        $img_src_file = $src_folder . $o->img_uni_name;

        if ( !file_exists($img_src_file) ) {

            array_push($files_inexistent, array("id"=>$o->id, "name" => $o->img_uni_name));

        } 

        else {

            // Movemos el archivo a la nueva ubicación 'src'
            $newFilePath 		= $folder . '/' . $o->img_uni_name;
            $moveit = copy($img_src_file, $newFilePath);
            if($moveit) array_push($files_moved, array("id"=>$o->id, "name" => $o->img_uni_name));

            // Si es una imagen, crear diferentes instancias
            if($file_type == "images") {

                //$parent_obj = newClass($parent_class);
                //$img_sizes = $parent_obj->images_sizes;

                // Image sizes
                $imgsize        = getimagesize($newFilePath);
                $file_size_arr  = array();
                $file_size_arr["src"] = $imgsize[0] . "x" . $imgsize[1];

                $imag = new image();

                foreach($img_sizes as $size => $params) {

                    if((!$params['max_width'] || !$params['max_height']) && $params['best_fit']) {
                        $resp = array("success" => 0, "message" => "ERROR in '$parent_class'' class image settings: 'best_fit' requires 'max_width' & 'max_height' not null");
                        die(json_encode($resp));
                    }

                    $imag->createImage($newFilePath, $size, $params);

                    $file_size_arr[$size] = $imag->width . "x" . $imag->height;

                }
            }

            $file_size = isset($file_size_arr) ? json_encode($file_size_arr) : NULL;
            $file_ext = substr($o->img_uni_name, strpos($o->img_uni_name, ".")+1);

            $new_title = ($old_title_col) ? $o->$old_title_col : "";

            // Crear registro en la DB
            $content_obj = new media();

            $arr			= array(
                'parent_class'      => $parent_class,
                'parent_id'         => $o->$old_parent_col,
                'file_name'         => $o->img_uni_name,
                'file_source_name'  => $o->img_src_name,
                'file_ext'          => $file_ext,
                'file_type'         => $file_type,
                'file_sizes'        => $file_size,
                'media_title'       => $new_title,
                'media_description' => '',
                'label'             => $label
            );

            $bool 			= $content_obj->create_row($arr);

            $resp = array();

            if($bool["success"]) {
                array_push($db_log_ok, array("id"=>$o->id, "name" => $o->img_uni_name, "error" => ""));
                
            } else {
                array_push($db_log_error, array("id"=>$o->id, "name" => $o->img_uni_name, "error" => $bool["error"]));
                
            }
            
        }
    }

}


?>

<!doctype html>
<html class="no-js" lang="es">

    <head>
        <?php require_once "inc/head.php"; ?>
    </head>

    <body class="home-pg">

        <?php require_once "inc/aside.php"; ?>

        <div class="main-wrapper">

            <?php require_once "inc/topbar.php"; ?>

            <main>

                <div class="grid-x grid-padding-x">
                    <div class="medium-12 cell">

                        <div class="main-header">

                            <h1>Herramienta para migrar imágenes</h1>
                            
                        </div>
                           
                        <div  style="font-size:14px" style='margin-bottom:20px'>
                            
                            <p>Para poder usar esta herramienta es necesario primero tener:</p>
                            <ul>
                                <li>Una copia funciona de la base de datos vieja.</li>
                                <li>Una copia de la carpeta que contenga las fotos originales.</li>
                                <li>La clase destino creada (o la calse 'main') con <i>$images_sizes</i> definidos.</li>
                            </ul>
                            
                            <p>La herramienta buscará los registros en la base de datos de origen, dentro de la tabla especificada y luego copiará las fotos de la carpeta de origen hacia la carpeta de destino, creando todas las instancias de las fotos que se requiera.</p>
                            
                            <p>&nbsp;</p>
                        </div>
                           
                        <div>
                            
                            <?php
                            
                            if($step == "review") {
                                
                                if(sizeof($files_inexistent) > 0) {
                                    $n = sizeof($files_inexistent);
                                    echo "<div class='callout alert'>
                                        <h4>ATENCIÓN</h4>
                                        <p>Hay $n archivos faltantes en:<br><i>$src_folder</i></p>";
                                    foreach($files_inexistent as $f) {
                                        echo "- " . $f['name'] . "<br>";

                                    }
                                    echo "</div>";
                                }
                                
                                if($limit_hasta > $regs_total) {
                                    $range_string = "$limit_desde a $regs_total de $regs_total";
                                } else {
                                    $range_string = "$limit_desde a $limit_hasta de $regs_total";
                                }
                                
                                echo "<h3>Copiar registros $range_string desde:</h3>";
                                
                                if(sizeof($files_inexistent) > 0) echo "<p>$n registros no se copiarán porque no se encontré el archivo.</p>";
                                
                                echo "<p>db_old_name<br><strong>$db_old_name</strong></p>
                                        <p>old_imgs_table<br><strong>$old_imgs_table</strong></p>
                                        <p>old_parent_col<br><strong>$old_parent_col</strong></p>
                                        <p>old_title_col<br><strong>$old_title_col</strong></p>
                                        <p>old_imgs_order<br><strong>$old_imgs_order</strong></p>
                                        <p>src_folder<br><strong>$src_folder</strong></p>
                                        <p>&nbsp;</p>";
                                
                                echo "<h3>Copiar hacia:</h3>
                                        <p>parent_class<br><strong>$parent_class</strong></p>
                                        <p>label<br><strong>$label</strong></p>
                                        <p>file_type<br><strong>$file_type</strong></p>
                                        <p>&nbsp;</p>";
                                
                                echo "<form action='migrate-imgs' method='get'>
                                        <input type='hidden' name='db_old_name' value='$db_old_name'>
                                        <input type='hidden' name='old_imgs_table' value='$old_imgs_table'>
                                        <input type='hidden' name='limit_desde' value='$limit_desde'>
                                        <input type='hidden' name='limit_hasta' value='$limit_hasta'>
                                        <input type='hidden' name='old_parent_col' value='$old_parent_col'>
                                        <input type='hidden' name='old_title_col' value='$old_title_col'>
                                        <input type='hidden' name='old_imgs_order' value='$old_imgs_order'>
                                        <input type='hidden' name='src_folder' value='$src_folder'>
                                        <input type='hidden' name='parent_class' value='$parent_class'>
                                        <input type='hidden' name='label' value='$label'>
                                        <input type='hidden' name='file_type' value='$file_type'>
                                        <input type='hidden' name='step' value='confirm'>
                                        <button class='button' type='submit'>CONFIRMAR</button>
                                        <br>
                                        <a class='button hollow' href='migrate-imgs'>VOLVER</a>
                                    </form>";
                                    
                            } 
                            
                            else if($step == 'confirm') {
                                
                                // Archivos movidos
                                echo "<p><strong>Archivos movidos: " . sizeof($files_moved) . "</strong></p>";
                                foreach($files_moved as $fm) {
                                    echo "- " . $fm["id"] . " - " . $fm["name"] . "<br>";
                                }
                                echo "<p>&nbsp;</p>";
                                
                                // Archivos no encontrados
                                echo "<p><strong>Archivos no encontrados: " . sizeof($files_inexistent) . "</strong></p>";
                                foreach($files_inexistent as $fi) {
                                    echo "- " . $fi["id"] . " - " . $fi["name"] . "<br>";
                                }
                                echo "<p>&nbsp;</p>";

                                // Registros creados
                                echo "<p><strong>Registros en la nueva DB: " . sizeof($db_log_ok) . "</strong></p>";
                                foreach($db_log_ok as $b) {
                                    echo "- " . $b["id"] . " - " . $b["name"] . "<br>";
                                }
                                echo "<p>&nbsp;</p>";
                                
                                // Errores al crear registros
                                echo "<p><strong>Errores al crear registros en la nueva DB: " . sizeof($db_log_error) . "</strong></p>";
                                foreach($db_log_error as $e) {
                                    echo "- " . $e["id"] . " - " . $e["name"] . " - Error: " . $e["error"] . "<br>";
                                }
                                echo "<p>&nbsp;</p>";
                                

                                
                                
                                
                                
                                $new_limit_desde = intval($limit_desde) + 100;
                                
                                if($new_limit_desde < $regs_total) {
                                    
                                    echo "<h3>Procesar otro rango de registros</h3>";
                                
                                    echo "<form action='migrate-imgs' method='get'>

                                            <div class='form-row'>
                                                <label>Rango de registros</label>
                                                <div class='form-field'>
                                                    <label>
                                                        Desde
                                                        <input type='number' name='limit_desde' value='$new_limit_desde' class='auto' required>
                                                    </label>

                                                </div>
                                                <div class='form-field'>
                                                    <label>
                                                        Cantidad
                                                        <input type='number' name='limit_hasta' value='$limit_hasta' class='auto' required>
                                                    </label>
                                                </div>

                                            </div>

                                            <input type='hidden' name='db_old_name' value='$db_old_name'>
                                            <input type='hidden' name='old_imgs_table' value='$old_imgs_table'>
                                            <input type='hidden' name='old_parent_col' value='$old_parent_col'>
                                            <input type='hidden' name='old_title_col' value='$old_title_col'>
                                            <input type='hidden' name='old_imgs_order' value='$old_imgs_order'>
                                            <input type='hidden' name='src_folder' value='$src_folder'>
                                            <input type='hidden' name='parent_class' value='$parent_class'>
                                            <input type='hidden' name='label' value='$label'>
                                            <input type='hidden' name='file_type' value='$file_type'>
                                            <input type='hidden' name='step' value='confirm'>
                                            <button class='button' type='submit'>CONFIRMAR</button>
                                            <br>
                                            <a class='button hollow' href='migrate-imgs'>VOLVER</a>
                                        </form>";
                                }
                                
                                else {
                                    
                                    echo "<h3>Se procesaron todos los registros</h3>";
                                }
                                
                            }
                            
                            
                            else {
                            ?>
                            
                            <form action="migrate-imgs" method="get">
                                
                                <h3>Datos de 'origen'</h3>
                                
                                <div class="form-field">
                                    <label>
                                        Nombre de la base de datos 'origen'
                                        <input type="text" name="db_old_name" placeholder="ejemplo_db" class="auto" required>
                                    </label>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Nombre de la tabla que guarda las imágenes en 'origen'
                                        <input type="text" name="old_imgs_table" placeholder="trabajos_imgs" class="auto" required>
                                    </label>
                                </div>
                                
                                <div class="form-row">
                                    <label>Rango de registros</label>
                                    <div class="form-field">
                                        <label>
                                            Desde fila
                                            <input type="number" name="limit_desde" placeholder="0" value="0" class="auto" required>
                                        </label>
                                        <p class="help-text">Máximo de regisros procesados al mismo tiempo: 150</p>
                                    </div>
                                    <div class="form-field">
                                        <label>
                                            Cantidad de filas
                                            <input type="number" name="limit_hasta" placeholder="100" value="100" class="auto" required>
                                        </label>
                                    </div>
                                    
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Nombre de la columna que guarda el parent_id de las imágenes en 'origen'
                                        <input type="text" name="old_parent_col" placeholder="trabajo_id" class="auto" required>
                                    </label>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Nombre de la columna que guarda el título o leyenda de las imágenes en 'origen'
                                        <input type="text" name="old_title_col" placeholder="legend"  class="auto">
                                    </label>
                                    <p class="help-text">Dejar en blanco si no existe este dato.</p>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Ordenamiento de las imágenes en 'origen'
                                        <input type="text" name="old_imgs_order" placeholder="ORDER BY orden ASC, id ASC" value="ORDER BY orden ASC, id ASC" required>
                                    </label>
                                    <p class="help-text">Campo obligatorio. Modificar si corresponde. Extraer este dato del php que admnistra las fotos en el admin viejo o del archivo que muestra las imágenes en el sitio viejo.</p>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Ruta de la carpeta en donde se guardan las imágenes en 'origen'
                                        <input type="text" name="src_folder" placeholder="C:/wamp64/www/example.com/contents/galeria/src/" required>
                                    </label>
                                    <p class="help-text">Ruta absoluta en C:/ (tiene que terminar con una barra).</p>
                                </div>
                                
                                <p>&nbsp;</p>
                                
                                <h3>Datos de 'destino'</h3>
                                
                                <div class="form-field">
                                    <label>
                                        Nombre de la tabla/clase parent en 'destino'
                                        <input type="text" name="parent_class" placeholder="trabajos" class="auto" required>
                                        <p class="help-text">Ejemplo: 'novedades'. Este campo completará el valor en la columa 'parent_class' de la tabla 'media'</p>
                                    </label>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        Label
                                        <input type="text" name="label" placeholder="thumbnail" class="auto" required>
                                        <p class="help-text">Ejemplo: 'thumbnail' o 'galeria'. Este campo completará el valor en la columa 'label' de la tabla 'media'</p>
                                    </label>
                                </div>
                                
                                <div class="form-field">
                                    <label>
                                        File type
                                        <input type="text" name="file_type" placeholder="image" value="image" class="auto" required>
                                        <p class="help-text">Ejemplo: 'image' o 'video'. Por ahora sfunciona solo con image.</p>
                                    </label>
                                </div>
                                
                                <input type="hidden" name="step" value="review">
                                
                                <button class="button" type="submit">MIGRAR</button>
                            </form>
                            
                            <?php
                            }
                            ?>
                            
                        </div>

                       
                    </div>
                </div>
            </main>

            <?php require_once "inc/footer.php"; ?>

        </div> <!-- /.main-wrapper -->

    </body>

</html>
