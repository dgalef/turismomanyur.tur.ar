<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "page_blocks";
$page_title             = _("Editar");

$show_back_button       = false;
//

$obj                    = new main($class_name);
$media_obj              = new media();

if(!isset($_GET['id'])) die("ID does not exist");

$id         = $_GET['id'];
$data 		= $obj->get_row($id);

if (!$data) {
    // If ID doesn't exists yet, a new record is created;
    $bool   = $obj->create_row(array());
    $id     = $obj->lastAffected;
    $data 	= $obj->get_row($id);
}

// Si el registro requiere un parent, tiene que venir especificado en la URL en la variable 'parent_id'
$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;

// Registro parent (para los casos en los que el listado de registros actual tenga un parent ID)
if($parent_id) {
    $parent_title   = $obj->get_parent_title($parent_id, "titulo");
    $page_title     = "$parent_title / $page_title";
}

// Archivo que edita/crea registros
$edit_file 	        = "$class_name-edit";
$add_file           = ($parent_id) ? $edit_file : $edit_file . "?parent_id=$parent_id";
$list_file          = ($parent_id) ? $class_name : $class_name . "?parent_id=$parent_id";


?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                        <div class="form-field">
                            <label><?php echo _("Título"); ?>
                                <input type="text" class="auto-slug" name="title" value="<?php echo escape($data->title); ?>" required>
                                <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                            </label>
                            <p class="help-text"><?php echo _("Help"); ?></p>
                        </div>


                        <!-- ---------->

                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <?php if($show_back_button) echo "<a class='button secondary' href='$class_name'>Volver</a>"; ?>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <div class="bottom-block">
                                    <label class="publicar-label" for=""><?php echo _("Publicar:"); ?></label>
                                    <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                                        <input type="hidden" name="status" class="chk-status" value="<?php echo $data->status; ?>">
                                        <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                        <label for="<?php echo "status-$id"; ?>">
                                            <div class="can-toggle__switch" data-checked="SI" data-unchecked="NO"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <button class="button" type="submit"><?php echo _("Guardar"); ?></button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>Volver</a>"; ?>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>"><?php echo _("Aceptar"); ?></a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()"><?php echo _("Cerrar"); ?></a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
