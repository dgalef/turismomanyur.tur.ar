<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "categories";
$page_title             = _("Editar categoría");

$show_back_button       = true;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");

$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    // If there's no ID, a new record is created;
    $bool = $obj->create_row(array());
    $id = $obj->lastAffected;
    $new_location = basename($_SERVER['REQUEST_URI']);
    if($parent_id) $new_location .= "&id=" . $id;
    else $new_location .= "?id=" . $id;
    header("Location: " . $new_location);
}

$data 		            = $obj->get_row($id);

if (!$data) die("No existe el ID");

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Volver al listado de registros
$volver_url   = $class_name;
if($parent_id) $volver_url .= "?parent_id=$parent_id";

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                        <div class="form-field">
                            <label><?php echo _("Título"); ?>
                                <input type="text" class="auto-slug" name="title" value="<?php echo escape($data->title); ?>" required>
                                <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                            </label>
                            <p class="help-text"><?php echo _("Nombre de la categoría"); ?></p>
                        </div>


                        <div class="form-field">
                            <label><?php echo _("Slug"); ?></label>
                            <div class="input-group">
                                <span class="input-group-label">cmtate.com/category/</span>
                                <input type="text" class="input-group-field slug unique off" id="slug" name="slug" value="<?php echo escape($data->slug); ?>" tabindex="-1" pattern="alpha_numeric_dash" required>

                            </div>
                            <span class="form-error" data-form-error-for="slug"><?php echo _("Campo requerido"); ?></span>
                            <p class="help-text"><?php echo _("Esto conforma la URL. Acepta sólo letras, números y guiones (sin caracteres especiales, ni guiones bajos).<br><strong>No puede estar duplicado (no puede existir otro registro con el mismo slug).</strong>"); ?></p>
                        </div>
                        
                        <div class="form-field">
                            <label><?php echo _("Asociar a"); ?>
                                <select class="" name="parent_id">
                                    <option value="NULL"><?php echo _("Primer nivel"); ?></option>
                                    <?php
                                    $obj->where = "WHERE id != $id";
                                    $obj->order = "ORDER BY title ASC";
                                    $first_level_cats = $obj->get_data();
                                    foreach($first_level_cats as $c) {
                                        $sel = ($data->parent_id == $c->id) ? "selected" : "";
                                        echo "<option value='$c->id' $sel>$c->title</option>";
                                    }
                                    ?>
                                </select>
                                <span class="form-error">Campo requerido</span>
                            </label>
                            <p class="help-text hide">Nombre del producto</p>
                        </div>

                        <!-- ---------->

                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <a class="button hollow" href="<?php echo $class_name; ?>"><?php echo _("Volver"); ?></a>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <div class="bottom-block">
                                    <label class="publicar-label" for=""><?php echo _("Publicar:"); ?></label>
                                    <?php $status = ($data->status == 'temp') ? "draft" : $data->status; ?>
                                    <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                                        <input type="hidden" name="status" class="chk-status" value="<?php echo $status; ?>">
                                        <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                        <label for="<?php echo "status-$id"; ?>">
                                            <div class="can-toggle__switch" data-checked="\f00c" data-unchecked="NO"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <button class="button" type="submit"><?php echo _("Guardar"); ?></button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>Volver</a>"; ?>
                            <a class="popup-btn-otro button hollow" href="<?php echo $add_file; ?>"><?php echo _("Añadir otro"); ?></a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>"><?php echo _("Aceptar"); ?></a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()"><?php echo _("Cerrar"); ?></a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
