<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "categories";
$page_title             = _("Categorías");

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");
$blocks_obj             = newClass("pages_blocks");


// Listado child de parent_class
$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if ($obj->parent_class) {

    if (!$parent_id && !$obj->allow_no_parent) die("Parent ID not specified");

    // Registro parent (para los casos en los que el listado de registros actual tenga un parent ID)
    if($parent_id) {
        $obj->where     = "WHERE parent_id = $parent_id";

        $parent_title   = $obj->get_parent_title($obj->parent_class, $parent_id, "title");
        $page_title     = "$page_title / $parent_title";
    }
}

else if (!$obj->parent_class && $parent_id) {
    die("There is a parent ID but not a parent class.");
}

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Obtener datos de la tabla correspondiente
if(isset($_GET['search']) && isset($_GET['val'])) {
    $data = $obj->search_data($_GET['search'], $_GET['val']);
}
else {
    $data = $obj->get_data();
}

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="list-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                        <div class="main-opts">

                            <!--
                            Buscador de registros.
                            Usar data-rel para especificar las columnas en donde buscar, separadas por coma. Ej data-rel="titulo,descripcion,autor"
                            El buscador funciona buscando texto -> "%texto a buscar%"
                            -->
                            <div class="search-input">
                                <input type="text" name="query" class="query" data-rel="title" value="<?php if(isset($_GET["val"])) echo $_GET["val"] ?>" placeholder="<?php echo _("Buscar"); ?>">
                                <i class="far fa-search fa-fw"></i>
                            </div>

                            <a class="button" href="<?php echo $add_file; ?>"><i class="far fa-plus"></i> <?php echo _("Añadir"); ?></a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="content-list">

                        <?php

                        // No busco y no hay registros
                        if (!isset($_GET['search']) && $obj->currRows == 0)
                            echo _("Haga click en 'Añadir' para crear el primer elemento.");

                        else {

                            // Busco y no hay registros
                            if(isset($_GET['search']) && $obj->currRows == 0)
                                echo _("No hay registros coincidentes con el criterio de búsqueda.");

                            // Hay registros
                            else {

                                if(isset($_GET['search']))
                                    echo "<h4>" . sprintf(_("Resultados de búsqueda para <em>%s</em>"), $_GET['val']) . "</h4>";

                                echo "<p>" . sprintf(_("Mostrando %s de %s"), $obj->rowsRange, $obj->totalRows) ."</p>";
                        ?>


                        <ul class='menu-level data-list' data-table="<?php echo $class_name; ?>" data-order="<?php echo $obj->sort_order; ?>">


                            <?php

                                // https://stackoverflow.com/questions/2871861/how-to-build-unlimited-level-of-menu-through-php-and-mysql

                                $html = '';
                                $parent = 0;
                                $parent_stack = array();

                                // $items contains the results of the SQL query
                                $children = array();
                                foreach ( $data as $item ) {
                                    $pid = ($item->parent_id) ? $item->parent_id : 0;
                                    $children[$pid][] = $item;
                                }

                                while ( ( $row = current( $children[$parent] ) ) || ( $parent > 0 ) )
                                {

                                    $p = $parent;

                                    if ( !empty( $row ) )
                                    {

                                        $id         = $row->id;
                                        $elem_id    = "item_" . $id;

                                        // URL para editar registros
                                        $edit_file 	= "$class_name-edit?id=$id";
                                        if($parent_id) $edit_file .= "&parent_id=$parent_id";

                                        $html .= "<li id='$elem_id' class='data-item' data-id='$row->id' data-pos='$row->pos'>
                                                        <div class='data-item-inner'>
                                                            <div class='item-title'><a class='title' href='$edit_file'>$row->title</a></div>
                                                            <div class='sort-handler'><span class='handler'><i class='far fa-arrows-alt fa-lg fa-fw'></i></span></div>
                                                            <div class='status-indicator published'>
                                                                <div class='checkbox-switch can-toggle can-toggle-size-small demo-rebrand-2'>
                                                                    <input type='hidden' name='status' class='chk-status auto-update' value='$row->status'>
                                                                    <input id='status-$id' type='checkbox' class='chk-publish'>
                                                                    <label for='status-$id'>
                                                                        <div class='can-toggle__switch' data-checked='SI' data-unchecked='NO'></div>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class='item-opts text-right'>
                                                                <a href='$edit_file' class='opt'><i class='fal fa-pen'></i></a>
                                                                <a href='#' class='opt' data-role='delete'><i class='fal fa-trash'></i></a>
                                                                <div class='checkbox-check'><input class='chk' type='checkbox' value='$row->id' name='id[]' id='$row->id'><label for='$row->id'></label></div>
                                                            </div>
                                                        </div>";

                                        // 1) The item contains children:
                                        // store current parent in the stack, and update current parent
                                        if ( !empty( $children[$row->id] ) )
                                        {
                                            $html .= "<ul class='menu-level data-list' data-table='$class_name' data-order='$obj->sort_order'>";
                                            array_push( $parent_stack, $parent );
                                            $parent = $row->id;
                                        }
                                        // 2) The item does not contain children
                                        else {
                                            $html .= "</li>";
                                        }
                                    }
                                    // 3) Current parent has no more children:
                                    // jump back to the previous menu level
                                    else
                                    {
                                        $html .= "</li></ul>";
                                        $parent = array_pop( $parent_stack );
                                    }

                                    next( $children[$p] );
                                }

                                // At this point, the HTML is already built
                                echo $html;

                            ?>



                        </ul>

                        <div class="data-list-opts text-right">
                            <div class="data-list-opt">
                                <a href="#" class="delete-selected"><?php echo _("Borrar seleccionados") ?></a>
                            </div>
                            <div class="data-list-opt select-all-opt">
                                <div class="checkbox-check">
                                    <input class="chk" type="checkbox" id="select-all">
                                    <label for="select-all" title="<?php echo _("Seleccionar todos") ?>"></label>
                                </div>
                            </div>
                        </div>

                        <div class="list-table-footer">

                            <?php echo $obj->getPagination(); ?>


                        </div>

                        <?php

                            } // fin if(currRows != 0)

                        }
                        ?>

                    </div> <!-- contenidos -->

                </div>
            </div>

            <div class="popup mfp-hide">
                <div class="mssg contents rounded">[mensaje]</div>
                <div class="action-buttons">
                    <!-- Aceptar -->
                    <a class="button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>"><?php echo _("Aceptar") ?></a>
                </div>
            </div>
        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
