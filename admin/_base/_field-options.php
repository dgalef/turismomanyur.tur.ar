<!---------------------------------
-----------------------------------
    DATO ÚNICO (ej: DNI)
-----------------------------------
---------------------------------->
<div class="form-field auto">
    <label class=""><?php echo _("DNI"); ?> (valor único)
        <input type="number" name="dni" class="is-unique" value="<?php echo escape($data->dni); ?>">
        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
    </label>
    <p class="help-text hide"><?php echo _("Este dato es único, no puede existir otro igual en la tabla."); ?></p>
</div>
   
<!---------------------------------
-----------------------------------
    COLOR
-----------------------------------
---------------------------------->
<div class="form-field">
    <label><?php echo _("Color"); ?></label>
    <div class="color-wrapper">
        <input type="color" class="color_box ignore" value="">
        <label>
            <input type="text" name="color_code" class="color_code" value="<?php echo escape($data->color_code); ?>" pattern="hex_color" placeholder="#CC0000">
            <span class="form-error"><?php echo _("Formato inválido"); ?></span>
        </label>
        <p class="help-text"><?php echo _("Establecer color en formato HEX. Ej: #CC0000"); ?></p>
    </div>
</div>

<!---------------------------------
-----------------------------------
    CHECKBOXES (se guardan los valores en una columna dentro del mismo row: "tag1 || tag2 || etc")
-----------------------------------
---------------------------------->
<div class="form-row">
    <label><?php echo _("Categorías"); ?> (Categorías de class -checkbox / multiple-)</label>
    <?php
    $vals_arr = explode(" || ", $data->tags);
    //print_r($vals_arr);
    foreach($obj->tags as $k => $v) {

        $sel = (array_search($k, $vals_arr) !== false) ? "checked" : "";

        echo   "<div class='form-field'>
                    <div class='checkbox-item'>
                        <div class='checkbox-check'>
                            <input class='chk' type='checkbox' value='$k' name='tags[]' id='tag-$k' $sel>
                            <label for='tag-$k' title='"._("Seleccionar")."'></label>
                        </div>
                        <label>$v</label>
                    </div>
                </div>";
    }
    echo "<input class='chk' type='hidden' value='' name='tags[]'>";
    ?>
</div>

<!---------------------------------
-----------------------------------
TOGGLER SWITCH (toggles value between 1 & 0)
-----------------------------------
---------------------------------->
<!--
    Opciones:
    - Con o sin fieldset para darle mas aire respecto de los elementos previos y posteriores
    - .can-toggle-size-medium o can-toggle-size-small para definir su tamaño
    - Con o sin span.switch-label
-->
<fieldset>
    <div class="form-field">
        <label><?php echo _("Términos y condiciones"); ?></label>
        <div class="switch-wrapper">
            <span class="switch-label">Aceptar</span>
            <?php $sw_name = 'tos'; ?>
            <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                <input type="hidden" name="<?php echo $sw_name; ?>" class="toggler-val" value="<?php echo $data->$sw_name; ?>">
                <input id="<?php echo $sw_name; ?>" type="checkbox" class="toggler-chk">
                <label for="<?php echo $sw_name; ?>">
                    <div class="can-toggle__switch" data-checked="\f00c" data-unchecked="NO"></div>
                </label>
            </div>        
        </div>
    </div>
</fieldset>

<!---------------------------------
-----------------------------------
TOGGLER ICON (toggles value between 1 & 0)
-----------------------------------
---------------------------------->
<!-- 
    Configurar: 
    - ícono para el estado 'on' (valu = 1) - Su color será $primary-color
    - ícono para el estado 'off' (valu = 0) - Su color será $medium-gray
-->
<?php $ico_name = 'featured';   // Nombre de la columna type='tinyint' ?>
<div class="checkbox-icon">
    <input type="hidden" name="<?php echo $ico_name; ?>" class="chk-toggler auto-update" value="<?php echo $row->$ico_name; ?>">
    <input class="chk-icon" type="checkbox" id="<?php echo "$ico_name-$id"; ?>">
    <label for="<?php echo "$ico_name-$id"; ?>" title="<?php echo _("Destacar") ?>">
        <i data-rel='on' class="fal fa-eye"></i>
        <i data-rel='off' class="fal fa-eye-slash"></i>
    </label>
</div>

<!---------------------------------
-----------------------------------
TOGGLER CONTENT
-----------------------------------
---------------------------------->
<!--
Se compone de un:
    .toggler-trigger[data-rel='sarasa']     
        (De acuerdo a su 'value', se muestran/ocultan los contenidos de los elementos relacionados por el data-rel.)
Y dos o más:
    .toggler-content[data-rel='sarasa']#val_1
    .toggler-content[data-rel='sarasa']#val_2
    ...
-->
<div class="form-field">
    <label>Tipo de contenido
        <select name="content_type" class="toggler-trigger" data-rel='content_type'>
            <option value='foto' <?php if ($data->media_type = "foto") echo "selected"; ?>>Foto</option>
            <option value='video' <?php if ($data->media_type = "video") echo "selected"; ?>>Video</option>
        </select>
    </label>
</div>
<div id="video" class="form-field toggler-content" data-rel='content_type'>
    <label>Video
        <input type="text" class="" name="video" value="<?php echo escape($data->video); ?>">
    </label>
</div>
<div id="foto" class="form-field toggler-content" data-rel='content_type'>
    <label>Foto
        <input type="text" class="" name="foto" value="<?php echo escape($data->foto); ?>">
    </label>
</div>
<!---------------------------------
-----------------------------------
FIELDSET / FORM ROW / FORM FIELD
-----------------------------------
---------------------------------->
<!-- 
    fieldset
        Agrupa varios elementos bajo un título
        '.boxed' es opciona para enmarcar
-->
<fieldset class="boxed">
    <h3>Título fieldset</h3>
    <div class="form-row">
        <div class="form-field">
            ...
        </div>
        ...
    </div>
    ...
</fieldset>

<!-- 
    .form-row
        Agrupa dos o mas .form-field en una hilera
        '.full' es opcional para que los campos de la hilera ocupen todo el ancho disponible
-->
<div class="form-row full">
    <div class="form-field">
        ...
    </div>
    ...
</div>
<!-- 
    .form-field
        Es el campo del formulario (label + input + form-error + help-text)
        '.grow' es opcional y sirve cuando el form-field está dentro de una .form-row para que su ancho crezca hasta ocupar el espacio restante de la hilera
        (no hace diferencia si .form-row es .full)
-->
<div class="form-row">
    <div class="form-field">
        ...
    </div>
    <div class="form-field grow">
        ...
    </div>
</div>