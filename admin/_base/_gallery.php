<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "gallery";
$page_title             = _("Gallery");
$label                  = "photo";

$show_back_button       = true;
$allow_feature          = false;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");
$blocks_obj             = newClass("pages_blocks");


// Listado child de parent_class
$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if ($obj->parent_class) {

    if (!$parent_id && !$obj->allow_no_parent) die("Parent ID not specified");

    // Registro parent (para los casos en los que el listado de registros actual tenga un parent ID)
    if($parent_id) {
        $obj->where     = "WHERE parent_id = $parent_id";

        $parent_title   = $obj->get_parent_title($obj->parent_class, $parent_id, "title");
        $page_title     = "$page_title / $parent_title";
    }
}

else if (!$obj->parent_class && $parent_id) {
    die("There is a parent ID but not a parent class.");
}

$media_data     = $obj->get_media($parent_id, $label);

// Volver al listado de registros
$volver_url = ($parent_id) ? $obj->parent_class : null;
// Volver al edit del registro parent
//if($parent_id) $volver_url = $obj->parent_class . "-edit?id=" . $parent_id;

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="media-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                        <div class="main-opts hide">

                            <!--
                            Buscador de registros.
                            Usar data-rel para especificar las columnas en donde buscar, separadas por coma. Ej data-rel="titulo,descripcion,autor"
                            El buscador funciona buscando texto -> "%texto a buscar%"
                            -->
                            <div class="search-input">
                                <input type="text" name="query" class="query" data-rel="title,subtitle" value="<?php if(isset($_GET["val"])) echo $_GET["val"] ?>" placeholder="<?php echo _("Buscar"); ?>">
                                <i class="far fa-search fa-fw"></i>
                            </div>

                            <a class="button" href="<?php echo $add_file; ?>"><i class="far fa-plus"></i> <?php echo _("Añadir"); ?></a>
                        </div>
                    </div>

                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="content-list">

                        <?php

                        // No busco y no hay registros
                        if (!isset($_GET['search']) && $obj->currRows == 0) {
                            echo _("Haga click en 'Añadir' para crear el primer elemento.");
                        }

                        else {


                            // Busco y no hay registros
                            if(isset($_GET['search']) && $obj->currRows == 0) {
                                echo _("No hay registros coincidentes con el criterio de búsqueda.");
                            }


                            // Hay registros
                            else {

                                if(isset($_GET['search']))
                                    echo "<h4>" . sprintf(_("Resultados de búsqueda para <em>%s</em>"), $_GET['val']) . "</h4>";

                                echo "<p>" . sprintf(_("Mostrando %s de %s"), $obj->rowsRange, $obj->totalRows) ."</p>";

                        ?>

                        <ul class="sortable data-list" data-table="media" data-order="<?php echo $obj->sort_order; ?>">

                            <?php

                                foreach($media_data as $row) {
                                    $id         = $row->id;
                                    $elem_id    = "item_" . $id;
                            ?>



                            <li id="<?php echo $elem_id; ?>" class="data-item" data-id="<?php echo $row->id; ?>" data-pos="<?php echo $row->pos; ?>">

                                <div class="data-item-inner">
                                   
                                    <!-- Thumbnail -->
                                    <?php
                                    if ($row->file_type == "images") {
                                        if($row->file_ext == "svg")
                                            $thum_el    = "<img class='fit-content' src='/media/images/src/$row->file_name'>";
                                        else
                                            $thum_el    = "<img class='fit-content' src='/media/images/tiny/$row->file_name'>";
                                    }
                                    // Add class 'fit-content' to <img> to keep original aspect ratio
                                    else
                                        $thum_el    = "<div class='thum-$row->file_type'></div>";

                                    echo "<div class='item-thum'>$thum_el</div>";
                                    ?>
                                    
                                    <div class="item-title">
                                        <input type='text' name='media_title' class='auto-update' value='<?php echo $row->media_title; ?>' placeholder='<?php echo _("Título"); ?>'>
                                        <span class='help-text'><?php echo $row->file_name; ?></span>
                                    </div>

                                    <!-- Handler para drag&drop -->
                                    <?php if($obj->sort_allow) { ?>
                                    <div class="sort-handler">
                                        <span class="handler" title="<?php echo _("Arrastrar para ordenar") ?>"><i class="far fa-arrows-alt fa-lg fa-fw"></i></span>
                                    </div>
                                    <?php } ?>

                                    <!-- Indicador de publicado -->
                                    <div class="status-indicator published">

                                        <div class="checkbox-switch can-toggle can-toggle-size-small demo-rebrand-2">
                                            <input type="hidden" name="status" class="chk-status auto-update" value="<?php echo $row->status; ?>" tabindex="-1">
                                            <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish" tabindex="-1">
                                            <label for="<?php echo "status-$id"; ?>" title="<?php echo _("Borrador <-> Publicado") ?>">
                                                <div class="can-toggle__switch" data-checked="SI" data-unchecked="NO"></div>
                                            </label>
                                        </div>

                                    </div>

                                    <!-- Opciones / Checkbox / etc -->
                                    <div class="item-opts text-right">

                                        <?php if($allow_feature) { ?>
                                        <div class="checkbox-star">
                                            <input type="hidden" name="featured" class="chk-featured auto-update" value="<?php echo $row->featured; ?>">
                                            <input class="chk-star" type="checkbox" id="star-<?php echo $row->id; ?>">
                                            <label for="star-<?php echo $row->id; ?>" title="<?php echo _("Destacar") ?>"></label>
                                        </div>
                                        <?php } ?>

                                        <a href="#" class="opt" data-role="delete" title="<?php echo _("Eliminar") ?>" tabindex="-1"><i class="fal fa-trash"></i></a>

                                        <div class="checkbox-check">
                                            <input class="chk" type="checkbox" value="<?php echo $row->id; ?>" name="id[]" id="<?php echo $row->id; ?>">
                                            <label for="<?php echo $row->id; ?>" title="<?php echo _("Seleccionar") ?>"></label>
                                        </div>

                                    </div>
                                </div>
                            </li>

                            <?php } // end [for each] ?>

                        </ul>

                        <div class="data-list-opts text-right">
                            <div class="data-list-opt">
                                <a href="#" class="delete-selected"><?php echo _("Borrar seleccionados"); ?></a>
                            </div>
                            <div class="data-list-opt select-all-opt">
                                <div class="checkbox-check">
                                    <input class="chk" type="checkbox" id="select-all">
                                    <label for="select-all" title="<?php echo _("Seleccionar todos") ?>"></label>
                                </div>
                            </div>
                        </div>

                        <div class="list-table-footer">

                            <?php echo $obj->getPagination(); ?>

                        </div>

                        <?php

                            } // fin if(currRows != 0)
                        }
                        ?>

                    </div> <!-- contenidos -->
                    
                    <!-- Media files -->
                    <div class="form-field">
                        <!-- <label><?php echo _("Foto"); ?></label> -->
                        <?php

                        // Allow multifiles upload
                        $multi          = "true";
                        // Allowed extensions
                        $exts           = "jpg,jpeg,gif,png";
                        // Tag for media file (e.g: "cover", "thumbnail", etc)
                        //$label          = "thumbnail";
                        // Hint for the user
                        $help           = _("Formatos aceptados: JPG, GIF y PNG (recomendado). Medida recomendada: 440 x 220 pixeles.");
                        // Show input[name='media_title'] -> (false or string)
                        $media_title    = "false";

                        echo "<div class='pluploader' data-id='$parent_id' data-class='$class_name' data-order='$media_obj->sort_order' data-label='$label' data-media-title='$media_title' data-multifiles='$multi' data-extensions='$exts' data-btn-text='Añadir logos' data-help-text='$help'>

                                        <div class='plupload-wrapper'></div>
                                    </div>";

                        ?>

                    </div>

                </div>
            </div>
            
            <div class="bottom-fixed">
                <div class="bottom-fixed-left">
                    <div class="bottom-block">
                        <?php
                        if($show_back_button && $volver_url) echo "<a class='button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>";
                        ?>
                    </div>
                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <a class="popup-btn-volver button hollow" href="<?php echo $volver_url; ?>"><i class="far fa-arrow-left"></i><?php echo _("Volver") ?></a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>"><?php echo _("Aceptar") ?></a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()"><?php echo _("Cerrar") ?></a>
                        </div>
                    </div>
                </div>
            </div>
            
        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
