<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "newsletters";
$page_title             = _("newsletter");

$show_back_button       = true;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");

$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    // If there's no ID, a new record is created;
    $result = $obj->create_row(array());
    if($result['success']) {
        $id = $obj->lastAffected;
        $new_location = basename($_SERVER['REQUEST_URI']);
        if($parent_id) $new_location .= "&id=" . $id;
        else $new_location .= "?id=" . $id;
        header("Location: " . $new_location);
    } else {
        die($result['error']);
    }
}

$data 		            = $obj->get_row($id);

$page_title             = ($data->status == 'temp') ? sprintf(_("Agregar %s"), $page_title) : sprintf(_(" Editar %s"), $page_title);

if (!$data) die("ID does not exist");

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Volver al listado de registros
$volver_url   = $class_name;
if($parent_id) $volver_url .= "?parent_id=$parent_id";

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$data->title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                        <div class="form-inner">
                           
                            <textarea id="code" style="width:100%;height:600px;font-size:14px;"><?php 
                                
                                function url_get_contents ($Url) {
                                    if (!function_exists('curl_init')){ 
                                        echo('CURL is not installed!');
                                        return;
                                    }
                                    $ch = curl_init();
                                    curl_setopt($ch, CURLOPT_URL, $Url);
                                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                                    $output = curl_exec($ch);
                                    curl_close($ch);
                                    return $output;
                                }
                                
                                echo url_get_contents("https://turismomanyur.tur.ar/nl/$data->slug");
                                
                                //echo file_get_contents("https://turismomanyur.tur.ar/nl/$data->slug");
                                
                                
                                ?></textarea>
                            
                            
                            
                        </div>

                        <!-- ---------->

                        <?php if($parent_id) echo "<input type='hidden' name='parent_id' value='$parent_id'>"; ?>
                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <a class="button hollow" href="<?php echo $volver_url; ?>"><i class="far fa-arrow-left"></i><?php echo _("Volver"); ?></a>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <a id="copy-code" class='button copy-code'>Copiar código</a>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>"; ?>
                            <a class="popup-btn-otro button hollow" href="<?php echo $add_file; ?>">
                                <?php echo _("Añadir otro"); ?>
                            </a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>">
                                <?php echo _("Aceptar"); ?>
                            </a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()">
                                <?php echo _("Cerrar"); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->

    <script>

        document.getElementById("copy-code").addEventListener("click", function() {
            copyToClipboard(document.getElementById("code"));
        });

        function copyToClipboard(elem) {
            // create hidden text element, if it doesn't already exist
            var targetId = "_hiddenCopyText_";
            var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
            var origSelectionStart, origSelectionEnd;
            if (isInput) {
                // can just use the original source element for the selection and copy
                target = elem;
                origSelectionStart = elem.selectionStart;
                origSelectionEnd = elem.selectionEnd;
            } else {
                // must use a temporary form element for the selection and copy
                target = document.getElementById(targetId);
                if (!target) {
                    var target = document.createElement("textarea");
                    target.style.position = "absolute";
                    target.style.left = "-9999px";
                    target.style.top = "0";
                    target.id = targetId;
                    document.body.appendChild(target);
                }
                target.textContent = elem.textContent;
            }
            // select the content
            var currentFocus = document.activeElement;
            target.focus();
            target.setSelectionRange(0, target.value.length);

            // copy the selection
            var succeed;
            try {
                succeed = document.execCommand("copy");
                alert("Código copiado");
            } catch(e) {
                succeed = false;
            }
            // restore original focus
            if (currentFocus && typeof currentFocus.focus === "function") {
                currentFocus.focus();
            }

            if (isInput) {
                // restore prior selection
                elem.setSelectionRange(origSelectionStart, origSelectionEnd);
            } else {
                // clear temporary content
                target.textContent = "";
            }
            return succeed;
        }

    </script>
    
</body>
</html>
