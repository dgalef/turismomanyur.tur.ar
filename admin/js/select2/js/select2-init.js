/*
    SELECT2 - TAGS
*/

$(function() {
    
    var select2_opts = {
        placeholder: "Seleccionar",
        allowClear: true,
        tags: true,
        tokenSeparators: [','],
        createTag: function (params) {

            // If term is empty
            if (params.term.length < 1) 
                return null; // Return null to disable tag creation

            return {
                id: 'temp_tag_id',
                text: params.term
            }
        }
    }
    $(".tags-selector").select2(select2_opts);
    $('.tags-selector').on('select2:select', function (e) {

        var params           = e.params.data;
        var tag_title        = encodeURIComponent(params["text"]);
        var multiple         = ($(this).is("[multiple]")) ? "true" : "false";

        var post_obj		 = "art_id=" + $(this).data("art-id");
        post_obj		    += "&tag_id=" + params["id"];
        post_obj			+= "&art_ref=" + $(this).data("art-ref");
        post_obj			+= "&tag_ref=" + $(this).data("tag-ref");
        post_obj			+= "&multiple=" + multiple;
        post_obj			+= "&table=relations";
        post_obj            += "&process=create-relation";
        post_obj		    += "&tag_title=" + tag_title;

        $.post('php/process_data.php', post_obj, function(data) {
            console.log(data);

            // If the tag was created...
            if(params["id"] == 'temp_tag_id') {
                var resp	= $.parseJSON(data);
                // Add new tag id
                $('.tags-selector').find( ":contains('" + tag_title + "')" ).val(resp.newId);
                // Reinit plugin
                $(".tags-selector").select2(select2_opts);
            }
        });

    });

    //
    // Remove tag relation
    $('.tags-selector, .tags-selector').on('select2:unselect', function (e) {

        var params = e.params.data;

        var post_obj		 = "art_id=" + $(this).data("art-id"); 
        post_obj		    += "&art_ref=" + $(this).data("art-ref"); 
        post_obj		    += "&tag_id=" + params["id"];
        post_obj		    += "&tag_ref=" + $(this).data("tag-ref"); 
        post_obj			+= "&table=relations";
        post_obj            += "&process=delete-relation";

        $.post('php/process_data.php', post_obj, function(data) {
            console.log(data);
        });
    });
})