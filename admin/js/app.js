Foundation.Abide.defaults.patterns['alpha_numeric_dash'] = /^[a-zA-Z0-9-]+$/;
Foundation.Abide.defaults.patterns['no_spaces'] = /^\S*$/;
Foundation.Abide.defaults.patterns['hex_color'] = /^#([A-Fa-f0-9]{6}|[A-Fa-f0-9]{3})$/;

$(document).foundation();

$("#login-form")
// form validation failed
    .on("forminvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + ev.target.id + " noes valido");
    if(typeof grecaptcha !== 'undefined') grecaptcha.reset();
})
// form validation passed, form will submit if submit event not returned false
    .on("formvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + frm.attr('id') + " is valid");


    frm.find("button").addClass("disabled").attr("disabled", "disabled");


    // Procesar formulario con ajax
    // usando FormData para habilitar el envío de archivos
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(frm[0]);
    }

    var jqxhr = $.ajax({
        url         : "php/account.php",
        data        : formdata ? formdata : frm.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){

            var resp	= $.parseJSON(data);

            var success	= resp.success;
            var mssg 	= resp.message;

            if(success == 1) {
                window.location = ".";

            } else {
                frm.find(".callout.alert").css("display", "block").html(mssg)

                frm.find("button").removeClass("disabled").removeAttr("disabled");
                if(typeof grecaptcha !== 'undefined') grecaptcha.reset();
            }

        }
    }).done(function() {
        //console.log( "success" );
    })
    .fail(function( jqXHR, textStatus, errorThrown ) {
        console.log( "textStatus:" + textStatus + "\r\n errorThrown: " + errorThrown );
    })
    .always(function() {
        //console.log( "complete" );
    });

})
// to prevent form from submitting upon successful validation
    .on("submit", function (ev) {
    ev.preventDefault();
});

$("#forgot-pass-form, #reset-pass-form")
// form validation failed
    .on("forminvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + ev.target.id + " noes valido");
    if(typeof grecaptcha !== 'undefined') grecaptcha.reset();
})
// form validation passed, form will submit if submit event not returned false
    .on("formvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + frm.attr('id') + " is valid");


    frm.find("button").addClass("disabled").attr("disabled", "disabled");


    // Procesar formulario con ajax
    // usando FormData para habilitar el envío de archivos
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(frm[0]);
    }

    var jqxhr = $.ajax({
        url         : "php/account.php",
        data        : formdata ? formdata : frm.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){

            var resp	= $.parseJSON(data);

            var success	= resp.success;
            var mssg 	= resp.message;

            $(".response").prepend("<p>" + mssg + "</p>");
            $(".response").removeClass("hide");

            if(frm.is("#reset-pass-form")) frm.remove();

            if(success != 1) {
                frm.find("button").removeClass("disabled").removeAttr("disabled");
                if(typeof grecaptcha !== 'undefined') grecaptcha.reset();

            }

        }
    }).done(function() {
        //console.log( "success" );
    })
    .fail(function( jqXHR, textStatus, errorThrown ) {
        console.log( "textStatus:" + textStatus + "\r\n errorThrown: " + errorThrown );
    })
    .always(function() {
        //console.log( "complete" );
    });

})
// to prevent form from submitting upon successful validation
    .on("submit", function (ev) {
    ev.preventDefault();
});

$("#edit-form")
    // form validation failed
    .on("forminvalid.zf.abide", function (ev, frm) {
        console.log("Form id " + ev.target.id + " no es valido");

        var $invalid_elements = $(".is-invalid-input");
        var $offsetTop = $invalid_elements.offset().top - 50;
        TweenMax.to(window, .5, {scrollTo:{y:$offsetTop, autoKill:false}, ease:Power3.easeInOut});
    })
    // form validation passed, form will submit if submit event not returned false
    .on("formvalid.zf.abide", function (ev, frm) {
        console.log("Form id " + frm.attr('id') + " is valid");
        frm.find("button").addClass("disabled").attr("disabled", "disabled");



        if(tinyMCE.editors.length > 0) tinyMCE.triggerSave();

        // Procesar formulario con ajax
        // usando FormData para habilitar el envío de archivos
        var formdata = false;
        if (window.FormData){
            formdata = new FormData(frm[0]);

            // Remover los campos marcados como "ignore-field"
            frm.find('.ignore-field').each(function(i, field) {
                var fname = $(field).attr('name');
                if(fname) {
                    formdata.delete(fname);
                }
            });

            // Set value NULL to all input[type="number"] and *.null that are empty
            frm.find("[type='number'], [type='date'], .null").filter(function() {return !this.value}).each(function(i, field) {
                var fname = $(field).attr('name');
                if(fname) {
                    formdata.set(fname, 'NULL');
                }
            });

            new Response(formdata).text().then(console.log)
        }


    // Qué onda esto? ---> $("form :not(.no-serialize)").serialize();

        var jqxhr = $.ajax({
            url         : "php/process_data.php",
            data        : formdata ? formdata : frm.serialize(),
            cache       : false,
            contentType : false,
            processData : false,
            type        : 'POST',
            success     : function(data, textStatus, jqXHR) {

                ///

                var obj 	= $.parseJSON(data);

                var success	= obj.success;
                var mssg 	= obj.message;
                var error 	= obj.error;
                var newId 	= obj.row_id;

                //console.log(mssg);

                if (success) {

                    console.log("success: " + mssg)
                    showResponse(success, mssg);

                }
                else {

                    console.log(error)
                    showResponse(success, error);
                }

                frm.find("button").removeClass("disabled").removeAttr("disabled");

            }



        }).done(function() {
            //console.log( "success" );
        })
        .fail(function( jqXHR, textStatus, errorThrown ) {
            console.log( "textStatus:" + textStatus + "\r\n errorThrown: " + errorThrown );
        })
        .always(function() {
            //console.log( "complete" );
        });

    })
        // to prevent form from submitting upon successful validation
        .on("submit", function (ev) {
        ev.preventDefault();
    });

// Mostrar respuesta
function showResponse(success, mssg) {

    if(success) {
        $("#response").addClass("response-ok");
        $("#response").removeClass("response-error");
    } else {
        $("#response").addClass("response-error");
        $("#response").removeClass("response-ok");
    }

    $('.mssg').html(mssg);

    $.magnificPopup.open({
        items: {
            src: '#response'
        },
        type: 'inline'

        // You may add options here, they're exactly the same as for $.fn.magnificPopup call
        // Note that some settings that rely on click event (like disableOn or midClick) will not work here
    }, 0);


/*
    (success) ? $(".mssg").addClass("mssgOK") : $(".mssg").addClass("mssgWrong");;
    $('.mssg').html(mssg);

    $.fancybox.open([{href : '#fancy-response'}], {modal: true, padding: 10});
    */
}

$(document).ready(function() {



    /*
        ASIDE MENU
        Auto active class
    */
    var url = window.location.toString();
    var currFileName = url.substring(url.lastIndexOf('/')+1);
    if(currFileName == "") currFileName = ".";
    $("[href='" + currFileName + "']").addClass("active");


    $("[href='" + $("body").data("rel") + "']").addClass("active");

    // Activar parent <a>
    $(".active").parents("ul").siblings("a").addClass("active");

    // Submenú
    $("aside nav a").each(function() {

        // Si el <a> tiene submenú
        if($(this).find("+ul").length > 0) {

            // Marcar como tiene submenú
            $(this).addClass("has-submenu").append("<span></span>");

            // Si el <a> está activo, marcar como submenu activo
            if($(this).is(".active")) {
                $(this).addClass("submenu-active");
            }
        }
    })

    // Click en <a>
    $("aside nav").on("click", "a", function(e) {

        // Click en flecha submenu o en <a> sin href
        if(e.target.nodeName == 'SPAN' || (e.target.nodeName == 'A' && $(this).attr("href") == null)) {
            e.preventDefault();

            $(this).find("+ul").slideToggle();
            $(this).toggleClass("submenu-active");

        }
    })
/*
    $(".copy-code").on("click", function(e) {
        var textarea = $("#code")[0];
        textarea.select();
        console.log("select");
    });
*/
    // Click en flecha de submenu
    $("aside nav a.has-submenu").on("click", "span", function(e){
        //e.preventDefault();
        //$(this).parent().trigger("click");
    })

    $("aside nav ul a").not(".active").find("+ul").hide();

    /*
        DELETE list elements
    */

    function deleteItems(dataObj) {

        // Set delete process
        dataObj += "&process=delete-rows";

        console.log(dataObj);

        $.post("php/process_data.php", dataObj, function(data){

            var response = $.parseJSON(data);

            if(!response.success) {
                alert("Error al eliminar registro/s");
            }
        });

    }

    $("a[data-role='delete']").on("click", function(e) {

        e.preventDefault();

        // Collect IDs
        var data      = "id=" + $(this).parents(".data-item").first().data("id");
        // Get table
        data += "&table=" + $(this).parents(".data-list").first().data("table");
console.log(data);
        console.log($(this).parents(".data-item").first());
        // Pedir reconfirmar accion
        if (confirm("¿Eliminar este registro?")) {
            deleteItems(data);
            $(this).parents(".data-item").first().addClass("deleted").slideUp();
        }
    });
    
    
    /*
        DUPLICATE list elements
    */

    function duplicateItems(dataObj) {

        // Set delete process
        dataObj += "&process=duplicate-rows";

        console.log(dataObj);

        $.post("php/process_data.php", dataObj, function(data){

            var response = $.parseJSON(data);

            console.log(response.message);
            
            if(!response.success) {
                alert(response.error);
            } else {
                window.location.reload();
            }
        });

    }
    
    $("a[data-role='duplicate']").on("click", function(e) {

        e.preventDefault();

        // Collect IDs
        var data      = "id=" + $(this).parents(".data-item").first().data("id");
        // Get table
        data += "&table=" + $(this).parents(".data-list").first().data("table");
        
        // Pedir reconfirmar accion
        if (confirm("¿Duplicar este registro?")) {
            duplicateItems(data);
            //$(this).parents(".data-item").first().addClass("deleted").slideUp();
        }
    });

    $(".data-list-opts").on("click", ".delete-selected", function(e) {

        e.preventDefault();

        // Collect IDs
        var data      = $(".data-item .chk").serialize();
        // Get table
        data += "&table=" + $(".data-item .chk").parents(".data-list").data("table");

        // Pedir reconfirmar accion
        if (confirm("¿Eliminar los registros seleccionados?")) {
            deleteItems(data);
            $(".data-item .chk:checked").parents(".data-item").addClass("deleted").slideUp();
        }
    });

    /*
        SELECT list items
    */
    $(".data-item").each(function(){
        var $item = $(this);

        $item.on("change", ".chk", function(){
            $item.toggleClass("selected");
        })
    })

    $(".data-list-opts").on("change", ".chk", function(){
        if($(this).prop("checked")) {
            $(".data-item").addClass("selected").find(".chk").prop("checked", true);
        } else {
            $(".data-item").removeClass("selected").find(".chk").prop("checked", false);
        }
    })



    /*
        SORT list elements
    */
    $( ".sortable" ).each(function(index, element) {

        var $sort_list	= $(this);
        var sort_table	= $(this).data("table");
        var sort_order	= $(this).data("order");

        $sort_list.sortable({
            opacity		: .5,
            tolerance	: "pointer",
            handle		: ".handler",
            click: function(e){ console.log("I'm selected."); },
            mousedown: function(e){ console.log("I'm mousedown."); },

            // Al reordenar, actualizar DB
            update : function () {

                var ids_list = [];
                var pos_list = [];

                $sort_list.children().each(function(){

                    var elem = $(this);
                    //ids_list.push(elem.attr("id"));
                    ids_list.push(elem.data("id"));
                    pos_list.push(elem.data("pos"));

                })

                if(sort_order == "ASC") {
                    pos_list.sort(function(a, b){return a-b});
                }
                else {
                    pos_list.sort(function(a, b){return b-a});
                }

                var elem_list = {};
                for(i = 0; i<ids_list.length; i++) {

                    elem_list[ids_list[i]] = pos_list[i];

                }

                var elem_list_json = JSON.stringify(elem_list);

                var post_obj		 = "sorted_items=" + elem_list_json;
                post_obj			+= "&table=" + sort_table;
                post_obj			+= "&process=update-index";
                console.log(post_obj);

                $.post('php/process_data.php', post_obj, function(data) {
                    if(data != 1){ alert(data)}
                    console.log(data);
                });

            }
        });

    });

    /*
        SORT list elements
    */
    $( ".menu-level" ).each(function(index, element) {

        var $sort_list	= $(this);
        var sort_table	= $(this).data("table");
        var sort_order	= $(this).data("order");

        $sort_list.sortable({
            opacity		: .5,
            tolerance	: "pointer",
            handle		: ".handler",
            connectWith : ".menu-level",
            dropOnEmpty : true,

            // Al reordenar, actualizar DB
            update : function () {

                var ids_list        = [];
                var pos_list        = [];
                var parent_list     = [];

                $sort_list.children().each(function(){

                    var elem = $(this);
                    ids_list.push(elem.data("id"));
                    pos_list.push(elem.data("pos"));

                    var parent = elem.parents("li");
                    if(parent.length > 0) parent_list.push(elem.parents("li").data("id"))
                    else parent_list.push(null);

                })

                if(sort_order == "ASC") {
                    pos_list.sort(function(a, b){return a-b});
                }
                else {
                    pos_list.sort(function(a, b){return b-a});
                }

                var elem_list = [];
                for(i = 0; i<ids_list.length; i++) {
                    elem_list.push(ids_list[i] + "|" + pos_list[i] + "|" + parent_list[i]);
                }

                if(elem_list.length > 0) {
                    var elem_list_json = JSON.stringify(elem_list);

                    var post_obj		 = "sorted_items=" + elem_list_json;
                    post_obj			+= "&table=" + sort_table;
                    post_obj			+= "&process=update-menu-tree";
                    console.log(post_obj);

                    $.post('php/process_data.php', post_obj, function(data) {
                        if(data != 1){ alert(data)}
                        console.log(data);
                    });
                }
            }
        });

    });

    /*
        SEARCH list elements
    */
    $(".search-input .fa-search").on("click", this, function(){
        search();
    });

    $(".query").on("keydown", this, function(e){
        if(e.which == 13) {
            e.preventDefault();
            search();
        }
    });

    $(".reset-query").on("click", this, function(){

        // Si es una tabla con parent
        if ( $(".query").data('parent') )
            window.location = "?parent_id=" + $(".query").data('parent');
        else
            window.location = "?pag=1";


    });

    $(".doSearch").on("change", function(){
        search();
    });

    function search() {

        if($(".query").val().length < 3) {
            alert("Ingrese un término de búsqueda de al menos 3 caracteres.");
        } 

        else {
            // Si es una tabla con parent
            if ( $(".query").data('parent'))
                var str = "?parent_id=" + $(".query").data('parent') + "&";
            else
                var str = "?";

            str += "search=" + $(".query").data('rel') + "&val=" + $(".query").val();
            $(".doSearch").each(function(){
                str += "&" + $(this).attr("name") + "=" + $(this).val();
            })

            window.location = str;
        }
    }

    /*
        AUTO-UDATE FIELD

        Para convertir un campo en 'auto actualizable', esté donde esté el campo, tiene que:
        a). ser hijo de un data-item[data-id=XX] en donde XX sea el ID del registro a actualizar
        b). ser nieto de un data-list[data-table=XX] en donde XX sea el nombre de la tabla a actualizar
        c). contener un [name=XX] en donde XX es el nombre de la columna a actualizar
        d). contener la clase "auto-update"
    */
    $("body").on("change", ".auto-update", function () {

        var $item   = $(this).parents(".data-item");
        var id      = $item.data("id");
        var table   = $(this).parents(".data-list, .data-edit").data("table");

        var post_obj		 = $(this).serialize();
        post_obj			+= "&table=" + table;
        post_obj			+= "&process=update-row";
        post_obj			+= "&id=" + id;

        console.log(post_obj);

        $.post('php/process_data.php', post_obj, function(data) {
            console.log(data);
        });

    });

    /*
        SWITCH
        Checkbox boolean (1 || 0)
    */
    $(".toggler-val").each(function(){
        if($(this).val() == 1) $(this).siblings(".toggler-chk").prop("checked", true);
    })
    $(".toggler-chk").on("click", this, function(){
        var $input = $(this).siblings(".toggler-val");
        if ( $(this).prop("checked") ) $input.val(1);
        else $input.val(0);

        // Trigger 'onchange' event if checkbox is('.auto-update')
        if($input.is(".auto-update")) $input.trigger("change");
    });

    //
    // publish / draft
    //
    $(".chk-status").each(function(){
        if($(this).val() == "published") $(this).siblings(".chk-publish").prop("checked", true);
    })
    $(".chk-publish").on("click", this, function(){
        var $chk_status = $(this).siblings(".chk-status");
        if ( $(this).prop("checked") ) $chk_status.val("published");
        else $chk_status.val("draft");

        // Trigger 'onchange' event if checkbox is('.auto-update')
        if($chk_status.is(".auto-update")) $chk_status.trigger("change");
    });

    //
    // featured [1] / [0]
    //
    $(".chk-featured").each(function(){
        if($(this).val() == 1) $(this).siblings(".chk-star").attr('checked', 'checked');;
    })
    $(".data-item").each(function(){
        var $item = $(this);

        $item.on("change", ".chk-star", function(){

            var $chk_featured = $(this).siblings(".chk-featured");

            if($(this).is(":checked")) $chk_featured.val(1);
            else $chk_featured.val(0);

            // Trigger 'onchange' event if checkbox is('.auto-update')
            if($chk_featured.is(".auto-update")) $chk_featured.trigger("change");
        })
    })
    
    //
    // Icon toggler [1] / [0]
    //
    $(".chk-toggler").each(function(){
        if($(this).val() == 1) $(this).siblings(".chk-icon").attr('checked', 'checked');;
    })
    $(".data-item").each(function(){
        var $item = $(this);

        $item.on("change", ".chk-icon", function(){

            var $chk_featured = $(this).siblings(".chk-toggler");

            if($(this).is(":checked")) $chk_featured.val(1);
            else $chk_featured.val(0);

            // Trigger 'onchange' event if checkbox is('.auto-update')
            if($chk_featured.is(".auto-update")) $chk_featured.trigger("change");
        })
    })
    
    /*
    TOGGLER CONTENT
    -------------------------------------------------
    Muestra/Oculta campos de acuerdo a su data-tel y su val
    -------------------------------------------------
    */
    function updateToggleContent(rel, val) {
        // Reset all .toggler-content
        $(".toggler-content").stop().slideUp();
        // Show current .toggler-content
        $(".toggler-content[data-rel='"+rel+"']#" + val).stop().slideDown();
    }
    $(".toggler-trigger").each(function(){
        var rel = $(this) .data("rel");
        var val = $(this) .val();
        updateToggleContent(rel, val);
    });
    $(".toggler-trigger").on("change", function() {
        var rel = $(this) .data("rel");
        var val = $(this) .val();
        updateToggleContent(rel, val);
    })


    /*
    DATEPICKER
    https://github.com/wakirin/Lightpick
    -------------------------------------------------
    Range
    -------------------------------------------------
    */
    $(".datepicker-range").each(function() {

        var picker = new Lightpick({
            field: $(this).find(".datepicker-from")[0],
            secondField: $(this).find(".datepicker-to")[0],
            singleDate: false,
            format: 'YYYY-MM-DD',
            //repick: true,
            onSelect: function(start, end){
                //var str = '';
                //str += start ? start.format('Do MMMM YYYY') + ' to ' : '';
                //str += end ? end.format('Do MMMM YYYY') : '...';
                //document.getElementById('result-3').innerHTML = str;
            }
        });
    })
    /*
    -------------------------------------------------
    Single date
    -------------------------------------------------
    */

    $(".datepicker-single").each(function() {

        var picker = new Lightpick({
            field: $(this).find(".datepicker")[0],
            singleDate: true,
            format: 'YYYY-MM-DD',
        });
    })

    /*
        // Single date
    */


    

    


    /* *************************************
        TinyMCE
        v 5.0.12 (18-07-2019)
        https://www.tiny.cloud/docs/
    */
    tinymce.init({
        selector: ".tinymce",
        plugins: "link lists paste table image media",
        menubar: false,
        content_css : '/css/app.css',

        //https://www.tiny.cloud/docs/advanced/editor-control-identifiers/#toolbarcontrols
        //toolbar	: "undo redo | styleselect | alignleft aligncenter alignright alignjustify | bold italic underline forecolor | bullist numlist | link unlink | image paste | table",
        toolbar	: "undo redo | styleselect | alignleft aligncenter alignright alignjustify | bold italic underline | bullist numlist | link unlink | image paste ",
        toolbar_drawer: 'floating',

        // https://www.tiny.cloud/docs/plugins/paste/
        //paste_as_text: true,
        paste_data_images: true,

        // https://www.tiny.cloud/docs/plugins/table/
        table_grid : false,
        table_appearance_options: false,
        table_advtab: false,
        table_default_styles: {
            width: '100%'
        },
        table_default_attributes: {
            //border: '1'
        },

        images_upload_url: 'js/tinymce/uploader.php',
        automatic_uploads: true,
        relative_urls: false,
        images_upload_base_path: '/media/tinymce',
        images_reuse_filename: true,

        //content_css: "js/tinymce/css/content.css",

        setup: function(editor) {
            // onchange guarda el contenido en el textarea para poder ser validado
            editor.on('change', function(e) {
                tinymce.triggerSave();
            });
            editor.on('blur', function(e) {
                var id = this.id;
                if ($("#"+id).is(".auto-update"))
                    $("#"+id).trigger("change");

            });
        },

        //https://www.tiny.cloud/docs/demo/format-custom/
        style_formats: [
            {title: 'Título', block: 'h4'},
            {title: 'Subtítulo', block: 'h5'},
            {title: 'Párrafo', block: 'p'},
        ],

    });
    
    tinymce.init({
        selector: ".tinymce-simple",
        plugins: "link lists paste",
        menubar: false,
        content_css : '/css/app.css',

        //https://www.tiny.cloud/docs/advanced/editor-control-identifiers/#toolbarcontrols
        //toolbar	: "undo redo | styleselect | alignleft aligncenter alignright alignjustify | bold italic underline forecolor | bullist numlist | link unlink | image paste | table",
        toolbar	: "undo redo | bold italic underline | bullist numlist | link unlink ",
        toolbar_drawer: 'floating',

        setup: function(editor) {
            // onchange guarda el contenido en el textarea para poder ser validado
            editor.on('change', function(e) {
                tinymce.triggerSave();
            });
            editor.on('blur', function(e) {
                var id = this.id;
                if ($("#"+id).is(".auto-update"))
                    $("#"+id).trigger("change");

            });
        },

    });

    function strip_tags(input, allowed) {
        //  discuss at: http://phpjs.org/functions/strip_tags/
        //   example 1: strip_tags('<p>Kevin</p> <br /><b>van</b> <i>Zonneveld</i>', '<i><b>');
        //   returns 1: 'Kevin <b>van</b> <i>Zonneveld</i>'
        //   example 2: strip_tags('<p>Kevin <img src="someimage.png" onmouseover="someFunction()">van <i>Zonneveld</i></p>', '<p>');
        //   returns 2: '<p>Kevin van Zonneveld</p>'
        //   example 3: strip_tags("<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>", "<a>");
        //   returns 3: "<a href='http://kevin.vanzonneveld.net'>Kevin van Zonneveld</a>"
        //   example 4: strip_tags('1 < 5 5 > 1');
        //   returns 4: '1 < 5 5 > 1'
        //   example 5: strip_tags('1 <br/> 1');
        //   returns 5: '1  1'
        //   example 6: strip_tags('1 <br/> 1', '<br>');
        //   returns 6: '1 <br/> 1'
        //   example 7: strip_tags('1 <br/> 1', '<br><br/>');
        //   returns 7: '1 <br/> 1'

        allowed = (((allowed || '') + '')
                   .toLowerCase()
                   .match(/<[a-z][a-z0-9]*>/g) || [])
            .join(''); // making sure the allowed arg is a string containing only tags in lowercase (<a><b><c>)
        var tags = /<\/?([a-z][a-z0-9]*)\b[^>]*>/gi,
            commentsAndPhpTags = /<!--[\s\S]*?-->|<\?(?:php)?[\s\S]*?\?>/gi;
        return input.replace(commentsAndPhpTags, '')
            .replace(tags, function($0, $1) {
            return allowed.indexOf('<' + $1.toLowerCase() + '>') > -1 ? $0 : '';
        });
    }

    /*
        AUTO SLUG
    */
    $(".auto-slug").on("blur", function() {

        var str = encodeURIComponent($(this).val());

        if($(".slug").val() == "") {

            if($(".slug").is(".is-unique")) {
                var data = "table=" + $("[name='table']").val() + "&process=create-slug&slug=" + str + "&unique=true";
            } else {
                var data = "table=" + $("[name='table']").val() + "&process=create-slug&slug=" + str + "&unique=false";
            }

            $.post("php/process_data.php", data, function(data) {
                $(".slug").val(data);
            });
        };
    });
    /*
    $(".slug.unique").on("blur", function() {
        var str = $(this).val();
        console.log(str);

        var data = "table=" + $("[name='table']").val() + "&process=create-slug&slug=" + str + "&unique=true";
        $.post("php/process_data.php", data, function(data) {
            if( str != data) {
                //$(".slug.unique").focus();
                showResponse(false, "¡Atención! El slug '" + str + "' ya existe en otro registro y no puede estar duplicado.")
            }
        });
    });
*/
    /*
        UNIQUE VALUE
    */
    $(".is-unique").on("blur", function() {
        var $input  = $(this);
        check_availability($input);
    });

    $(".is-unique").on("blur keyup", function(e) {
        var $input  = $(this);
        reset_availab_error($input);
    });

    function check_availability($input) {
        var val     = $input.val();
        var column  = $input.attr('name');
        var table   = $("[name='table']").val();
        var id      = $("[name='id']").val();

        if(val != "") {

            var data = "table=" + table + "&process=check-availability&column=" + column + "&value=" + val + "&id=" + id;
            $.post("php/process_data.php", data, function(data) {

                var obj 	= $.parseJSON(data);
                if( obj.success) add_availab_error($input);
            });
        }
    }

    function add_availab_error($input) {
        $input.parents(".form-field").find("label").addClass("value-already-exists");
        $input.after("<span class='form-error value-duplicated-error' style='display:block !important'>¡Atención! Ya existe otro registro con este valor.</span>");
    }

    function reset_availab_error($input) {
        $input.parents(".form-field").find(".value-already-exists").removeClass("value-already-exists");
        $input.siblings(".value-duplicated-error").remove();
    }

    /*
        COLOR
    */
    $(".color-wrapper").each(function() {

        var $box = $(this).find(".color_box");
        var $code = $(this).find(".color_code");

        $box.val($code.val());

        $box.on("change", function(){
            var new_color = $(this).val();
            $code.val(new_color);
        })

        $code.on("change", function(){
            var new_color = $(this).val();
            $box.val(new_color);
        })

    });

});

// Create slug
var stripAccents = (function () {
    var in_chrs   = 'àáâãäçèéêëìíîïñòóôõöùúûüýÿ',
        out_chrs  = 'aaaaaceeeeiiiinooooouuuuyy',
        chars_rgx = new RegExp('[' + in_chrs + ']', 'g'),
        transl    = {}, i,
        lookup    = function (m) { return transl[m] || m; };

    for (i=0; i<in_chrs.length; i++) {
        transl[ in_chrs[i] ] = out_chrs[i];
    }

    return function (s) { return s.replace(chars_rgx, lookup); }

})();
function createSlug(string) {
    var string = string.toLowerCase();
    // Reemplazar acentos y eñe
    string = stripAccents(string);
    // Reemplazar espacios con guiones
    string = string.replace(/ /g, "-");
    // Eliminar todos los caracteres especiales restantes
    string = string.replace(/[^a-zA-Z0-9-]/g, "");
    // Reemplazar más de un guión por uno solo
    return string.replace(/-{2,}/g, "-");
}
