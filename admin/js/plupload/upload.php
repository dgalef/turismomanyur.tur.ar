<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once ("../../php/config.php");

// Si no existe archivo...
if (empty($_FILES) || $_FILES["file"]["error"]) {
    
    $resp = array();
    $resp["success"]    = 0;
    $resp["message"]    = "Archivo vacio. Posible error de max_file_size, post_max_size y/o upload_max_size";

    die(json_encode($resp));
    
}

// Si no se especificó el nombre de la clase...
if (empty($_POST['parent_class'])) {
    
    $resp = array();
    $resp["success"]    = 0;
    $resp["message"]    = "Error: No hay parent_class especificado.";

    die(json_encode($resp));
}

// Si IMagick no está instalado...
//if(!$im = system("type convert")) {
   // die('{"Error": IMagick no está disponible.}');
//};


$parent_class   = $_POST["parent_class"];
$parent_id      = (isset($_POST["parent_id"]) && $_POST["parent_id"] != "") ? $_POST["parent_id"] : NULL;
$label          = (isset($_POST["label"]) && $_POST["label"] != "") ? $_POST["label"] : NULL;

$fileName   = isset($_REQUEST["name"]) ? $_REQUEST["name"] : $_FILES["file"]["name"];

$fileExt    = strtolower(pathinfo($fileName, PATHINFO_EXTENSION));

// File type [image, video, document, audio, vcard, svg]
switch($fileExt) {

    case "jpg":
    case "gif":
    case "png":
    case "jpeg":
    case "svg":
        $file_type      = "images";
        break;

    case "mp4":
    case "mov":
        $file_type      = "videos";
        break;

    case "pdf":
    case "doc":
    case "docx":
        $file_type      = "documents";
        break;

    case "mp3":
        $file_type      = "audios";
        break;

    case "vcard":
        $file_type      = "vcards";
        break;

    default:
        $file_type      = "others";
        break;
}

if ($file_type == "images")
    $file_folder    = $file_type . "/src";
else
    $file_folder    = $file_type;

$folder		= str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . "/media/$file_folder");
if (!file_exists($folder)) mkdir($folder, 0777, true);

$chunk      = isset($_REQUEST["chunk"]) ? intval($_REQUEST["chunk"]) : 0;
$chunks     = isset($_REQUEST["chunks"]) ? intval($_REQUEST["chunks"]) : 0;


$filePath   = "$folder/$fileName";




function file_newname($path, $filename){
    if ($pos = strrpos($filename, '.')) {
        $name = urls_amigables(substr($filename, 0, $pos));
        $fileExt = substr($filename, $pos);
        $filename = $name . $fileExt;
    } else {
        $name = urls_amigables($filename);
        $filename = $name;
    }

    $newpath = $path.'/'.$filename;
    $newname = $filename;
    $counter = 0;
    while (file_exists($newpath)) {
        $newname = $name .'_'. $counter . $fileExt;
        $newpath = $path.'/'.$newname;
        $counter++;
    }

    return $newname;
}


$newName = file_newname($folder, $fileName);

// Ruta completa final
$newFilePath 		= $folder . '/' . $newName;


// Open temp file
$out = @fopen("{$filePath}.part", $chunk == 0 ? "wb" : "ab");
if ($out) {
    // Read binary input stream and append it to temp file
    $in = @fopen($_FILES['file']['tmp_name'], "rb");

    if ($in) {
        while ($buff = fread($in, 4096))
            fwrite($out, $buff);
    } else
        die('{"OK": 0, "info": "Failed to open input stream."}');

    @fclose($in);
    @fclose($out);

    @unlink($_FILES['file']['tmp_name']);
} else
    die('{"OK": 0, "info": "Failed to open output stream."}');


// Check if file has been uploaded
if (!$chunks || $chunk == $chunks - 1) {
    // Strip the temp .part suffix off
    rename("{$filePath}.part", $newFilePath);


    // Si es una imagen, crear diferentes instancias
    if($file_type == "images" && $fileExt != "svg") {

        $parent_obj = newClass($parent_class);
        
        if($label && isset( $parent_obj->images_sizes[$label]))
            $img_sizes = $parent_obj->images_sizes[$label];
        else
            $img_sizes = $parent_obj->images_sizes["default"];
            

        // Image sizes
        $imgsize        = getimagesize($newFilePath);
        $file_size_arr  = array();
        $file_size_arr["src"] = $imgsize[0] . "x" . $imgsize[1];

        foreach($img_sizes as $size => $params) {
            
            if((!$params['max_width'] || !$params['max_height']) && $params['best_fit']) {
                $resp = array("success" => 0, "message" => "ERROR in '$parent_class'' class image settings: 'best_fit' requires 'max_width' & 'max_height' not null");
                die(json_encode($resp));
            }
        
            $imag = new image($newFilePath);
            $imag->createImage($size, $params);

            $file_size_arr[$size] = $imag->width . "x" . $imag->height;
            
        }
    }

    $file_size = isset($file_size_arr) ? json_encode($file_size_arr) : NULL;

    // Crear registro en la DB
    $content_obj = new media();

    $arr			= array(
        'parent_class'      => $parent_class,
        'parent_id'         => $parent_id,
        'file_name'         => $newName,
        'file_source_name'  => $fileName,
        'file_ext'          => pathinfo($fileName, PATHINFO_EXTENSION),
        'file_type'         => $file_type,
        'file_sizes'        => $file_size,
        'media_title'       => '',
        'media_description' => '',
        'label'             => $label
    );

    $bool 			= $content_obj->create_row($arr);

    $resp = array();

    $resp["success"]    = ($bool["success"]) ? 1 : 0;
    $resp["message"]    = ($bool["success"]) ? "Archivo subido, registro creado con éxito." : $bool["error"];;
    $resp["file_path"]  = $newFilePath;
    $resp["file_name"]  = $newName;
    $resp["file_id"]    = $content_obj->lastAffected;
    $resp["file_pos"]   = $content_obj->lastPos;

    die(json_encode($resp));


}



?>
