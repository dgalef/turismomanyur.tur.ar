(function($) {

    $.fn.plup = function( options ) {

        // Establish our default settings
        var settings = $.extend({

            // Etiqueta que describe el uso del archivo (ej: "portada", "curriculum", "thumbnail")
            label           : "",

            chunk_size      : '25mb',

            // [string] Texto para el botón
            btn_txt			: "Subir archivo",

            // [string] Texto de ayuda
            help_text		: "Archivos JPG, GIF o PNG (hasta 2Mb).",

            // [object] Extensions allowed
            extensions		: "jpg,jpeg,gif,png",

            // [string] Tamaño máximo del archivo a subir
            // Deberá ser igual o inferior a upload_max_filesize y post_max_size de php.ini
            max_size		: "50mb",

            // [string] Nombre de la clase (tabla)
            parent_class	: null,

            // [string, number] ID del registro parent
            parent_id		: null,

            // [bool] Permitir upload de multiples archivos
            multi_files		: false,

            // Nombre del archivo existente en la base de datos
            current_file    : null,

            // [bool, string] Si es false, el archivo no mostrará un input para colocar el título.
            // Se se especifica un string, se usa de place holder (ej: <input placeholder='Colocar título'>)
            media_title      : "Título",

            // Acción que se llevará a cabo luego de subir los archivos [reload, show_media]
            post_action     : "show_media"

        }, options);

        //


        var contID = "plup" + Math.round(Date.now() + Math.random());
        $(this).attr("id", contID);

        //
        // PREPARAR DOM elements
        //

        // Crear IDs únicos
        var thWrapper	= contID + "-thum-wrapper",
            plWrapper	= contID + "-plup-wrapper",
            plContainer	= contID + "-container",
            plFilelist	= contID + "-filelist",
            plPickfiles	= contID + "-pickfiles",
            plConsole	= contID + "-console";
        

        // Colocal IDs
        $(this).find(".plupload-thumbnails").attr("id", thWrapper);
        $(this).find(".plupload-wrapper").attr("id", plWrapper);

        //
        // Crear elementos del uploader
        //
        $("#"+plWrapper).append("<div id='"+plFilelist+"' class='filelist'></div><div id='"+plContainer+"' class='plup-container'><a id='"+plPickfiles+"' class='button small hollow pickfiles' href='#'>" + settings.btn_txt + "</a></div><pre id='"+plConsole+"' class='console'></pre><p class='help-text'>" + settings.help_text + "</p>");

        //
        // Mostrar thumbmail si existe al inicializar el uploader
        //
        updateMedia();

        //
        // Init PLUPLOAD
        //
        var uploader = new plupload.Uploader({
            runtimes 		: 'html5,flash,html4',
            browse_button 	: plPickfiles,
            container		: document.getElementById(contID),
            url 			: '../js/plupload/upload.php',
            flash_swf_url 	: '../js/plupload/Moxie.swf',
            chunk_size      : settings.chunk_size,

            init: {
                PostInit	: function() {
                },

                FilesAdded	: function(up, files) {
                    console.log(new Date().toLocaleTimeString());
                    plupload.each(files, function(file) {
                        document.getElementById(plFilelist).innerHTML += '<div id="' + file.id + '">' + file.name + ' (' + plupload.formatSize(file.size) + ') <b></b></div>';
                    });
                    uploader.start();
                    uploader.disableBrowse();
                },

                UploadProgress: function(up, file) {
                    document.getElementById(file.id).getElementsByTagName('b')[0].innerHTML = '<span>' + file.percent + "%</span>";
                    console.log("prog")
                },

                Error: function(up, err) {
                    console.log("\nError #" + err.code + ": " + err.message);
                    document.getElementById(plConsole).innerHTML += "\nError #" + err.code + ": " + err.message;
                }
            }
        });

        // Custom options
        uploader.setOption("multi_selection", settings.multi_files);

        //Preparamos los parámetros que se enviarán al script
        var params = {
            parent_class	: settings.parent_class,
            label           : settings.label,
            parent_id		: settings.parent_id
          };
        uploader.setOption("multipart_params", params);

        uploader.setOption("filters", {
            max_file_size : settings.max_size,
            mime_types: [ { extensions: settings.extensions } ]
            
        });

        // Custom events
        uploader.bind('UploadFile', function(upldr, file) {
            $("a#"+plPickfiles).addClass("disabled");
        });


        var ok = true;

        uploader.bind('FileUploaded', function(upldr, file, object) {

            var response = $.parseJSON(object.response);

            if(response.success) {

                if(settings.post_action == "show_media")
                    updateMedia();
                
            }

            else {
                ok = false;
                alert(response.message);
            }
        });


        uploader.bind('UploadComplete', function(upldr, files) {

            // Called when all files are either uploaded or failed

            if(settings.post_action == "reload") {

                window.location.reload();

            } else {

                resetUploader();
            }
            
            console.log(new Date().toLocaleTimeString());

        });

        uploader.init();

        ///

        function updateMedia() {
            
            var obj  = "table=" + settings.parent_class + "&";
                obj += "process=get-media-rows&";
                obj += "parent_id=" + settings.parent_id + "&";
                obj += "label=" + settings.label + "&";
                obj += "media_title=" + settings.media_title;

            $.post("php/process_data.php", obj, function(data) {
                
                if (data != "") {
                    $("#" + thWrapper).html(data);
            
                    $("#" + thWrapper).removeClass("hide");

                    // Ocultar uploader si corresponde
                    if(!settings.multi_files) $("#" + plWrapper).addClass("hide");
                }
                
                else
                    console.log("No hay media");
            });
            
            
        }

        function resetUploader() {

            $("#" + plFilelist).html("");
            $("#" + plPickfiles).removeClass("disabled");
            uploader.disableBrowse(false);
        }

        function showUploader() {

            // Mostrar uploader
            $("#" + plWrapper).removeClass("hide");
        }

        function removeThum(file_id) {
            $(".data-item[data-rel='"+file_id+"']").remove();

            // Mostrar uploader si corresponde
            if($(".data-item").length < 1) $("#" + plWrapper).removeClass("hide");
        }

        function removeFile(file_id) {
            var dataObj = {
                process     : "delete-rows",
                table       : "media",
                id          : file_id
            }
            $.post("php/process_data.php", dataObj, function(data){

                var response = $.parseJSON(data);

                if(response.success) {
                    removeThum(file_id);
                    showUploader();
                }
            });
            return false;
        }

        $("#" + thWrapper).on("click", ".delete-image", function(e){
            e.preventDefault();
            if (confirm("¿Eliminar el registro seleccionado?")) {
                removeFile($(this).attr("data-rel"));
            }
        });



    }

}(jQuery));
