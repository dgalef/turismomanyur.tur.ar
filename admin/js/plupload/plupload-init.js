/*
    PLUPLOAD
*/

$(function() {
    
    $(".pluploader").each(function(){

        if($("body").is(".media-pg")) {
            var action_after_upload = "reload";
        } else {
            var action_after_upload = "show_media";
        }

        $plup = $(this);

        $plup.plup({
            parent_id 		: $plup.data("id"),
            parent_class	: $plup.data("class"),
            //file_type       : $plup.data("file-type"),
            multi_files     : $plup.data("multifiles"),
            label           : $plup.data("label"),
            media_title      : $plup.data("media-title"),
            extensions  	: $plup.data("extensions"),
            btn_txt			: $plup.data("btn-text"),
            help_text		: $plup.data("help-text"),
            //current_file    : $plup.data("current-file"),
            post_action     : action_after_upload
        })
    });
    
});