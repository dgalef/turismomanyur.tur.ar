<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "slider";
$page_title             = _("slide");

$show_back_button       = true;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");

$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    // If there's no ID, a new record is created;
    $result = $obj->create_row(array());
    if($result['success']) {
        $id = $obj->lastAffected;
        $new_location = basename($_SERVER['REQUEST_URI']);
        if($parent_id) $new_location .= "&id=" . $id;
        else $new_location .= "?id=" . $id;
        header("Location: " . $new_location);
    } else {
        die($result['error']);
    }
}

$data 		            = $obj->get_row($id);

$page_title             = ($data->status == 'temp') ? sprintf(_("Agregar %s"), $page_title) : sprintf(_(" Editar %s"), $page_title);

if (!$data) die("ID does not exist");

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Volver al listado de registros
$volver_url   = $class_name;
if($parent_id) $volver_url .= "?parent_id=$parent_id";

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                        <div class="form-inner">
                           
                            <!-- Título -->
                            <div class="form-field">
                                <label><?php echo _("Título"); ?>
                                    <input type="text" class="auto-slug" name="title" value="<?php echo escape($data->title); ?>" required>
                                    <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                </label>
                                <p class="help-text"><?php echo _("Usar etiqueta &lt;br&gt; para forzar un salto de líena."); ?></p>
                            </div>

                            <div class="form-field">
                                <label><?php echo _("Copete"); ?>
                                    <input type="text" class="auto" name="subtitle" value="<?php echo escape($data->subtitle); ?>">
                                    <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                </label>
                                <p class="help-text"><?php echo _("Frase corta que aparece sobre el título. Ideal para poner una categoría o similar. Ej: 'Salidas grupales'"); ?></p>
                            </div>
                            
                            <div class="form-field">
                                <label><?php echo _("Leyenda extra"); ?>
                                    <input type="text" class="auto" name="subtitle_2" value="<?php echo escape($data->subtitle_2); ?>">
                                    <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                </label>
                                <p class="help-text"><?php echo _("Aparecerá debajo del título. Ideal para poner un precio."); ?></p>
                            </div>

                            <div class="form-row">
                                
                                <div class="form-field">
                                    <label><?php echo _("Botón / texto"); ?>
                                        <input type="text" class="auto" name="cta_text" value="<?php echo escape($data->cta_text); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                    <p class="help-text"><?php echo _("Ej: 'Ver más' o 'Más info'."); ?></p>
                                </div>
                                
                                <div class="form-field">
                                    <label><?php echo _("Botón / link"); ?>
                                        <input type="text" class="" name="cta_link" value="<?php echo escape($data->cta_link); ?>">
                                        <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                                    </label>
                                    <p class="help-text"><?php echo _("URL destino. Puede ser con absoluta (con protocolo, ej: https://turismomanyur.tur.ar/destinos/argentina) o relativa (ej: '/destinos/argentina')."); ?></p>
                                </div>
                            </div>

                            <!-- Media files -->
                            <div class="form-field">
                                <label><?php echo _("Foto"); ?></label>
                                <?php

                                // Allow multifiles upload
                                $multi          = "false";
                                // Allowed extensions
                                $exts           = "jpg,jpeg,gif,png";
                                // Tag for media file (e.g: "cover", "thumbnail", etc)
                                $label          = "slide";
                                // Hint for the user
                                $help           = _("Formatos aceptados: JPG (recomendado), GIF y PNG. Medida mínima recomendada: 1600 x 620 pixeles");
                                // Show input[name='media_title'] -> (false or string)
                                $media_title    = "false";

                                echo "<div class='pluploader' data-id='$id' data-class='$class_name' data-order='$media_obj->sort_order' data-label='$label' data-media-title='$media_title' data-multifiles='$multi' data-extensions='$exts' data-btn-text='Subir archivo' data-help-text='$help'>
                                        <ul class='plupload-thumbnails data-list hide sortable' data-table='media' data-order='$media_obj->sort_order'></ul>
                                        <div class='plupload-wrapper'></div>
                                    </div>";

                                ?>

                            </div>
                            
                        </div>

                        <!-- ---------->

                        <?php if($parent_id) echo "<input type='hidden' name='parent_id' value='$parent_id'>"; ?>
                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <a class="button hollow" href="<?php echo $volver_url; ?>"><i class="far fa-arrow-left"></i><?php echo _("Volver"); ?></a>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <div class="bottom-block">
                                    <label class="publicar-label" for=""><?php echo _("Publicar:"); ?></label>
                                    <?php $status = ($data->status == 'temp') ? "draft" : $data->status; ?>
                                    <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                                        <input type="hidden" name="status" class="chk-status" value="<?php echo $status; ?>">
                                        <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                        <label for="<?php echo "status-$id"; ?>">
                                            <div class="can-toggle__switch" data-checked="\f00c" data-unchecked="NO"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <button class="button" type="submit"><?php echo _("Guardar"); ?></button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>"; ?>
                            <a class="popup-btn-otro button hollow" href="<?php echo $add_file; ?>">
                                <?php echo _("Añadir otro"); ?>
                            </a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>">
                                <?php echo _("Aceptar"); ?>
                            </a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()">
                                <?php echo _("Cerrar"); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->

</body>
</html>
