<?php
require_once "php/config.php";
?>
<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>" style="height:100%">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="login-pg">

    <div class="login-wrapper">

        <div class="login-content text-center">

            <h4>
                <?php echo $adminTitle; ?><br>
                <small><?php echo $adminVersion; ?></small>
            </h4>

            <img class="logo" src="img/logo.svg" style="max-width:300">

            <form id="forgot-pass-form" class="login" data-abide novalidate>

                <div class="grid-x grid-padding-x small-padding-collapse">
                    <div class="medium-12 cell text-center">

                        <div data-abide-error class="alert callout" style="display: none;">
                            <p><i class="fi-alert"></i> Ingresar un email</p>
                        </div>

                    </div>

                    <div class="medium-12 cell">
                        <h3>Restablecer contraseña</h3>
                        <p>
                            Se enviará un correo con un link para poder restablecer la contraseña.
                        </p>
                    </div>

                    <div class="medium-12 cell">
                        <label>Ingresar email/usuario
                            <input id="adminuser" name="adminuser" type="text" required>
                        </label>
                    </div>

                    <div class="medium-12 cell">
                        <input type="hidden" name="form-name" value="forgot">
                            <button
                                    id="submit" type="submit"
                                    class="g-recaptcha button"
                                    >
                                ENVIAR
                            </button>
                    </div>

                    <div class="medium-12 cell">
                        <div class="response">

                        </div>
                    </div>

                </div>

            </form>





        </div>



    </div>

    <p class="author"><?php echo $adminCopy; ?></p>

    <?php require_once "inc/footer.php"; ?>

    <!-- Invisible ReCaptcha
    <script src="https://www.google.com/recaptcha/api.js?onload=checkCaptcha&render=explicit" async defer></script>
    <script>

        // Invisible ReCaptcha
        var current_widget_id;
        //
        function checkCaptcha() {

            $(".g-recaptcha").each(function() {
                var object = $(this);
                var widgetID;
                widgetID = grecaptcha.render(object.attr("id"), {
                    "sitekey" : "6LcZLIwUAAAAAIsCRYD_LP5V7IuURNto4quOSxCz", // myadminpanel
                    //"badge" : "inline",
                    "callback" : function(token) {

                        var form = object.parents('form');
                        form.find(".g-recaptcha-response").val(token);

                        current_widget_id = widgetID;
                        console.log("current_widget_id: " + current_widget_id);
                        // Goes through a form and if there are any invalid inputs, it will display the form error element
                        // Fires these events: Abide#event:formvalid Abide#event:forminvalid
                        form.foundation('validateForm');

                    }
                });
            });
        };

    </script>
-->
</body>

</html>
