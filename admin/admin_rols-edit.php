<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "admin_rols";
$page_title             = _("rol");

$show_back_button       = true;

//

$obj                    = newClass($class_name);
$media_obj              = newClass("media");

$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if(isset($_GET['id'])) {
    $id = $_GET['id'];
} else {
    // If there's no ID, a new record is created;
    $result = $obj->create_row(array());
    if($result['success']) {
        $id = $obj->lastAffected;
        $new_location = basename($_SERVER['REQUEST_URI']);
        if($parent_id) $new_location .= "&id=" . $id;
        else $new_location .= "?id=" . $id;
        header("Location: " . $new_location);
    } else {
        die($result['error']);
    }
}

$data 		            = $obj->get_row($id);

$page_title             = ($data->status == 'temp') ? sprintf(_("Agregar %s"), $page_title) : sprintf(_(" Editar %s"), $page_title);

if (!$data) die("No existe el ID");

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Volver al listado de registros
$volver_url   = $class_name;
if($parent_id) $volver_url .= "?parent_id=$parent_id";

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="edit-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                    </div>
                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <form name="editForm" id="edit-form" data-abide novalidate>

                        <!-- Título -->
                        <div class="form-field">
                            <label><?php echo _("Título"); ?>
                                <input type="text" class="" name="title" value="<?php echo escape($data->title); ?>" required>
                                <span class="form-error"><?php echo _("Campo requerido"); ?></span>
                            </label>
                        </div>

                        

                        <!-- ---------->

                        <?php if($parent_id) echo "<input type='hidden' name='parent_id' value='$parent_id'>"; ?>
                        <input type="hidden" name="table" value="<?php echo $class_name; ?>">
                        <input type="hidden" name="process" value="update-row">
                        <input type="hidden" name="id" value="<?php echo $data->id; ?>">

                        <div class="bottom-fixed">
                            <div class="bottom-fixed-left">
                                <div class="bottom-block">
                                    <a class="button hollow" href="<?php echo $volver_url; ?>"><i class="far fa-arrow-left"></i><?php echo _("Volver"); ?></a>
                                </div>
                            </div>
                            <div class="bottom-fixed-right">
                                <div class="bottom-block">
                                    <label class="publicar-label" for=""><?php echo _("Activar:"); ?></label>
                                    <div class="checkbox-switch can-toggle can-toggle-size-medium demo-rebrand-2">
                                        <input type="hidden" name="status" class="chk-status" value="<?php echo $data->status; ?>">
                                        <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                        <label for="<?php echo "status-$id"; ?>">
                                            <div class="can-toggle__switch" data-checked="\f00c" data-unchecked="NO"></div>
                                        </label>
                                    </div>
                                </div>
                                <div class="bottom-block">
                                    <button class="button" type="submit"><?php echo _("Guardar"); ?></button>
                                </div>
                            </div>
                        </div>

                    </form>

                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                            <?php if($show_back_button) echo "<a class='popup-btn-volver button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>"; ?>
                            <a class="popup-btn-otro button hollow" href="<?php echo $add_file; ?>">
                                <?php echo _("Añadir otro"); ?>
                            </a>
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-aceptar button" href="<?php echo basename($_SERVER['REQUEST_URI']); ?>">
                                <?php echo _("Aceptar"); ?>
                            </a>
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()">
                                <?php echo _("Cerrar"); ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->


</body>
</html>
