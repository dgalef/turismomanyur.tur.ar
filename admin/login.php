<?php
session_start();
require_once "php/config.php";
?>
<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>" style="height:100%">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="login-pg">

    <div class="login-wrapper">

        <div class="login-content text-center">

            <h4>
                <?php echo $adminTitle; ?><br>
            </h4>
            
            <?php
            if (sizeof($available_langs) > 1) {
                if ($lang == "es")
                    echo "<a class='lang-selector' href='login?lang=en'>English version</a>";
                else 
                    echo "<a class='lang-selector' href='login?lang=es'>Versión en español</a>";
            }
            ?>
            
            
            <img class="logo" src="img/logo.svg" style="max-width:300">

            <form id="login-form" class="login" data-abide novalidate>

                <div class="grid-x grid-padding-x small-padding-collapse">
                    <div class="medium-12 cell text-center">

                        <div data-abide-error class="alert callout" style="display: none;">
                            <p><i class="fi-alert"></i> <?php echo _("Alguno de los datos es incorrecto."); ?></p>
                        </div>

                    </div>

                    <div class="medium-12 cell">
                        <label>
                            <?php echo _("Usuario"); ?>
                            <input id="adminuser" name="adminuser" type="text" required>
                        </label>
                    </div>

                    <div class="medium-12 cell">
                        <label>
                            <?php echo _("Contraseña"); ?>
                            <input id="adminpass" name="adminpass" type="password" required>
                        </label>
                    </div>
                    
                    <div class="medium-12 cell">
                            <input type="hidden" name="form-name" value="login">
                            <button
                                    id="submit" type="submit"
                                    class="g-recaptcha button"
                                    >
                                <?php echo _("ACCEDER"); ?>
                            </button>
                    </div>

                </div>

            </form>

            <div class="login-footer">
                <a href="forgot-pass.php"><?php echo _("Recuperar la contraseña"); ?></a>
            </div>

        </div>



    </div>

    <p class="author"><?php echo $adminVersion . " - " . $adminCopy; ?></p>

    <?php require_once "inc/footer.php"; ?>

    <!-- Invisible ReCaptcha
    <script src="https://www.google.com/recaptcha/api.js?onload=checkCaptcha&render=explicit" async defer></script>
    <script>

        // Invisible ReCaptcha
        var current_widget_id;
        //
        function checkCaptcha() {

            $(".g-recaptcha").each(function() {
                var object = $(this);
                var widgetID;
                widgetID = grecaptcha.render(object.attr("id"), {
                    "sitekey" : "6LcZLIwUAAAAAIsCRYD_LP5V7IuURNto4quOSxCz", // myadminpanel
                    //"badge" : "inline",
                    "callback" : function(token) {

                        var form = object.parents('form');
                        form.find(".g-recaptcha-response").val(token);

                        current_widget_id = widgetID;
                        console.log("current_widget_id: " + current_widget_id);
                        // Goes through a form and if there are any invalid inputs, it will display the form error element
                        // Fires these events: Abide#event:formvalid Abide#event:forminvalid
                        form.foundation('validateForm');

                    }
                });
            });
        };

    </script>
-->
</body>

</html>
