<?php

/**
* Name:  Google Invisible reCAPTCHA
*
* Author: Geordy James
*         @geordyjames
*
* Location: https://github.com/geordyjames/google-Invisible-reCAPTCHA

* Created:  13.03.2017

* Created by Geordy James to make a easy version of google Invisible reCAPTCHA PHP Library
*
* Description:  This is an unofficial version of google Invisible reCAPTCHA PHP Library
*
*/

class Recaptcha{

    private $secret_key;

    public function __construct($key){
        $this->secret_key = $key;
    }

    public function verifyResponse($recaptcha){

        $remoteIp = $this->getIPAddress();

        // Discard empty solution submissions
        if (empty($recaptcha)) {
            return array(
                'success' => false,
                'error-codes' => 'missing-input',
            );
        }

        $getResponse = $this->getHTTP(
        //$getResponse = $this->getCurl(
            array(
                'secret' => $this->secret_key,
                'remoteip' => $remoteIp,
                'response' => $recaptcha,
            )
        );

        // get reCAPTCHA server response
        $responses = json_decode($getResponse, true);


        if (isset($responses['success']) and $responses['success'] == true) {
            $status = true;
        } else {
            $status = false;
            $error = (isset($responses['error-codes'])) ? $responses['error-codes']
                : print_r($responses);
        }

        return array(
            'success' => $status,
            'error-codes' => (isset($error)) ? $error : null,
        );
    }


    private function getIPAddress(){
        if (!empty($_SERVER['HTTP_CLIENT_IP']))
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
        else
        {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    private function getHTTP($data){

        // TO USE IN LOCALHOST

        // Option 1: Disable SSL
        // Ref: https://stackoverflow.com/questions/26148701/file-get-contents-ssl-operation-failed-with-code-1-and-more#answer-26151993
        /*
        $arrContextOptions=array(
            "ssl"=>array(
                "verify_peer"=>false,
                "verify_peer_name"=>false,
            ),
        );
        $url = 'https://www.google.com/recaptcha/api/siteverify?'.http_build_query($data);
        $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
        */

        // Option 2: Use along with cacert.pem (file must be in the same folder of this script)
        // Ref: https://stackoverflow.com/questions/26148701/file-get-contents-ssl-operation-failed-with-code-1-and-more#answer-28701786
        /*
        $arrContextOptions=array(
            "ssl"=>array(
                "cafile" => "cacert.pem",       // -> downloaded from https://curl.haxx.se/docs/caextract.html
                "verify_peer"=> true,
                "verify_peer_name"=> true,
            ),
        );
        $url = 'https://www.google.com/recaptcha/api/siteverify?'.http_build_query($data);
        $response = file_get_contents($url, false, stream_context_create($arrContextOptions));
        */


        // TO USE IN REMOTE

        $url = 'https://www.google.com/recaptcha/api/siteverify?'.http_build_query($data);
        $response = file_get_contents($url);

        return $response;
    }

    /*
    private function getCurl($data) {
        $ch = curl_init();

        curl_setopt_array($ch, [
            CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $data,
            CURLOPT_RETURNTRANSFER => true
        ]);

        $output = curl_exec($ch);
        curl_close($ch);

        //$json = json_decode($output);
        return $output;
    }
    */
}

