<?php

session_start();
include_once("config.php");

if (!isset($_POST['table']) || !isset($_POST['process']) ) {

    echo _('No se especifico nombre de tabla o tipo de proceso.');
    exit;
}

$table			= $_POST['table'];
$process 		= $_POST['process'];

$obj = newClass($table);

function prepareArray($arr) {

    // Eliminar valores que no corresponden a las columnas de la tabla en la DB
    unset($arr["table"]);
    unset($arr["process"]);
    unset($arr["onoffswitch"]);

    return $arr;
}

if ($process == "create-row") {

    $arr 			= prepareArray($_POST);

    $result 		= $obj->create_row($arr);

    echo json_encode($result);

    exit;

} // create-row

else if ($process == "update-row") {

    $arr 			= prepareArray($_POST);

    $result			= $obj->update_row($arr, $_POST['id']);

    echo json_encode($result);

    exit;

} // update-row

else if ($process == "update-col") {


    $mssg = $obj->update_col($_POST['col'], $_POST['new_val'], $_POST['item_id']);
    $newId = $_POST['item_id'];

    echo json_encode(array(
                            "mssg" => "$mssg",
                            "newId" => "$newId"
                            ));
} // update-col

else if ($process == "duplicate-rows") {

    $arr 			= $_POST['id'];

    if(is_array($arr)) {
        foreach($arr as $id) {
            // Borrar registro en la DB
            $result 		= $obj->duplicate_row($id);
        }
    }
    else {
        // Borrar registro en la DB
        $result 		= $obj->duplicate_row($arr);
    }

    echo json_encode($result);

    exit;

} // delete-rows

else if ($process == "delete-rows") {

    $arr 			= $_POST['id'];

    if(is_array($arr)) {
        foreach($arr as $id) {
            // Borrar registro en la DB
            $bool 		= $obj->delete_row($id);
        }
    }
    else {
        // Borrar registro en la DB
        $bool 		= $obj->delete_row($arr);
    }

    $newId 			= $obj->lastAffected;

    $resp 			= array();
    $resp["success"]= $bool;		// TRUE or FALSE

    if($bool) $resp["mssg"] = _("La operación ha sido realizada con éxito.");
    else $resp["mssg"] 	= _("Error al actualizar el registro.");

    $resp["newId"] 	= $newId;

    echo json_encode($resp);

    exit;

} // delete-rows

else if ($process == "get-media-rows") {

    $arr 			= prepareArray($_POST);

    $result 		= $obj->get_media_rows($_POST["parent_id"], $_POST["label"], $_POST["media_title"]);

    echo $result;

    exit;

} // create-row

else if ($process == "create-relation") {

    // Si el tag no existía y el usuario lo está creando, crear primero el tegistro en la tabla correspondiente
    if ($_POST['tag_id'] == 'temp_tag_id') {

        $tags_table = $_POST['tag_ref'];
        $tags_obj   = newClass($tags_table);
        $tags_obj->auto_slug = true;
        $tag_title  = $_POST['tag_title'];

        $bool = $tags_obj->create_row(['title' => $tag_title, 'slug' => '', 'status' => 'draft']);

        if($bool)
            $tag_id = $tags_obj->lastAffected;
        else
            return ["success" => false, "mssg" => _("No se pudo crea el tag")];
        
    }

    // Si el tag ya existía (el usuario lo seleccionó del listado precargado)
    else {
        $tag_id 			= $_POST['tag_id'];
    }

    $art_ref 			= $_POST['art_ref'];
    $art_id 			= $_POST['art_id'];
    $tag_ref 			= $_POST['tag_ref'];
    $multiple           = $_POST['multiple'];
    
    // Remover una posible relacion anterior
    if ($multiple == "false")
        $obj->delete_relation($art_ref, $art_id, $tag_ref, null);

    // Crear relación en la tabla de relaciones
    $bool = $obj->create_row(['art_ref' => $art_ref, 'art_id' => $art_id, 'tag_ref' => $tag_ref, 'tag_id' => $tag_id]);

    $resp 			= array();
    $resp["success"]= $bool;		// TRUE or FALSE

    if($bool) $resp["mssg"] = _("La relación ha sido creada.");
    else $resp["mssg"] 	= _("Error al crear la relación.");

    $resp["newId"] 	= (isset($tag_id)) ? $tag_id : $obj->lastAffected;

    echo json_encode($resp);

    exit;


} // create-relation

else if ($process == "delete-relation") {

    $art_ref 			= $_POST['art_ref'];
    $art_id 			= $_POST['art_id'];
    $tag_ref 			= $_POST['tag_ref'];
    $tag_id 			= $_POST['tag_id'];

    // Borrar registro en la DB
    $bool 		    = $obj->delete_relation($art_ref, $art_id, $tag_ref, $tag_id);

    $newId 			= $obj->lastAffected;

    $resp 			= array();
    $resp["success"]= $bool;		// TRUE or FALSE

    if($bool) $resp["mssg"] = _("La relación ha sido eliminada.");
    else $resp["mssg"] 	= _("Error al eliminar la relación.");

    $resp["newId"] 	= $newId;

    echo json_encode($resp);

    exit;

} // delete-relation

else if ($process == "update-menu-tree") {

    $sorted_items = json_decode($_POST["sorted_items"]);

    $mssg = "";
    foreach ($sorted_items as $item) {
        $i      = explode("|", $item);
        $pos    = $i[0];
        $id     = $i[1];
        $parent = $i[2];
        $mssg   = $obj->update_menu_tree($id, $pos, $parent);
    }

    echo $mssg;

} // update-menu-tree


else if ($process == "update-index") {

    $sorted_items = json_decode($_POST["sorted_items"]);

    foreach ($sorted_items as $k => $v) {
        $mssg = $obj->update_index_from($v, $k);
    }

    echo $mssg;

} // update-index

else if ($process == "create-slug") {

    if ($_POST["unique"] == "true")
        echo $obj->create_unique_slug($_POST["slug"]);
    else
        echo $obj->urls_amigables($_POST["slug"]);

    exit;

} // create-slug

else if ($process == "check-availability") {

    $result = $obj->check_availability($_POST['column'], $_POST['value'], $_POST['id']);

    echo json_encode($result);

    exit;
}

else {
    echo _("El proceso no es válido.");
}

///////////////////////////////


?>
