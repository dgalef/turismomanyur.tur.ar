<?php

/* CONNECT TO DB */
switch($_SERVER['HTTP_HOST']) {

    // Localhost
    case "manyur":
    case "localhost":
        $db_host = 'localhost';
        $db_name = 'manyur_db';
        $db_user = 'root';
        $db_pass = '';
        break;

    case "manyur.teste.ar":
        $db_host = 'localhost';
        $db_name = 'estratest_manyur';
        $db_user = 'estratest_admin';
        $db_pass = 'test@28327';
        break;

    // Remoto
    default:
        $db_host = 'localhost';
        $db_name = 'turismom_db';
        $db_user = 'turismom_admin';
        $db_pass = 'V8Y-&yH20UZQ';
        break;
}


try {

    // Common servers
    $driver_options = array( PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'" );

    $db = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass, $driver_options);

    $db->exec("SET NAMES 'utf8'; SET lc_messages = 'es_ES'");
    
    /* Set error mode */
    //$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT );
    //$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
    $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

}
catch(PDOException $e) {
    echo $e->getMessage();
    die();
}



?>
