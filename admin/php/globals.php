<?php
/*
    Configura variables y funciones útiles para utilizar en cualquier parte de la aplicación

*/

$sel		= "selected";
$chk		= "checked";

// Convierte caracteres especiales en entidades html (ej: 'ñ' -> '$ntilde;')
function escape($string = null) {
    $str = htmlspecialchars($string, ENT_QUOTES);
    return $str;
}

/*
    Recibe un string con formato HTML y quita los TAGS con excepción de los permitidos.
    Además, limpia los atributos de los TAGS que quedan.

    $str			= [string] Un string con formato HTML
    $allowed_tags	= [string] Ej: "<p><br><b>" TAGS que no serán eliminados de $str
    $clean_attr		= [bool] Indica si se eliminan los atributos de los TAGS permitidos. Ej: <p style='color:red'>
*/
function clean_html($str, $allowed_tags, $clean_attr = true) {
    $txt = strip_tags($str, $allowed_tags);
    return $txt;
    //return preg_replace("/<([a-z][a-z0-9]*)[^>]*?(\/?)>/i",'<$1$2>', $txt);
}

// Fechas
date_default_timezone_set('America/Argentina/Buenos_Aires');

function now($format = "Y-m-d") {
    $str = date($format);
    return $str;
}


/*
    References:
    http://php.net/manual/en/function.date.php
    http://php.net/manual/es/datetime.format.php
    http://php.net/manual/es/function.strftime.php

    http://lachabela.wordpress.com/2012/02/24/fechas-en-espanol-con-php-y-setlocale/

    Ejemplo:
        "Y-m-d H:i:s"	-->		2014-10-24 15:35:05	[sólo para columna del tipo 'datetime']
        "d/m/Y"			-->		24/10/2014 [para columna del tipo 'date' o 'datetime']
*/

function format_date($date, $format = "Y-m-d") {

    if(!$date) {
        return NULL;
    }

    $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
    $meses_cortos = array("Ene","Feb","Mar","Abr","May","Jun","Jul","Ago","Sep","Oct","Nov","Dic");
    $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");

    //echo $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') ;

    $temp_date = date_create($date);

    // Formateo personalizado de la fecha completa
    if($format == "fulldate") {

        $str = "";
        // Nombre del día:
        //$str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        $str .= date_format($temp_date, 'd');
        // Nexo
        $str .= " de ";
        // Nombre del mes (ej: Octubre)
        $str .= $meses[date_format($temp_date, 'n')-1];
        // Nexo
        $str .= " de ";
        // Año (ej: 2014)
        $str .= date_format($temp_date,'Y');
    }
    
    else if($format == "nom-num-dia") {

        $str = "";
        // Nombre del día:
        $str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        $str .= date_format($temp_date, 'd');
        // Nexo
        //$str .= " de ";
        // Nombre del mes (ej: Octubre)
        //$str .= $meses[date_format($temp_date, 'n')-1];
        // Nexo
        //$str .= " de ";
        // Año (ej: 2014)
        //$str .= date_format($temp_date,'Y');
    }

    else if($format == "nom-dia-mes") {

        $str = "";
        // Nombre del día:
        $str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        $str .= date_format($temp_date, 'd');
        // Nexo
        $str .= " de ";
        // Nombre del mes (ej: Octubre)
        $str .= $meses[date_format($temp_date, 'n')-1];
        // Nexo
        //$str .= " de ";
        // Año (ej: 2014)
        //$str .= date_format($temp_date,'Y');
    }
    
    else if($format == "nom-mes") {

        $str = "";
        // Nombre del día:
        //$str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        //$str .= date_format($temp_date, 'd');
        // Nexo
        //$str .= " de ";
        // Nombre del mes (ej: Octubre)
        $str .= $meses[date_format($temp_date, 'n')-1];
        // Nexo
        //$str .= " de ";
        // Año (ej: 2014)
        //$str .= date_format($temp_date,'Y');
    }
    
    else if($format == "nom-mes-corto") {

        $str = "";
        // Nombre del día:
        //$str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        //$str .= date_format($temp_date, 'd');
        // Nexo
        //$str .= " de ";
        // Nombre del mes (ej: Octubre)
        $str .= $meses_cortos[date_format($temp_date, 'n')-1];
        // Nexo
        //$str .= " de ";
        // Año (ej: 2014)
        //$str .= date_format($temp_date,'Y');
    }
    
    else if($format == "num-dia") {

        $str = "";
        // Nombre del día:
        //$str .= $dias[date_format($temp_date, 'w')] . " ";
        // Número del día (ej: 05)
        $str .= date_format($temp_date, 'd');
        // Nexo
        //$str .= " de ";
        // Nombre del mes (ej: Octubre)
        //$str .= $meses[date_format($temp_date, 'n')-1];
        // Nexo
        //$str .= " de ";
        // Año (ej: 2014)
        //$str .= date_format($temp_date,'Y');
    }

    // Formateo estándar de la fecha (ej: 01/12/2014 - Referencias: http://php.net/manual/en/function.date.php)
    else {

        $str = date_format($temp_date, $format);
    }

    return $str;
}

/*
    Crea una cadena sin caracteres especiales
*/

function urls_amigables($url) {

//    $url 	= utf8_decode($url);

    // Tranformamos todo a minusculas
    $url 	= mb_strtolower($url);

    //Rememplazamos caracteres especiales latinos
    $find 	= array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ');
    $repl 	= array('a', 'e', 'i', 'o', 'u', 'n', 'a', 'e', 'i', 'o', 'u', 'n');
    $url 	= str_replace ($find, $repl, $url);

    // Añadimos los guiones
    $find 	= array(' ', '&', '\r\n', '\n', '+');
    $url 	= str_replace ($find, '-', $url);

    // Eliminamos y reemplazamos demás caracteres especiales
    $find 	= array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
    $repl 	= array('', '-', '');
    $url 	= preg_replace ($find, $repl, $url);

    return $url;

}


/**
 * truncateHtml can truncate a string up to a number of characters while preserving whole words and HTML tags
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 *
 * @return string Trimmed string.
 */
function truncateHtml($text, $length = 100, $ending = '...', $exact = false, $considerHtml = true) {
    if ($considerHtml) {
        // if the plain text is shorter than the maximum length, return the whole text
        if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
            return $text;
        }
        // splits all html-tags to scanable lines
        preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);
        $total_length = strlen($ending);
        $open_tags = array();
        $truncate = '';
        foreach ($lines as $line_matchings) {
            // if there is any html-tag in this line, handle it and add it (uncounted) to the output
            if (!empty($line_matchings[1])) {
                // if it's an "empty element" with or without xhtml-conform closing slash
                if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
                    // do nothing
                    // if tag is a closing tag
                } else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
                    // delete tag from $open_tags list
                    $pos = array_search($tag_matchings[1], $open_tags);
                    if ($pos !== false) {
                        unset($open_tags[$pos]);
                    }
                    // if tag is an opening tag
                } else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
                    // add tag to the beginning of $open_tags list
                    array_unshift($open_tags, strtolower($tag_matchings[1]));
                }
                // add html-tag to $truncate'd text
                $truncate .= $line_matchings[1];
            }
            // calculate the length of the plain text part of the line; handle entities as one character
            $content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
            if ($total_length+$content_length> $length) {
                // the number of characters which are left
                $left = $length - $total_length;
                $entities_length = 0;
                // search for html entities
                if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
                    // calculate the real length of all entities in the legal range
                    foreach ($entities[0] as $entity) {
                        if ($entity[1]+1-$entities_length <= $left) {
                            $left--;
                            $entities_length += strlen($entity[0]);
                        } else {
                            // no more characters left
                            break;
                        }
                    }
                }
                $truncate .= substr($line_matchings[2], 0, $left+$entities_length);
                // maximum lenght is reached, so get off the loop
                break;
            } else {
                $truncate .= $line_matchings[2];
                $total_length += $content_length;
            }
            // if the maximum length is reached, get off the loop
            if($total_length>= $length) {
                break;
            }
        }
    } else {
        if (strlen($text) <= $length) {
            return $text;
        } else {
            $truncate = substr($text, 0, $length - strlen($ending));
        }
    }
    // if the words shouldn't be cut in the middle...
    if (!$exact) {
        // ...search the last occurance of a space...
        $spacepos = strrpos($truncate, ' ');
        if (isset($spacepos)) {
            // ...and cut the text in this position
            $truncate = substr($truncate, 0, $spacepos);
        }
    }
    // add the defined ending to the text
    $truncate .= $ending;
    if($considerHtml) {
        // close all unclosed html-tags
        foreach ($open_tags as $tag) {
            $truncate .= '</' . $tag . '>';
        }
    }
    return $truncate;
}

/**
 * Grab the url of a publicly embeddable video hosted on vimeo
 * @param  str $video_url The "embed" url of a video
 * @return str            The url of the thumbnail, or false if there's an error
 */
function grab_vimeo_thumbnail($vimeo_url){
    if( !$vimeo_url ) return false;
    $data = json_decode( file_get_contents( 'https://vimeo.com/api/oembed.json?url=' . $vimeo_url ) );
    if( !$data || !isset($data->thumbnail_url) ) return false;
    return $data->thumbnail_url;
}

?>
