<?php

session_start();
require_once "config.php";
/*
require_once "Recaptcha.php";

$recaptcha = $_POST['g-recaptcha-response'];

$object = new Recaptcha('6LcZLIwUAAAAAPGKsU-o5PL4MT_Tr0HrJgd9Hshm');    // myadminpanel
$response = $object->verifyResponse($recaptcha);

// ReCpatcha inválido
if(isset($response['success']) and $response['success'] != true) {
    $err = "Error de ReCaptcha. Error code is :". print_r($response['error-codes']);

    $resp       = array(
        "success" => 0,
        "message" => $err
    );

    echo json_encode($resp);
}

// ReCpatcha válido
else {
*/

    $form = $_POST["form-name"];

    // Login
    if($form == "login") {

        $adminuser      = $_POST["adminuser"];
        $adminpass      = $_POST["adminpass"];

        // Chequear si el usuario existe
        $users_obj = new admin_users();
        $user = $users_obj->get_user($adminuser, $adminpass);

        if($user) {

            // published = activo
            if($user->status == "published") {

                unset($user->salt);
                unset($user->password);

                $_SESSION['adminuser'] = $user;

                $resp       = array(
                    "success" => 1,
                    "message" => "Login exitoso"
                );

                echo json_encode($resp);

            } else {

                $resp       = array(
                    "success" => 0,
                    "message" => "Usuario inactivo"
                );

                echo json_encode($resp);
            }

        }

        else
        {
            $resp       = array(
                "success" => 0,
                "message" => "Usuario o contraseña incorrectos"
            );

            echo json_encode($resp);

        }

    }

    else if($form == "forgot") {

        $users_obj = new admin_users();
        $user_row = $users_obj->get_row(null, "email", $_POST['adminuser']);

        if($user_row) {

            $send = send_mail($user_row);

            if($send) {

                $resp       = array(
                    "success" => 1,
                    "message" => "Mail enviado. Por favor revise su casilla."
                );

                echo json_encode($resp);


            } else {

                $resp       = array(
                    "success" => 0,
                    "message" => "No se pudo enviar el email."
                );

                echo json_encode($resp);
            }
        }

        else {

            $resp       = array(
                "success" => 0,
                "message" => "No existe el usuario " . $_POST['adminuser'] . "."
            );

            echo json_encode($resp);

        }

    }

    else if($form == "reset") {

        if(!empty($_POST["token"]))
        {
            $pass 	= $_POST["password"];
            $token 	= $_POST["token"];
            $users_obj = newClass("admin_users");
            $user_row = $users_obj->get_row(null, "password", $token);

            if(!$user_row) {

                $resp       = array(
                    "success" => 0,
                    "message" => "No hay user con ese tokena."
                );
            }

            else
            {

                $bool = $users_obj->reset_pass($user_row->id, $pass);

                if($bool) {

                    $resp       = array(
                        "success" => 1,
                        "message" => "La contraseña fue restablecida."
                    );


                }
                else {

                    $resp       = array(
                        "success" => 0,
                        "message" => "No se pudo restablecer la contraseña."
                    );

                }

                echo json_encode($resp);
                //exit;
            }
        }

    }

    function send_mail($u) {

        $nombre 	= $u->first_name;
        $email 		= $u->email;
        $token 		= $u->password;
        //$host		= "http://" . $_SERVER['HTTP_HOST'];
        $host       = ($_SERVER['HTTP_HOST'] == "myadminpanel") ? $_SERVER['HTTP_HOST'] : $_SERVER['HTTP_HOST'] . "/admin";
        $protocol   = $_SERVER['PROTOCOL'] = isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) ? 'https' : 'http';
        $path       = "$protocol://$host/reset-pass.php?token=$token";


        $subject 	= "Restablecer contraseña para $host";

        $contenido 	= "<font face='Arial' size='2'>";
        $contenido .= "<p><strong>Hola " . $nombre .":</strong></p>";
        $contenido .= "<p>Hacé click en el siguiente link para elegir una nueva contraseña:</p>";
        $contenido .= "<p><a href='$path'>RESTABLECER CONTRASEÑA</a></p>";
        $contenido .= "<p><em>Este es un email autogenerado. No responder.</em></p>";
        $contenido .= "</font>";


        $toMail		= $email;


        $header = "From: $nombre <" . $email . ">\n";
        $header .= "MIME-Version: 1.0 \n";
        $header .= "Content-Type: text/html;charset=utf-8";

        $bool = mail($toMail, $subject, $contenido ,$header);

        return $bool;


    }

/*
}
*/
?>
