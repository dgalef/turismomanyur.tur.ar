<?php
require_once "langs.php";
require_once "connection.php";
require_once "globals.php";

// Corrije un error de algunos servidores que no tienen disponible la variable $_SERVER['DOCUMENT_ROOT']
if ( ! isset($_SERVER['DOCUMENT_ROOT'] ) ) {
    $_SERVER['DOCUMENT_ROOT'] = str_replace( '\\', '/', substr($_SERVER['SCRIPT_FILENAME'], 0, 0-strlen($_SERVER['PHP_SELF']) ) );
}

define("__ROOT__", $_SERVER['DOCUMENT_ROOT']);

// Automatically load classes
spl_autoload_register(function ($class) {

    if (file_exists(__ROOT__."/admin")) {
        $classes_path = __ROOT__."/admin";
    } else {
        $classes_path = __ROOT__;
    }

    $class_path = $classes_path . "/classes/$class.class.php";

    if (file_exists($class_path)) include $class_path;

});

/* 
    $alt_pdo_db sirve para pasar una conexión diferente a la establecida en connection.php
    Por ejeplo, para usar la herramienta migrate-imgs.php, pasando la conexión a la DB de origen
*/
function newClass($class_name, $alt_pdo_db = false) {

    if(class_exists($class_name)) return new $class_name();
    else return new main($class_name, $alt_pdo_db);

}

$adminTitle 	= _("My Admin Panel");
$adminCopy	    = _("©2021 Estudio Estratega");
$adminVersion   = _("v.4.3");

?>
