<?php

$default_lang = "es";

$available_langs = array(
    "es" => "es_ES"
    //,"en" => "en_US"
);

if (isset($_GET['lang']) && isset($available_langs[$_GET['lang']])) {
    $lang = $_GET['lang'];
    $locale = $available_langs[$_GET['lang']];
    $_SESSION['lang'] = $lang;
}
else if (isset($_SESSION['lang'])) {
    $lang = $_SESSION['lang'];
    $locale = $available_langs[$_SESSION['lang']];
}
else {
    $lang = $default_lang;
    $locale = $available_langs[$default_lang];
}


if (defined('LC_MESSAGES')) {
    setlocale(LC_ALL, $locale); // Linux
    bindtextdomain($locale, "langs");
    // langs/$locale/LC_MESSAGES/$locale.mo
    // langs/$locale/LC_MESSAGES/$locale.po
} else {
    putenv("LC_ALL={$locale}"); // windows
    bindtextdomain($locale, ".\langs");
    // langs/es_ES/LC_MESSAGES/$locale.mo
    // langs/es_ES/LC_MESSAGES/$locale.po
}
textdomain($locale);
bind_textdomain_codeset($locale, 'UTF-8');

?>