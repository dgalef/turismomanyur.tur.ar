<?php

session_start();

if(empty($_SESSION['adminuser'])) {
    header('Location: login');
    exit;
} else {
    $adminuser = $_SESSION['adminuser'];
}

require_once "php/config.php";

$class_name             = "users";
$page_title             = "Usuarios";

// Quick settings
$show_back_button       = true;
$allow_feature          = false;
$allow_duplicate        = false;

$obj                    = newClass($class_name);
$media_obj              = newClass("media");
$blocks_obj             = newClass("pages_blocks");
$rol_obj                = newClass("admin_rols");

// Listado child de parent_class
$parent_id = (isset($_GET['parent_id'])) ? $_GET['parent_id'] : NULL;
if ($obj->parent_class) {

    if (!$parent_id && !$obj->allow_no_parent) die("Parent ID not specified");

    // Registro parent (para los casos en los que el listado de registros actual tenga un parent ID)
    if($parent_id) {
        $obj->where     = "WHERE parent_id = $parent_id";

        $parent_title   = $obj->get_parent_title($obj->parent_class, $parent_id, "title");
        $page_title     = "$page_title / $parent_title";
    }
}

else if (!$obj->parent_class && $parent_id) {
    die("There is a parent ID but not a parent class.");
}

// URL para crear registros
$add_file   = "$class_name-edit";
if($parent_id) $add_file .= "?parent_id=$parent_id";

// Obtener datos de la tabla correspondiente
if(isset($_GET['search']) && isset($_GET['val'])) {
    $data = $obj->search_data($_GET['search'], $_GET['val']);
}
else {
    $data = $obj->get_data();
}

$pagination = $obj->getPagination(); 

// Volver al listado de registros
$volver_url = ($parent_id) ? $obj->parent_class : null;

?>

<!doctype html>
<html class="no-js" lang="<?php echo $lang; ?>">

<head>
    <?php require_once "inc/head.php"; ?>
</head>

<body class="list-pg" data-rel="<?php echo $class_name; ?>">

    <?php require_once "inc/aside.php"; ?>

    <div class="main-wrapper">

        <?php require_once "inc/topbar.php"; ?>

        <main>
            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">

                    <div class="main-header">

                        <?php echo "<h1>$page_title</h1>"; ?>

                        <div class="main-opts">

                            <!--
                            Buscador de registros.
                            Usar data-rel para especificar las columnas en donde buscar, separadas por coma. Ej data-rel="titulo,descripcion,autor"
                            El buscador funciona buscando texto -> "%texto a buscar%"
                            -->
                            <div class="search-input">
                                <input type="text" name="query" class="query" data-rel="apellido,nombre,email" value="<?php if(isset($_GET["val"])) echo $_GET["val"] ?>" placeholder="<?php echo _("Buscar"); ?>">
                                <i class="far fa-search fa-fw"></i>
                            </div>

                            <?php
                            if ($adminuser->rol_id == 1) {
                                ?>
                            <a class="button" href="<?php echo $add_file; ?>"><i class="far fa-plus"></i> <?php echo _("Añadir"); ?></a>
                            <?php } ?>
                        </div>
                    </div>

                </div>
            </div>

            <div class="grid-x grid-padding-x">
                <div class="medium-12 cell">


                    <div class="content-list">

                        <?php

                        // No busco y no hay registros
                        if (!isset($_GET['search']) && $obj->currRows == 0) {
                            echo _("Haga click en 'Añadir' para crear el primer elemento.");
                        }

                        else {


                            // Busco y no hay registros
                            if(isset($_GET['search']) && $obj->currRows == 0) {
                                echo _("No hay registros coincidentes con el criterio de búsqueda.");
                            }


                            // Hay registros
                            else {

                                if(isset($_GET['search']))
                                    echo "<h4>" . sprintf(_("Resultados de búsqueda para <em>%s</em>"), $_GET['val']) . "</h4>";

                                echo "<p>" . sprintf(_("Mostrando %s de %s"), $obj->rowsRange, $obj->totalRows) ."</p>";

                        ?>

                        <ul class="sortable data-list" data-table="<?php echo $class_name; ?>" data-order="<?php echo $obj->sort_order; ?>">

                            <?php

                            foreach($data as $row) {
                                $id         = $row->id;
                                $elem_id    = "item_" . $id;

                                // URL para editar registros
                                $edit_file 	= "$class_name-edit?id=$id";
                                if($parent_id) $edit_file .= "&parent_id=$parent_id";

                               
                                //$categorias = $obj->get_tags_by_art("string", $row->id, "categorias", "productos_categorias");
                            ?>

                            <li id="<?php echo $elem_id; ?>" class="data-item" data-id="<?php echo $row->id; ?>" data-pos="<?php echo $row->pos; ?>">

                                <div class="data-item-inner">

                                   

                                    <div class="item-title">
                                        
                                        <?php 
                                
                                $disab = ($adminuser->rol_id == 2) ? "style='pointer-events:none'" : "";
                                //echo "<div class='row-labels'><span class='row-label'>$rol</span></div>";
                                echo "<a class='title' href='$edit_file' $disab>
                                                    $row->apellido, $row->nombre
                                                </a>
                                                <small>
                                                    $row->email
                                                </small>";
                                        ?>
                                        
                                        
                                        <!-- <span class="label tiny"></span> -->
                                        
                                    </div>

                                    <!-- Handler para drag&drop -->
                                    <?php if($obj->sort_allow) { ?>
                                    <div class="sort-handler">
                                        <span class="handler" title="Arrastrar para ordenar"><i class="far fa-arrows-alt fa-lg fa-fw"></i></span>
                                    </div>
                                    <?php } ?>

                                    <?php
                                if ($adminuser->rol_id == 1) {
                                    ?>
                                    
                                    <!-- Indicador de publicado -->
                                    <div class="status-indicator published">

                                        
                                        <div class="checkbox-switch can-toggle can-toggle-size-small demo-rebrand-2">
                                            <input type="hidden" name="status" class="chk-status auto-update" value="<?php echo $row->status; ?>">
                                            <input id="<?php echo "status-$id"; ?>" type="checkbox" class="chk-publish">
                                            <label for="<?php echo "status-$id"; ?>" title="<?php echo _("Desactivar <-> Activar") ?>">
                                                <div class="can-toggle__switch" data-checked="SI" data-unchecked="NO"></div>
                                            </label>
                                        </div>

                                    </div>

                                    <!-- RoW options -->
                                    
                                            
                                    <div class="item-opts text-right">

                                        <?php if($allow_feature) { ?>
                                        <div class="checkbox-star">
                                            <input type="hidden" name="featured" class="chk-featured auto-update" value="<?php echo $row->featured; ?>">
                                            <input class="chk-star" type="checkbox" id="star-<?php echo $row->id; ?>">
                                            <label for="star-<?php echo $row->id; ?>" title="<?php echo _("Destacar") ?>"></label>
                                        </div>
                                        <?php } ?>

                                        <a href="<?php echo $edit_file; ?>" class="opt" title="<?php echo _("Editar") ?>"><i class="fal fa-pen"></i></a>

                                        <?php if($allow_duplicate) { ?>
                                        <a href="#" class="opt" data-role="duplicate" title="<?php echo _("Duplicar") ?>"><i class="fal fa-copy"></i></a>
                                        <?php } ?>

                                        <a href="#" class="opt" data-role="delete" title="<?php echo _("Eliminar") ?>"><i class="fal fa-trash"></i></a>

                                        <div class="checkbox-check">
                                            <input class="chk" type="checkbox" value="<?php echo $row->id; ?>" name="id[]" id="<?php echo $row->id; ?>">
                                            <label for="<?php echo $row->id; ?>" title="<?php echo _("Seleccionar") ?>"></label>
                                        </div>

                                    </div>
                                    <?php } ?>
                                </div>

                            </li>

                            <?php } // end [for each] ?>

                        </ul>

                        <div class="data-list-opts text-right">
                            <div class="data-list-opt">
                                <a href="#" class="delete-selected"><?php echo _("Borrar seleccionados") ?></a>
                            </div>
                            <div class="data-list-opt select-all-opt">
                                <div class="checkbox-check">
                                    <input class="chk" type="checkbox" id="select-all">
                                    <label for="select-all" title="<?php echo _("Seleccionar todos") ?>"></label>
                                </div>
                            </div>
                        </div>

                        <div class="list-table-footer">
                            <?php echo $pagination; ?>
                        </div>

                        <?php

                            } // fin if(currRows != 0)
                        }
                        ?>

                    </div> <!-- contenidos -->

                </div>
            </div>
            
            <div class="bottom-fixed">
                <div class="bottom-fixed-left">
                    <div class="bottom-block">
                        <?php
                        if($show_back_button && $volver_url) echo "<a class='button hollow' href='$volver_url'><i class='far fa-arrow-left'></i>" . _("Volver") . "</a>";
                        ?>
                    </div>
                </div>
            </div>

            <div id="response" class="popup mfp-hide">
                <div class="response-content">
                    <div class="grid-x grid-margin-x">
                        <div class="cell">
                            <div class="mssg">[mensaje]</div>
                        </div>
                    </div>
                </div>
                <div class="response-footer">
                    <div class="grid-x grid-margin-x">
                        <div class="small-6 cell">
                        </div>
                        <div class="small-6 cell text-right">
                            <a class="popup-btn-cerrar button hollow" onClick="javascript:$.magnificPopup.close()">
                                <?php echo _("Cerrar") ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <?php require_once "inc/footer.php"; ?>

    </div> <!-- /.main-wrapper -->

</body>
</html>
