��    o      �  �         `	  
   a	     l	     t	     �	     �	  
   �	  
   �	  "   �	     �	     �	     �	     �	     �	     
     
     0
  
   7
     B
     [
     k
     w
     ~
     �
     �
     �
     �
     �
  5   �
     �
     	               0     9     @  *   Z  %   �  (   �     �     �     �      �          8  '   X  ,   �  =   �  g   �  �   S     !     '     3  1   E  X   w     �     �     �  
   �  6   �     '     .     3     8     @     G     ^  +   x     �     �     �     �  ;     3   ?     s     �     �     �     �     �     �     �  	   �  	   �     �  (   �     %     )     2     8     D     V  3   _     �  
   �  	   �     �     �     �     �     �     �     �                              $     ,     2  #  K     o     x     ~     �     �  
   �     �     �  	   �     �     �  	   �     �     �     	       
         +     D  
   S     ^     d     i     o     x     |     �  8   �     �     �  	   �     �  	                  $     >     X     u     �     �     �     �     �  !   �  (     /   1  Y   a  �   �     \  
   a     l  2   {  R   �                      %        A     E     J     M     S     X     g     {     �     �     �     �  4   �  )        =     V  
   o     z     �     �     �     �     �     �     �     �     �     �     �     �  
   �     	  %        8     =     F     S  	   X     b     g     |  	   �     �     �     �     �     �     �     �     �     �     (          \   e       4       C   *                          3   f          W   >   Y          G      ,   D      K          ?   P       h   &   =       S   !      E   N   H          -   ]   I   R   L       
   0   	   $       7   O   1      [           c   V   %   :   _   M                     B   T   8   A          <   +          6   U       l                      F   o   d          '      ^   ;   m   Q           `      j   2       X   5          g   Z       b      "   a      k   i              )   9       J   /   @   n                 .               #     Editar %s ACCEDER ADMIN USERS Aceptar Activar: Admin Rols Agregar %s Alguno de los datos es incorrecto. Apellido Arrastrar para ordenar Añadir Añadir otro Base Borrador <-> Publicado Borrar seleccionados Buscar CATEGORIES CM TATE - My Admin Panel Campo requerido Categorías Cerrar Ciudad Color Contraseña DNI Datos de acceso Datos personales Dejar en blanco si no se desea cambiar la contraseña Desactivar <-> Activar Desde: Destacar Documento de identidad Duplicar Editar El proceso no es válido. El registro fue actualizado correctamente. El registro fue creado correctamente. El registro fue duplicado correctamente. Elegir una contraseña Eliminar Email Error al actualizar el registro. Error al crear la relación. Error al eliminar la relación. Especificar fecha en formato DD-MM-YYYY Establecer color en formato HEX. Ej: #CC0000 Este dato es único, no puede existir otro igual en la tabla. Este es un panel de administración creado especialmente para gestionar los contenidos de tu sitio web. Esto conforma la URL. Acepta sólo letras, números y guiones (sin caracteres especiales, ni guiones bajos).<br><strong>No puede estar duplicado (no puede existir otro registro con el mismo slug).</strong> Fecha Fecha rango Formato inválido Formatos aceptados: JPG (recomendado), GIF y PNG. Formatos aceptados: JPG, GIF y PNG (recomendado). Medida recomendada: 440 x 220 pixeles. Foto Gallery Guardar Habilitar: Haga click en 'Añadir' para crear el primer elemento. Hasta: Help Hola Hola %s INICIO Información adicional Ingresar un email válido La operación ha sido realizada con éxito. La relación ha sido creada. La relación ha sido eliminada. Mostrando %s de %s My Admin Panel No hay registros coincidentes con el criterio de búsqueda. No se especifico nombre de tabla o tipo de proceso. No se permiten espacios No se pudo crea el tag Nombre Notas Número PROJECTS País Projects Provincia Publicar: Recuperar la contraseña Resultados de búsqueda para <em>%s</em> Rol SERVICES Salir Seleccionar Seleccionar todos Services Será el nombre de usuario para acceder al sistema. Slug Subtítulo Teléfono Texto Texto de ayuda Tipo Términos y condiciones Título Título del registro Usuario Volver base project rol service usuario v.4.3 ©2020 Estudio Estratega Project-Id-Version: CM Tate
PO-Revision-Date: 2020-12-07 17:16-0300
Last-Translator: 
Language-Team: 
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.1
X-Poedit-Basepath: ../../..
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: css
X-Poedit-SearchPathExcluded-1: js
X-Poedit-SearchPathExcluded-2: scss
X-Poedit-SearchPathExcluded-3: vendor
X-Poedit-SearchPathExcluded-4: node_modules
  Edit %s LOGIN ADMIN USERS Accept Enable: Admin Rols Add %s Incorrect login credentials. Last name Drag to reorder Add Add other Base Draft <-> Published Delete selected Search CATEGORIES CM TATE - My Admin Panel Required field Categories Close City Color Password DNI Credentials Personal details Leave this blank unless you want to change your password Disable <-> Enable From: Highlight Identification Duplicate Edit Invalid process. Row successfully updated. Row successfully created. Row successfully duplicated. Choose a password Delete Email Error updating the row. Error creating the relation. Error deleting the relation. Specify date in format MM-DD-YYYY Enter a value in HEX format. Eg: #CC0000 This should be unique, it cannot be duplicated. This is an administration panel specially created to manage the contents of your website. This is used to build the URL. Only letters, numbers and dashes are allowed (no special characters or underscores).<br><strong>It cannot be duplicated.</strong> Date Date range Invalid format Accepted extensions: JPG (recommended), GIF & PNG. Accepted formats: JPG, GIF, PNG (recommended). Recommended size: 440 x 220 pixels. Photo Gallery Save Enable Click 'Add' to create the first item. To: Help Hi Hi %s HOME Aditional info Enter a valid email Action completed successfully. Relation created. Relation deleted. Showing %s of %s My Admin Panel There are no records that match the search criteria. Table name or process type not specified. White spaces not allowed Tag couldn´t be created First name Notes Number PROJECTS Country Projects Province Publish: Password recovery Search results for <em>%s</em> Role SERVICES Exit Select Select all Services This will be the user name to log in. Slug Subtitle Phone number Text Help text Type Terms and conditions Title Row title User Back base project role service user v.4.3 ©2020 Estudio Estratega 