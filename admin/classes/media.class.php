<?php
class media extends main {

    // Allow manual reoredering of records [bool]
    public $sort_allow			            = true;
    // New items will be positioned at the top [ASC] or at the bottom [DESC]
    public $sort_order                      = "ASC";

    /*
        Function get_media()

        ***Params***
        $fetch_mode     = [string: row, all] -> Devuelve un registro o un array con varios registros
        $parent_class   = [string] -> Nombre de la clase parent
        $parent_id      = [int] -> ID del registro parent
        $label          = [string] -> (Opcional) Etiqueta que describe al archivo
        $rpp            = [int] -> Número de filas por página
    */
    /*
    public function get_media($fetch, $parent_class, $parent_id = NULL, $label = NULL, $rpp = NULL) {

        $this->query = "SELECT * FROM $this->table ";
        if($this->where != "") $this->query .= "$this->where AND parent_class = '$parent_class' ";
        else $this->query .= "WHERE parent_class = '$parent_class' ";
        if($parent_id) $this->query .= "AND parent_id = $parent_id ";
        if($label) $this->query .= "AND label = '$label' ";
        $this->query .= "$this->order ";

        // Pagination
        // Si se especifica $rpp, se toma como cantidad de filas/página a mostrar
        if ($rpp) $this->preparePagination($rpp);

        // Consulta a la DB
        $this->query .= " $this->limit";

        $sth = $this->db->prepare($this->query);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();
        $this->currRows = $sth->rowCount();
        if(!$this->totalRows) $this->totalRows = $this->currRows;

        $data = ($fetch == "row") ? $sth->fetchObject() : $sth->fetchAll();

        return $data;
    }
    */
    /*
        Function get_media_for_art()

          $relation_table = [string] Nombre la tabla de relaciones (ej: productos_categorias);
          $art_id         = [int] ID del registro cuyas categorias asociadas deberán marcarse con 'selected';

        Se usa para mostrar relaciones entre dos tablas (la tabla 'media' por un lado y otra), en conjunto con el plugin 'select2'

        [table_1]
        [relation_table]
        [media]

        La función devuelve un conjunto de <option> con todas las categorias existentes.
        Las asociadas a $art_id llevarán el atributo "selected".

    */
    public function get_media_for_art($tag_table, $relation_table, $art_id) {

        $tag_data = $this->get_media("all", $tag_table);
        $art_tag_obj = new main($relation_table);

        $opts = "";
        foreach($tag_data as $t) {
            $art_tag_obj->where = "WHERE art_id = $art_id AND tag_id = $t->id";
            $sel = ($art_tag_obj->get_data()) ? "selected" : "";
            $opts .= "<option value='$t->id' $sel>$t->title</option>";
        }

        return $opts;
    }

    public function remove_media_from_parent($parent_class, $parent_id) {
        
        $this->where = "WHERE parent_class = '$parent_class' AND parent_id = $parent_id";
        $this->query = "SELECT * FROM $this->table $this->where $this->order";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();
        
        $data = $sth->fetchAll();
        
        foreach($data as $d) {
        
            $this->delete_row($d->id);
        }
    }
    
    // DUPLICATE MEDIA
    public function duplicate_media_from_parent($parent_class, $parent_id, $new_parent_id) {
        
        $this->query = "SELECT id FROM media WHERE parent_class = '$parent_class' AND parent_id = $parent_id";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();

        $data = $sth->fetchAll();

        foreach($data as $d) {
            $this->duplicate_row($d->id, $new_parent_id);
        }
        
    }
    
    public function duplicate_row($id, $new_parent_id = null) {

        $pos = $this->highest_pos() + 1;
        $this->lastPos = $pos;

        $created_by = $_SESSION['adminuser']->id;

        $this->query = "INSERT INTO $this->table
                        (parent_class, parent_id, file_name, file_source_name, file_ext, file_type, file_sizes, media_title, media_description, label, created_by, featured, pos)
                    SELECT
                        parent_class, '$new_parent_id', file_name, file_source_name, file_ext, file_type, file_sizes, media_title, media_description, label, '$created_by', featured, '$pos'
                    FROM
                        $this->table
                    WHERE
                        id = $id;";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute();
            $result["success"]  = 1;
            $result["message"]  = "El registro fue duplicado correctamente.";
            $this->lastAffected = $this->db->lastInsertId();
        }

        catch(PDOException $e) {

            $result["error"] = $e->errorInfo[2];
        }

        return $result;

    }

    /*
        Delete row
    */
    public function delete_row($id) {

        $this->remove_files($id);

        return parent::delete_row($id);
    }

    /*
        Remove files from server
    */
    public function remove_files($id) {

        $media_row 		= $this->get_row($id);
        $file_name      = $media_row->file_name;
        
        // Search for media records with same file_name (pointing to same file)
        $this->where = "WHERE file_name = '$file_name' AND id != $id";
        $other_regs = $this->get_data();
            
        // If there is no more regs, delete file
        if(!$other_regs) {

            // If media is image
            if ($media_row->file_type == "images") {
                    
                // Remove all sizes
                $img_sizes = json_decode($media_row->file_sizes);

                foreach($img_sizes as $size => $value) {
                    $file  = __ROOT__ . "/media/images/$size/" . $file_name;
                    if (is_file($file)) unlink($file);
                }

                // Remove src
                $file  = __ROOT__ . "/media/images/src/" . $file_name;
                if (is_file($file)) unlink($file);
            }
            
            // If media is video
            else {
                // Remove src
                $file  = __ROOT__ . "/media/" . $media_row->file_type . "/" . $file_name;
                if (is_file($file)) unlink($file);
            }
        }
    }
}

?>
