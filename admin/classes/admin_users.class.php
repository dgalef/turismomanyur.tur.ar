<?php
class admin_users extends main {

    //
    // Listings
    //
    // Allow manual reoredering of records [bool]
    public $sort_allow			            = false;
    // New items will be positioned at the top [ASC] or at the bottom [DESC]
    public $sort_order                      = "ASC";
    // If $sort_allow, use this colum to sort rows
    public $sort_by_column                  = NULL;
    // [string] Rewrite custom orden (if $sort_allow == false)
    public $order                           = "ORDER BY first_name ASC, last_name ASC, id ASC";

    //
    // Adding
    //
    // Automatically create a slug when creating a row [bool] - (slug column must exist in MySQL table)
    public $auto_slug		                 = true;

    //
    // Edition
    //
    // It´s a one-record table [bool]
    public $is_single_row		             = false;

    //
    // Table relations
    //
    public $parent_class            		= "";

    //
    // Media
    //
    public $images_sizes = array();

    //
    // Static categories
    //
    public $categories				= array("key"    => "value",
                                            "key2"     => "value2");


    // CREATE ROW
    public function create_row($arrColsVals) {
        
        $pass = isset($arrColsVals['password']) ? $arrColsVals['password'] : "temp";
        $pass_arr = $this->create_password_hash($pass);

        $arrColsVals["salt"] 		= $pass_arr['salt'];
        $arrColsVals["password"] 	= $pass_arr['password'];
        
        return parent::create_row($arrColsVals);
        
    }

    public function update_row($arrColsVals, $id) {

        if (isset($arrColsVals["password"])) {

            $new_pass 	= $arrColsVals["password"];

            unset($arrColsVals["password"]);
            unset($arrColsVals["confirmPassword"]);

            // Si hay un cambio de contraseña...
            if(!empty($new_pass))
            {
                $pass_arr = $this->create_password_hash($new_pass);

                $arrColsVals["salt"] 		= $pass_arr['salt'];
                $arrColsVals["password"] 	= $pass_arr['password'];
            }

        }
        // Update user
        return parent::update_row($arrColsVals, $id);


    }
/*
    public function get_data($status = "all", $rpp = NULL) {

        if(isset($_GET['q'])) {
            $q = $_GET['q'];
            $this->where = "WHERE first_name LIKE '%$q%' OR last_name LIKE '%$q%' OR email LIKE '%$q%'";
        }

        return parent::get_data($status, $rpp);

    }
*/
    public function get_user($email, $password){

        $query = "SELECT * FROM $this->table WHERE email = :email";

        $sth = $this->db->prepare($query);
        $sth->bindParam(':email', $email);
        $sth->execute();

        $data = $sth->fetchObject();

        if($data)
        {
            // Using the password submitted by the user and the salt stored in the database,
            // we now check to see whether the passwords match by hashing the submitted password
            // and comparing it to the hashed version already stored in the database.
            $check_password = hash('sha256', $password . $data->salt);
            for($round = 0; $round < 65536; $round++)
            {
                $check_password = hash('sha256', $check_password . $data->salt);
            }

            if($check_password === $data->password)
            {
                return $data;
            }

        }

        return false;

    }

    public function user_exists($email){

        $query = "SELECT * FROM $this->table WHERE email = :email";

        $sth = $this->db->prepare($query);
        $sth->bindParam(':email', $email);
        $sth->execute();

        $data = $sth->fetchObject();

        return $data;
    }


    // CREATE ROW
    /*
    public function add_user($arrColsVals) {

        unset($arrColsVals["action"]);
        unset($arrColsVals["confirmPassword"]);
        unset($arrColsVals["tos"]);

        //$arrColsVals["fecha_alta"] = date("Y-m-d");

        $pass = isset($arrColsVals['password']) ? $arrColsVals['password'] : "temp";
        $pass_arr = $this->create_password_hash($pass);

        $arrColsVals["salt"] 		= $pass_arr['salt'];
        $arrColsVals["password"] 	= $pass_arr['password'];

        $cols_arr = array();	// Columns
        $vals_arr = array();	// Values
        $plhd_arr = array();	// Placeholders

        foreach ($arrColsVals as $col => $val) {
            array_push($cols_arr, $col);
            array_push($vals_arr, $val);
            array_push($plhd_arr, "?");
        }

        $cols 	= implode(",", $cols_arr);
        $plhd	= implode(",", $plhd_arr);

        $query  = "INSERT INTO $this->table ( $cols ) VALUES ( $plhd )";

        $sth 	= $this->db->prepare($query);
        $bool 	= $sth->execute($vals_arr);

        $this->lastAffected = $this->db->lastInsertId();

        return $bool;

    }
*/
    
    public function check_login($email, $password){

        $query = "SELECT * FROM $this->table WHERE email = :email AND password = :password";

        $sth = $this->db->prepare($query);
        $sth->bindParam(':email', $email);
        $sth->bindParam(':password', $password);
        $sth->execute();

        $data = $sth->fetchObject();

        return $data;
    }

    public function reset_pass($id, $pass) {

        $arr = $this->create_password_hash($pass, $salt = NULL);

        return parent::update_row($arr, $id);

    }

    public function create_password_hash($pass) {

        // A salt is randomly generated here to protect again brute force attacks
        // and rainbow table attacks.  The following statement generates a hex
        // representation of an 8 byte salt.  Representing this in hex provides
        // no additional security, but makes it easier for humans to read.
        // For more information:
        // http://en.wikipedia.org/wiki/Salt_%28cryptography%29
        // http://en.wikipedia.org/wiki/Brute-force_attack
        // http://en.wikipedia.org/wiki/Rainbow_table
        $salt = dechex(mt_rand(0, 2147483647)) . dechex(mt_rand(0, 2147483647));

        // This hashes the password with the salt so that it can be stored securely
        // in your database.  The output of this next statement is a 64 byte hex
        // string representing the 32 byte sha256 hash of the password.  The original
        // password cannot be recovered from the hash.  For more information:
        // http://en.wikipedia.org/wiki/Cryptographic_hash_function
        $password = hash('sha256', $pass . $salt);

        // Next we hash the hash value 65536 more times.  The purpose of this is to
        // protect against brute force attacks.  Now an attacker must compute the hash 65537
        // times for each guess they make against a password, whereas if the password
        // were hashed only once the attacker would have been able to make 65537 different
        // guesses in the same amount of time instead of only one.
        for($round = 0; $round < 65536; $round++)
        {
            $password = hash('sha256', $password . $salt);
        }

        $arr = array(
            "password" 	=> $password,
            "salt"		=> $salt
        );

        return $arr;
    }


}

?>
