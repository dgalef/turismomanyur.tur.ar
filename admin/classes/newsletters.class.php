<?php
class newsletters extends main {

    //
    // Listings
    //
    // Allow manual reoredering of records [bool]
    public $sort_allow			            = false;
    // New items will be positioned at the top [ASC] or at the bottom [DESC]
    public $sort_order                      = "DESC";
    // If $sort_allow, use this colum to sort rows
    public $sort_by_column                  = "created_on";
    // [string] Rewrite custom orden (if $sort_allow == false)
    // public $order                           = "ORDER BY fecha DESC, id DESC";

    //
    // Adding
    //
    // Automatically create a slug when creating a row [bool] - (slug column must exist in MySQL table)
    public $auto_slug		                 = true;

    //
    // Edition
    //

    //
    // Table relations
    //
    // [string | NULL] Nombre de la lase parent, si hay
    public $parent_class            		= NULL;
    // [bool] Permitir una clase con parent, mostrar contenido sin parent_id
    public $allow_no_parent                 = NULL;
    // [string | NULL] Nombre de la lase child, si hay
    public $child_class            		    = NULL;
    // [string] Texto del botón que edita el listado child
    public $child_edit_btn_text             = NULL;

    //
    // Media
    //
    public $images_sizes = [

        "default" => [

            // Medium image
            "medium" => [
                "max_width"         => 700,
                "max_height"        => null,        // 720,
                "best_fit"          => false,
                "allow_scale_up"    => true
            ],

            // Small image
            "small" => [
                "max_width"         => 350,
                "max_height"        => null,
                "best_fit"          => false,
                "allow_scale_up"    => true
            ],

            // Tiny (for admin lists)
            "tiny" => [
                "max_width"         => 200,
                "max_height"        => 200,
                "best_fit"          => true,
                "allow_scale_up"    => false
            ]
        ]
    ];

   

}

?>
