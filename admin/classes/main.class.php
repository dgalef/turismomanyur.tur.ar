<?php

class main {

    // DB connection object
    public $db;
    public $alt_pdo_db;
    //
    public $table;

    //
    // Listings
    //
    // Allow manual reoredering of records [bool]
    public $sort_allow			            = true;
    // New items will be positioned at the top [DESC] or at the bottom [ASC]
    public $sort_order                      = "ASC";
    // If $sort_allow, use this colum to sort rows
    public $sort_by_column                  = NULL;

    //
    // Adding
    //
    // Automatically create a slug when creating a row [bool] - (slug column must exist in MySQL table)
    public $auto_slug		                 = false;

    //
    // Edition
    //
    // [bool] Si es false, el botón volver (al listado) no se muestra.
    public $show_back_button                 = true;

    //
    // Table relations
    //
    // [string | NULL] Nombre de la lase parent, si hay
    public $parent_class            		= NULL;
    // [bool] Permitir una clase con parent, mostrar contenido sin parent_id
    public $allow_no_parent                 = NULL;
    // [string | NULL] Nombre de la lase child, si hay
    public $child_class            		    = NULL;
    // [string] Texto del botón que edita el listado child
    public $child_edit_btn_text             = NULL;


    /*
        Default Media size

            max_width & max_height  = (int || null)
                null   -> I one value is null, dest image will match the other value and scale proportionally.
                int    -> If both values are set, dest image will match larger side and scale proportionally (if best_fit == true)
                          or be cropped to match aspect ratio (if best_fit == false)

            best_fit    = [bool]
                    true    -> Prevents crop (keeps original aspect ratio).
                               Takes 'max_width' & 'max_height' (both required) and scales image to best fit.
                    false   -> Crops the image to match both 'max_width' and 'max_height'.

            allow_scale_up  = [bool] Allows image resize up if neccessary

    */
    public $images_sizes = [
        
        "default" => [
            /*
            // XLarge image
            "xxlarge" => [
                "max_width"         => 3840,
                "max_height"        => null,       // 2160
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],
*/ 
            
            "xlarge" => [
                "max_width"         => 2880,
                "max_height"        => null,       // 1620
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],
            
            // Large image
            "large" => [
                "max_width"         => 1920,
                "max_height"        => null,       // 1080
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],

            // Medium image
            "medium" => [
                "max_width"         => 1280,
                "max_height"        => null,        // 720,
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],

           // Small image
            "small" => [
                "max_width"         => 640,
                "max_height"        => null,
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],

            // Tiny (for admin lists)
            "tiny" => [
                "max_width"         => 200,
                "max_height"        => 200,
                "best_fit"          => true,
                "allow_scale_up"    => false
            ]
        ]
    ];

    //
    // Query details
    public $query           = "";
    public $where			= "";
    public $limit			= "";
    public $order			= "";

    //
    // Last row affected
    public $lastAffected    = NULL;
    public $lastPos         = NULL;
    //
    // Pagination
    public $rows_per_page	= 50;     // Maybe NULL - pagination wont be displayed
    public $pag				= 1;
    public $last;
    public $currRows		= NULL;     
    public $rowsRange		= NULL;
    public $totalRows       = NULL;
    public $urlCat			= '';
    public $pagNav			= '';
    //
    
    // CUSTOM
    public $current_row;
    //

    public function __construct($table = NULL, $alt_pdo_db = NULL) {

        if(!is_string($table) && $table !== NULL) {
            die("Al crear una instancia de la clase '" . get_class($this) . "', el parámetro \$table debe ser un string (el nombre de la tabla)");
        }

        else {
            if($table) $this->table = $table;
            else $this->table = get_class($this);

            global $db;
            $this->db = ($alt_pdo_db) ? $alt_pdo_db : $db;
        }

        if($this->sort_allow) {
            $this->order = "ORDER BY pos $this->sort_order, id $this->sort_order";
        } else if($this->order == "") {
            $this->order = "ORDER BY $this->sort_by_column $this->sort_order, id $this->sort_order";
        }
        
        // Include custom image size if declared
        if (isset($this->custom_images_sizes))
            $this->images_sizes = array_merge($this->images_sizes, $this->custom_images_sizes);
    }

    public function getTableName() {
        //return get_class($this);
        return $this->table;
    }

    // GET DATA (all rows from table)
    public function get_data($status = "saved", $rpp = NULL) {

        if($status == "all") {
            // Devolver todos los registros
        }

        else if($status == "saved") {
            // Devolver los registros guardados (!= 'temp')
            $this->where .= ($this->where == "") ? "WHERE status != 'temp'" : " AND status != 'temp'";
        }

        else if($status == "published") {
            // Devolver sólo los registros publicados
            $this->where .= ($this->where == "") ? "WHERE status = 'published'" : " AND status = 'published'";
        }

        $this->query = "SELECT * FROM $this->table $this->where $this->order";

        // Pagination
        // Si se especifica $rpp, se toma como cantidad de filas/página a mostrar
        if ($rpp) $this->rows_per_page = $rpp;
        if ($this->rows_per_page) $this->preparePagination();

        // Consulta a la DB
        $this->query .= " $this->limit";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();
        $this->currRows = $sth->rowCount();
        if(!$this->totalRows) $this->totalRows = $this->currRows;
        
        if($this->rows_per_page) {
            $range_init = ($this->pag - 1) * $this->rows_per_page + 1;
            $range_end = $range_init + $this->rows_per_page - 1;
            $range_end = ($range_end < $this->totalRows) ? $range_end : $this->totalRows;
            $this->rowsRange = "$range_init - $range_end";
        }
        else {
            $this->rowsRange = "1 - $this->totalRows";
        }

        $data = $sth->fetchAll();

        return $data;

    }

    // GET DATA - SEARCH (backend)
    public function search_data($search_keys, $search_val, $rpp = NULL) {

        $join = "";

        $val = $search_val;
        $arr = explode(",", $search_keys);

        if($this->where == "") {
            $this->where = "WHERE (";
        } else {
            $this->where .= " AND (";
        }

        $k = 0;
        foreach ($arr as $key) {

            $this->where .= "$key LIKE '%$val%'";

            $k++;

            if ($k < sizeof($arr)) $this->where .= " OR ";
        }
        $this->where .= ")";

        

        return $this->get_data($rpp);

    }

    // GET ROW (select by 'id' or -optional- set $id = NULL and sepcify $col & $val)
    public function get_row($id, $col = NULL, $val = NULL) {

        $col = ($id) ? "id" : $col;
        $val = ($id) ? $id : $val;
        
        $this->query = "SELECT * FROM $this->table WHERE $col = :val";
        
        $sth = $this->db->prepare($this->query);
        $sth->bindParam(':val', $val);
        $sth->execute();
        $data = $sth->fetchObject();

        return $data;
    }

    public function get_first($status = "published") {

        $this->query = "SELECT * FROM $this->table ";
        if ( $status == "published" ) $this->query .= "WHERE status = 'published' ";
        $this->query .= $this->order . " LIMIT 0,1";

        $sth = $this->db->prepare($this->query);
        $sth->execute();
        $data = $sth->fetchObject();

        return $data;
    }

    public function get_last($status = "published") {

        if ( $this->sort_order == "ASC" )
            $reverse_order = str_replace('ASC', 'DESC', $this->order);
        else
            $reverse_order = str_replace('DESC', 'ASC', $this->order);

        $this->query = "SELECT * FROM $this->table ";
        if ( $status == "published" ) $this->query .= "WHERE status = 'published' ";
        $this->query .= $reverse_order . " LIMIT 0,1";

        $sth = $this->db->prepare($this->query);
        $sth->execute();
        $data = $sth->fetchObject();

        return $data;
    }

    /*
        Function to get next row, based on a 'pos' or a 'date_from' given.

        Params:
            - pos = [int] Required. May be null if you need to use date instead (ie: on a news table).
            - date = [datetime] Optional. Usually $row->date_from.
            - loop = [bool] When 'true' && if no next row, then it will return the first row.
    */
    public function get_next($pos, $date = null, $loop = true, $status = "published") {

        $this->query = "SELECT * FROM $this->table ";
        if ( $this->sort_order == "ASC" ) {
            if($date)
                $this->query .= "WHERE date_from > '$date' ";
            else
                $this->query .= "WHERE pos > $pos ";
        }
        else {
            if($date)
                $this->query .= "WHERE date_from < '$date' ";
            else
                $this->query .= "WHERE pos < $pos ";
        }

        if ( $status == "published" ) $this->query .= " AND status = 'published' ";

        $this->query .= $this->order;

        $sth = $this->db->prepare($this->query);
        $sth->execute();
        $data = $sth->fetchObject();

        if (!$data && $loop)
            $data = $this->get_first($status);

        return $data;
    }

    /*
        Function to get previous row, based on a 'pos' or a 'date_from' given.

        Params:
            - pos = [int] Required. May be null if you need to use date instead (ie: on a news table).
            - date = [datetime] Optional. Usually $row->date_from.
            - loop = [bool] When 'true' && if no prev row, then it will return the last row.
    */
    public function get_prev($pos, $date = null, $loop = true, $status = "published") {

        $this->query = "SELECT * FROM $this->table ";

        if ( $this->sort_order == "ASC" ) {
            $reverse_order = str_replace('ASC', 'DESC', $this->order);

            if($date)
                $this->query .= "WHERE date_from < '$date' ";
            else
                $this->query .= "WHERE pos < $pos ";
        }
        else {
            $reverse_order = str_replace('DESC', 'ASC', $this->order);

            if($date)
                $this->query .= "WHERE date_from > '$date' ";
            else
                $this->query .= "WHERE pos > $pos ";
        }

        if ( $status == "published" ) $this->query .= " AND status = 'published' ";

        $this->query .= $reverse_order;
        $sth = $this->db->prepare($this->query);
        $sth->execute();
        $data = $sth->fetchObject();

        if (!$data && $loop)
            $data = $this->get_last($status);

        return $data;
    }

    // CREATE ROW
    public function create_row($arrColsVals) {

        // Autogenerar slug
        // ATENCION: Ahora se crean automáticamente los registros, sin título!
        if ( isset($arrColsVals["slug"]) && $arrColsVals["slug"] == '' && $this->auto_slug)
            $arrColsVals["slug"] = $this->create_unique_slug($arrColsVals["title"]);

        // Position: Tomar el valor más alto de 'pos' en la tabla y sumarle uno
        $pos = $this->highest_pos() + 1;
        $arrColsVals["pos"] = $pos;
        $this->lastPos = $pos;
        //

        // User who is creatieng this row
        if (isset($_SESSION['adminuser']))
            $arrColsVals["created_by"] = $_SESSION['adminuser']->id;

        $cols_arr = array();	// Columns
        $vals_arr = array();	// Values
        $plhd_arr = array();	// Placeholders

        foreach ($arrColsVals as $col => $val) {
            array_push($cols_arr, $col);
            array_push($vals_arr, $val);
            array_push($plhd_arr, "?");
        }

        $cols 	= implode(",", $cols_arr);
        $plhd	= implode(",", $plhd_arr);

        $this->query  = "INSERT INTO $this->table ( $cols ) VALUES ( $plhd )";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute($vals_arr);
            $result["success"]  = 1;
            $result["message"]  = _("El registro fue creado correctamente.");
            $this->lastAffected = $this->db->lastInsertId();
        }

        catch(PDOException $e) {

            //$result["error"] = $e->getMessage();
            $result["error"] = $e->errorInfo[2];
            //$result["error"] = print_r($e);
            //$result["error"] = $e;
        }

        return $result;

    }
    
    // DUPLICATE ROW
    public function duplicate_row($id, $new_parent_id = null) {

        // Registro a duplicar
        $row        = $this->get_row($id);

        // Columnas existentes en la tabla
        $columns    = $this->get_columns();

        // Ignorar estas columnas al duplicar el registro
        $ignore_columns = array("id", "created_on", "last_modify_on", "last_modify_by");

        // Nuevo valor 'title'
        //$new_title = $row->title . "  - Copy from ID $id";
        $new_title = $row->title;

        // Si existe la columna 'slug' y el registro a duplicar tiene un slug, crear nuevo slug
        if(in_array("slug", $columns) && $row->slug)
            $new_slug = $this->create_unique_slug($row->slug);
        else {
            $new_slug = null;
        }

        // Nuevo valor 'pos'
        $new_pos = $this->highest_pos() + 1;
        $this->lastPos = $new_pos;

        // Nuevo valor 'created_by'
        $new_created_by = $_SESSION['adminuser']->id;

        // Nuevo valor 'status'
        $new_status = "draft";


        $cols_arr = array();	// Columns
        $vals_arr = array();	// Values


        foreach ($columns as $c) {

            if (!in_array($c, $ignore_columns)) {

                array_push($cols_arr, $c);

                if($c == 'title') 
                    array_push($vals_arr, "'$new_title'");
                else if($c == 'slug' && $new_slug) 
                    array_push($vals_arr, "'$new_slug'");
                else if($c == 'created_by') 
                    array_push($vals_arr, "'$new_created_by'");
                else if($c == 'pos') 
                    array_push($vals_arr, "'$new_pos'");
                else if($c == 'status') 
                    array_push($vals_arr, "'$new_status'");
                else
                    array_push($vals_arr, $c);

                //array_push($plhd_arr, "?");
            }
        }

        $cols 	= implode(",", $cols_arr);
        $vals	= implode(",", $vals_arr);

        $this->query = "INSERT INTO $this->table
                        ( $cols )
                    SELECT
                        $vals
                    FROM
                        $this->table
                    WHERE
                        id = $id";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute();
            $result["success"]  = 1;
            $result["message"]  = _("El registro fue duplicado correctamente.");
            $this->lastAffected = $this->db->lastInsertId();
            $new_id = $this->lastAffected;

            $media_obj = new media();
            $media_obj->duplicate_media_from_parent($this->table, $id, $new_id);

            $this->duplicate_relations($id, $new_id);
        }

        catch(PDOException $e) {

            $result["error"] = $e->errorInfo[2];
        }

        return $result;

    }
    
    // DUPLICATE RELATIONS
    public function duplicate_relations($id, $new_id) {

        $relations_obj = new main("relations");
        // Columnas existentes en la tabla
        $columns    = $relations_obj->get_columns();

        // Ignorar estas columnas al duplicar el registro
        $ignore_columns = array("id", "created_on", "last_modify_on", "last_modify_by");

        // Nuevo valor 'pos'
        $new_pos = $this->highest_pos() + 1;
        $this->lastPos = $new_pos;

        // Nuevo valor 'created_by'
        //$new_created_by = $_SESSION['adminuser']->id;
        $new_created_by = "";

        // Nuevo valor 'status'
        $new_status = "draft";


        $cols_arr = array();	// Columns
        $vals_arr = array();	// Values


        foreach ($columns as $c) {

            if (!in_array($c, $ignore_columns)) {

                array_push($cols_arr, $c);

                if($c == 'art_id') 
                    array_push($vals_arr, "'$new_id'");
                else if($c == 'created_by') 
                    array_push($vals_arr, "'$new_created_by'");
                else if($c == 'pos') 
                    array_push($vals_arr, "'$new_pos'");
                else if($c == 'status') 
                    array_push($vals_arr, "'$new_status'");
                else
                    array_push($vals_arr, $c);
            }
        }

        $cols 	= implode(",", $cols_arr);
        $vals	= implode(",", $vals_arr);

        $this->query = "INSERT INTO relations ( $cols ) SELECT $vals FROM relations WHERE art_id = $id AND art_ref = '$this->table'";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute();
            $result["success"]  = 1;
            $result["message"]  = _("El registro fue duplicado correctamente.");

        }

        catch(PDOException $e) {

            $result["error"] = $e->errorInfo[2];
        }

        return $result;

    }
    
    /* /////////////////////////////
        MEDIA ELEMENTS
    */ /////////////////////////////
    public function get_media($parent_id = NULL, $label = NULL, $rpp = 1000, $status = "saved") {

        $this->query = "SELECT * FROM media WHERE parent_class = '$this->table' ";
        if($parent_id) $this->query .= "AND parent_id = $parent_id ";
        if($label) $this->query .= "AND label = '$label' ";
        if($status != 'saved') $this->query .= "AND status = '$status' ";
        $this->query .= "ORDER BY pos ASC ";

        // Pagination
        // Si se especifica $rpp, se toma como cantidad de filas/página a mostrar
        if ($rpp) $this->preparePagination($rpp);

        // Consulta a la DB
        $this->query .= " $this->limit";

        $sth = $this->db->prepare($this->query);

        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();
        
        
        
        
        
        
        $this->currRows = $sth->rowCount();
        if(!$this->totalRows) $this->totalRows = $this->currRows;

        if($this->rows_per_page) {
            $range_init = ($this->pag - 1) * $this->rows_per_page + 1;
            $range_end = $range_init + $this->rows_per_page - 1;
            $range_end = ($range_end < $this->totalRows) ? $range_end : $this->totalRows;
            $this->rowsRange = "$range_init - $range_end";
        }
        else {
            $this->rowsRange = "1 - $this->totalRows";
        }

        
        
        

        $data = ($rpp == 1) ? $sth->fetchObject() : $sth->fetchAll();

        return $data;
        
    }
    
    public function get_img($parent_id = null, $src = "srcset", $label = NULL, $use_placeholder = false) {
        
        $this->query = "SELECT * FROM media WHERE parent_class = '$this->table' ";

        if ( $parent_id )
            $this->query .= "AND parent_id = $parent_id ";

        if ( $label ) 
            $this->query .= "AND label = '$label' ";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();

        $row = $sth->fetchObject();

        // *** //

        if ( $row ) {

            // Vector (SVG)
            if ($row->file_ext == "svg") {
                $img = "<img src='/media/images/src/" . $row->file_name . "'>";
            }

            // Bitmaps (JPG, PNG, GIF)
            else {

                // Get sizes available from $row
                $sizes = json_decode($row->file_sizes);    
                unset($sizes->src);
                unset($sizes->tiny);

                if ( $src == "srcset" ) {

                    $srcset_arr = [];
                    foreach($sizes as $k => $v) {

                        $WxH = explode("x", $v);

                        $srcset_arr[] = "/media/images/" . $k . "/" . $row->file_name . " " . $WxH[0] . "w";
                    }

                    $srcset_str = implode(", ", $srcset_arr);

                    $img = "<img srcset='$srcset_str' src='/media/images/medium/$row->file_name'>";

                }
                else {
                    $img = "<img src='/media/images/$src/$row->file_name'>";
                }
            }

            return $img;
        }

        // No image
        else if ( $use_placeholder ) {

            // Get sizes from $this class
            if ( $label && isset($this->images_sizes[$label]) )                    
                $sizes = $this->images_sizes[$label];
            else
                $sizes = $this->images_sizes["default"];

            if ( $src == "srcset" )
                $no_img_path = "/media/images/no-image-" . $sizes["medium"]["max_width"] . "w.jpg";
            else
                $no_img_path = "/media/images/no-image-" . $sizes[$src]["max_width"] . "w.jpg";

            $img = "<img class='no-img' src='$no_img_path'>";

            return $img;
        }

        return false;
        
    }
    
    public function get_imgs($parent_id = null, $src = "srcset", $label = NULL, $status = 'saved') {

        $this->query = "SELECT * FROM media WHERE parent_class = '$this->table' ";

        if ( $parent_id )
            $this->query .= "AND parent_id = $parent_id ";

        if ( $label ) 
            $this->query .= "AND label = '$label' ";

        if ( $status != "saved" ) 
            $this->query .= "AND status = '$status' ";

        $this->query .= "ORDER BY pos ASC";
        
        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        $data = $sth->fetchAll();

        // *** //

        $imgs = [];

        foreach ( $data as $d) {

            // Vector (SVG)
            if ($d["file_ext"] == "svg") {
                $thum = "<img src='/media/images/src/" . $d['file_name'] . "'>";
            }

            // Bitmaps (JPG, PNG, GIF)
            else {

                // Get sizes available from $row
                $sizes = json_decode($d["file_sizes"]);    
                unset($sizes->src);
                unset($sizes->tiny);

                if ( $src == "srcset" ) {

                    $srcset_arr = [];
                    foreach($sizes as $k => $v) {

                        $WxH = explode("x", $v);

                        $srcset_arr[] = "/media/images/" . $k . "/" . $d["file_name"] . " " . $WxH[0] . "w";
                    }

                    $srcset_str = implode(", ", $srcset_arr);

                    $thum = "<img srcset='$srcset_str' src='/media/images/medium/" . $d["file_name"] . "'>";

                }
                else {
                    $thum = "<img src='/media/images/$src/" . $d['file_name'] . "'>";
                }
            }


            $d["thum"] = $thum;
            $d["link"] = "/media/images/large/" . $d['file_name'];

            array_push($imgs, $d);
        }

        return $imgs;
    }
    
    // UPDATE ROW
    public function update_row($arrColsVals, $id) {

        // User & date of update
        if(isset($_SESSION['adminuser'])) 
            $arrColsVals["last_modified_by"] = $_SESSION['adminuser']->id;
        
        $date = date('Y-m-d H:i:s');
        $arrColsVals["last_modified_on"] = $date;

        $vals_arr = array();	// Values

        $this->query  = "UPDATE $this->table SET ";

        foreach ($arrColsVals as $col => $val) {

            // Guardar varias categorias en una columna
            // El valor viene como array y se transforma en string (ej: "musica || arte || moda || ")
            if(is_array($val)) {
                $val = implode(" || ", $val);
                $val = substr($val, 0, -4);  // Quita el último ' || ' de la cadena, que está sucedido por un valor vacío.
            }

            // Para permitir insertar un valor NULL, se transforma un string "NULL" en valor del tipo NULL
            if($val == "NULL") $val = NULL;

            array_push($vals_arr, $val);	// Push value to array

            $this->query .= "$col = ?";

            end($arrColsVals);
            if ($col !== key($arrColsVals)) $this->query .= ", ";
        }

        $this->query .= "WHERE id = $id";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute($vals_arr);
            $result["success"]  = 1;
            $result["message"]  = _("El registro fue actualizado correctamente.");
            $this->lastAffected = $this->db->lastInsertId();
        }

        catch(PDOException $e) {

            //$result["error"] = $e->getMessage();
            $result["error"] = $e->errorInfo[2];
            //$result["error"] = print_r($e);
            //$result["error"] = $e;
        }

        return $result;

    }

    // DELETE ROW/S
    public function delete_row($id) {

        // Remove associated images first
        $media_obj = new media();
        $media_obj->remove_media_from_parent($this->table, $id);

        // Remove row
        $this->query 	= "DELETE FROM $this->table WHERE id = $id";
        $sth 	= $this->db->prepare($this->query);
        $bool 	= $sth->execute();
        return $bool;
    }

    // UPDATE INDEX FROM ALL ROWS (used to order rows)
    public function update_index_from($idx, $id) {

        $this->query  = "UPDATE $this->table SET pos = :idx WHERE id = :id";


        $sth = $this->db->prepare($this->query);
        $sth->bindParam(':idx', $idx);
        $sth->bindParam(':id', $id);
        $bool = $sth->execute();

        $this->lastAffected = $this->db->lastInsertId();

        if ($bool) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_menu_tree($idx, $id, $pid) {

        $this->query  = "UPDATE $this->table SET pos = $idx, parent_id = $pid WHERE id = $id";

        $sth = $this->db->prepare($this->query);
        $bool = $sth->execute();

        $this->lastAffected = $this->db->lastInsertId();

        if ($bool) {
            return 1;
        } else {
            return 0;
        }
    }


    // GET highest pos value
    public function highest_pos() {

        $this->query = "SELECT MAX(pos) as maximum FROM $this->table";

        $sth = $this->db->prepare($this->query);
        $sth->execute();

        $data = $sth->fetchObject();

        return $data->maximum;
    }

    // GET tags for a specific article
    public function get_tags_by_art($art_id, $tag_table, $return_type = "data", $return_col = "title", $separator = ", ") {

        // $this->query = "SELECT * FROM $this->table $this->where $this->order";
        
        $this->query = "SELECT t.* FROM $tag_table t
                            INNER JOIN relations r
                                ON t.id = r.tag_id
                            WHERE r.tag_ref = '$tag_table' AND r.art_ref = '$this->table' AND r.art_id = $art_id
                            ORDER BY t.title ASC";
        
        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();

        $data = $sth->fetchAll();

        if($return_type == "data")
            return $data;


        else if ($return_type == "string") {
            $tags = "";
            if($data) {
                $k = 0;
                foreach($data as $d) {
                    $tags .= $d->$return_col;
                    $k++;
                    if($k < sizeof($data)) $tags .= $separator;
                }
            }

            return $tags;
        }

    }

    /*

        Function get_arts_by_tag()

        Esta función se usa para obtener atículos con una o varias categoría/s o etiqueta/s determinada/s
        Ejemplos:
            - Sirve para obtener un lsitado de noticias con una categoría específica.
            - Sirve para obtener un listado de artículos relacionados a otro por etiqueta/s

        Parámetros:

        $tag_id             [int / array] -> ID de tag o una array que contenga los ID de varios tags
        $tag_table          [string] -> Nombre de la tabla de tags. Ej: 'categorias'
        $exclude_art_id     [int / NULL] -> ID del art a excluir en el listado devuelto.
    */

    public function get_arts_by_tag($tag_id, $tag_table = "categories", $exclude_art_id = NULL) {

        // Si no hay tag_id
        if (!$tag_id) {
            return NULL;
        }

        // Si tag_id es un array
        if (is_array($tag_id)) {
            $where = "WHERE (";
            $k = 0;
            foreach($tag_id as $i) {
                $where .= "r.tag_id = $i->id ";
                $k++;
                if($k < sizeof($tag_id)) $where .= "OR ";
            }
            $where .= ") ";
        }

        // Si tag_id es un único valor
        else {
            $where = "WHERE r.tag_id = $tag_id ";
        }

        // Si se especifica un articulo para omitir
        if ($exclude_art_id) {
            $where .= "AND a.id != $exclude_art_id ";
        }

        // Order
        if($this->sort_allow) {
            $this->order = "ORDER BY a.pos $this->sort_order, a.id $this->sort_order";
        } else if($this->sort_by_column != "") {
            $this->order = "ORDER BY a.$this->sort_by_column $this->sort_order, a.id $this->sort_order";
        }

        $this->query = "SELECT a.* FROM $this->table a
                            INNER JOIN relations r
                                ON a.id = r.art_id
                            $where AND r.tag_ref = '$tag_table' AND r.art_ref = '$this->table' 
                            $this->order
                            $this->limit";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $sth->execute();

        $data = $sth->fetchAll();

        return $data;
    }

    // LAST ROW AFFECTED
    public function get_lastAffected() {
        return $this->lastAffected;
    }

    /*
        Function create_tags_selector()
        
        Función que devuelve un selector 'Select2' con relaciones

          $art_id               = [int] ID del registro cuyas opciones asociadas deberán marcarse con 'selected';
          $tag_ref              = [string] Nombre de la tabla relacionada. Ej: 'categories', 'tags', etc
          $allow_multiple_tags  = [bool] Define si se permite seleccionar múltiples opciones
          $allow_new_tags       = [bool] Define si se permite crear opciones on the fly
    */
    public function tags_selector($art_id, $tag_ref = "categories", $allow_multiple_tags = true, $allow_new_tags = true, $field_to_search = "title") {
        
        $multiple   = ($allow_multiple_tags) ? "multiple" : "";
        $allow_new  = ($allow_new_tags) ? "true" : "false";
        
        $tags_obj   = newClass($tag_ref);
        $rels_obj   = newClass("relations");
        $tags_data  = $tags_obj->get_data();
        
        
        $options_str = "";
        foreach($tags_data as $t) {
            $rels_obj->where = "WHERE art_ref = '$this->table' AND art_id = $art_id AND tag_ref = '$tag_ref' AND tag_id = $t->id";
            $sel = ($rels_obj->get_data()) ? "selected" : "";
            $options_str .= "<option value='$t->id' $sel>" . $t->$field_to_search . "</option>";
        }
        
        $empty_option = (!$multiple) ? "<option value=''></option>" : "";
        $selector_el = "<select class='tags-selector' data-tags='$allow_new' $multiple data-art-ref='$this->table' data-tag-ref='$tag_ref' data-art-id='$art_id' style='width:100%'>
                            $empty_option
                            $options_str
                        </select>";
        
        return $selector_el;
    }
    
    public function delete_relation($art_ref, $art_id, $tag_ref, $tag_id) {

        $this->query 	= "DELETE FROM $this->table WHERE art_ref = '$art_ref' AND art_id = $art_id AND tag_ref = '$tag_ref'";
        if ($tag_id)
            $this->query 	.= " AND tag_id = $tag_id";

        $sth 	= $this->db->prepare($this->query);
        $bool 	= $sth->execute();

        return $bool;
    }
    
    /*
        Function get_media_rows()

        Función que devuelve un listado de elementos <li> para usar en conjunto con Plupload

          $parent_class     = [string] Nombre de la clase 'parent'
          $parent_id        = [int] ID del registro parent
          $label            = [string] Tag que identifica a/los registro/s media
          $rpp              = [int] Cantidad de elemtnos devueltos
    */
    public function get_media_rows($parent_id = null, $label = NULL, $media_title = false, $rpp = NULL) {

        //$media_obj  = new media();
        $media_data = $this->get_media($parent_id, $label, $rpp);

        $media_rows = "";
        foreach ($media_data as $m) {
            
            $title_input = ($media_title != "false") ? "<input type='text' name='media_title' class='auto-update ignore-field' value='$m->media_title' placeholder='$media_title'>" : "";
            
            if ($m->file_type == "images") {
                if($m->file_ext == "svg")
                    $thum_el    = "<img class='fit-content' src='/media/images/src/$m->file_name'>";
                else
                    $thum_el    = "<img class='fit-content' src='/media/images/tiny/$m->file_name'>";
            }
                // Add class 'fit-content' to <img> to keep original aspect ratio
            else
                $thum_el    = "<div class='thum-$m->file_type'></div>";

            $media_rows .= "<li class='data-item' data-rel='$m->id' data-id='$m->id' data-pos='$m->pos'>
                                <div class='data-item-inner'>
                                    <div class='sort-handler'><span class='handler' title='"._("Arrastrar para ordenar")."'><i class='far fa-arrows-alt fa-lg fa-fw'></i></span></div>
                                    <div class='item-thum'>$thum_el</div>
                                    <div class='item-title'>
                                        $title_input
                                        <span class='help-text'>$m->file_name</span></div>
                                    <div class='item-opts'><a class='opt delete-image' data-rel='$m->id'><i class='fal fa-trash'></i></a></div>
                                </div>
                            </li>";
        }

        return $media_rows;
    }

    public function get_parent_title($parent_class, $parent_id, $column_name) {

        if($parent_class == $this->table) {
            $parent_row = $this->get_row($parent_id);
            return ($parent_row) ? $parent_row->$column_name : "";
        }

        else {
            $parent_obj = new main($parent_class);
            $parent_row = $parent_obj->get_row($parent_id);
            return ($parent_row) ? $parent_row->$column_name : "";
        }

        //return "Acá va el título del registro parent $parent_class";
    }
    
    // GET COLUMNS FROM TABLE
    public function get_columns() {

        global $db_name;
        
        $this->query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '$db_name' AND TABLE_NAME = '$this->table'";

        $sth = $this->db->prepare($this->query);
        $sth->setFetchMode(PDO::FETCH_ASSOC);
        $sth->execute();

        $data = $sth->fetchAll();

        $columns = [];
        foreach($data as $d) {
            array_push($columns, $d["COLUMN_NAME"]);
        }
        
        return $columns;

    }

    //////////////////////////////////
    // PAGINATION					//
    //////////////////////////////////

    public function preparePagination() {

        //$this->rows_per_page = $rpp;

        // Chequear si hay una página y si no se le asigna 1
        if (isset($_GET['pag']) && is_numeric($_GET['pag'])) {
            $this->pag = $_GET['pag'];
    //	} else if (isset($_SESSION['sess_pag']) && is_numeric($_SESSION['sess_pag'])) {
    //		$this->pag = $_SESSION['sess_pag'];
        } else {
            $this->pag = 1;
        }

        //$_SESSION['sess_pag'] = $this->pag;

        //$this->query	= "SELECT id FROM $this->table $this->where";

        $sth = $this->db->prepare($this->query);
        $sth->execute();
        $this->totalRows = $sth->rowCount();


        // Busca cual es la ultima pagina
        $this->last = ($this->totalRows != 0) ? ceil($this->totalRows/$this->rows_per_page) : 1;

        // Fuerza el valor de pagina para que no sea < 0 ni > (last page)
        if ($this->pag < 1) {
            $this->pag = 1;
        } 
        /*
        elseif ($this->pag > $this->last) {
            $this->pag = $this->last;
        }
*/
        // Establece el rango actual de filas para mostrar (se basa en total de paginas y cantidad de filas por pag)
        $this->limit = 'LIMIT ' . ($this->pag - 1) * $this->rows_per_page .',' .$this->rows_per_page;
    }


    public function getPagination() {

        $prev = $this->pag - 1;
        $next = $this->pag + 1;

        // Si hay un término de búsqueda (en front): $_GET["q"]
        if(isset($_GET["q"])) {
            $this->query = "q=" . $_GET["q"] . "&";
        }

        // Si hay un término de búsqueda (en back): $_GET["search"] & / $_GET["val"]
        else if(isset($_GET["search"]) && isset($_GET["val"])) {
            $this->query = "search=" . $_GET["search"] . "&val=" . $_GET["val"] . "&";
        }
        else {
            $this->query = "";
        }
        //$this->query = (isset($_GET["q"])) ? "q=" . $_GET["q"] . "&" : "";

        $pagNav = "<ul class='pagination'>";
        if($this->pag > 1) $pagNav .= "<li class='prev'><a href='?".$this->query."pag=$prev'></a></li>";

        $k = 0;
        while($k < $this->last) {
            $k++;
            $active = ($this->pag == $k) ? "active" : "";
            $pagNav .= "<li class='$active'><a href='?".$this->query."pag=$k'>$k</a></li>";
        }

        if($this->pag < $this->last) $pagNav .= "<li class='next'><a href='?".$this->query."pag=$next'></a></li>";
        $pagNav .= "</ul>";

        $this->pagNav = $pagNav;

        return $pagNav;
    }

    public function urls_amigables($url, $toLower = true) {

        //Rememplazamos caracteres especiales latinos
        $find 	= array('á', 'é', 'í', 'ó', 'ú', 'ñ', 'Á', 'É', 'Í', 'Ó', 'Ú', 'Ñ', 'ü');
        $repl 	= array('a', 'e', 'i', 'o', 'u', 'n', 'a', 'e', 'i', 'o', 'u', 'N', 'u');
        $url 	= str_replace($find, $repl, $url);

        // Añadimos los guiones
        $find 	= array(' ', '&', '\r\n', '\n', '+');
        $url 	= str_replace ($find, '-', $url);

        // Eliminamos y reemplazamos demás caracteres especiales
        $find 	= array('/[^a-zA-Z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
        $repl 	= array('', '-', '');
        $url 	= preg_replace ($find, $repl, $url);

        if($toLower) {
            // La función strtolower() no funciona para caracteres especiales
            $url 	= utf8_decode($url);
            $url 	= strtolower($url);
            $url 	= utf8_encode($url);
        }

        return $url;

    }

    public function create_unique_slug($value) {
        
        if($value) {

            $temp_slug = $this->urls_amigables($value);
            $slug = $temp_slug;

            $counter = 2;
            while ( $this->get_row(null, "slug", $slug) ) {
                $slug = $temp_slug . '-' . $counter;
                $counter++;
            }

            return $slug;
        }
        
        else {
            return $value;
        }
    }

    public function check_availability($column, $value, $id) {


        $this->query = "SELECT * FROM $this->table WHERE $column = :val AND id != :id";

        $sth = $this->db->prepare($this->query);
        $sth->bindParam(':id', $id);
        $sth->bindParam(':val', $value);
        $sth->execute();

        $data = $sth->fetchObject();

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        if($data) $result["success"] = 1;

        return $result;
    }


    /// CUSTOM
    public function translate($val) {
     
        global $lang;
        $val_en = $val . "_" . $lang;
        
        if ( isset($this->current_row->$val_en) && $this->current_row->$val_en != "" )
            return $this->current_row->$val_en;
        else
            return $this->current_row->$val;
    }
    
}

?>
