<?php
class productos extends main {

    //
    // Listings
    //
    // Allow manual reoredering of records [bool]
    public $sort_allow			            = true;
    // New items will be positioned at the top [ASC] or at the bottom [DESC]
    public $sort_order                      = "ASC";
    // If $sort_allow, use this colum to sort rows
    public $sort_by_column                  = NULL;
    // [string] Rewrite custom orden (if $sort_allow == false)
    // public $order                           = "ORDER BY fecha DESC, id DESC";

    //
    // Adding
    //
    // Automatically create a slug when creating a row [bool] - (slug column must exist in MySQL table)
    public $auto_slug		                 = true;

    //
    // Edition
    //

    //
    // Table relations
    //
    // [string | NULL] Nombre de la lase parent, si hay
    public $parent_class            		= NULL;
    // [bool] Permitir una clase con parent, mostrar contenido sin parent_id
    public $allow_no_parent                 = NULL;
    // [string | NULL] Nombre de la lase child, si hay
    public $child_class            		    = NULL;
    // [string] Texto del botón que edita el listado child
    public $child_edit_btn_text             = NULL;

    //
    // Media
    //
    public $custom_images_sizes = [

        "thumbnail" => [

            "small" => [
                "max_width"         => 1280,
                "max_height"        => null,        // 720,
                "best_fit"          => false,
                "allow_scale_up"    => false
            ],

        ]
    ];

    //
    // Static categories
    //
    public $categories				= array("educacion"    => "Educación",
                                            "cultura"      => "Cultura",
                                            "diseno"       => "Diseño",
                                            "deporte"       => "Deporte",
                                            "aire-libre"       => "Aire libre");

    public $tags				    = array("educacion"    => "Educación",
                                            "cultura"      => "Cultura",
                                            "diseno"       => "Diseño",
                                            "deporte"       => "Deporte",
                                            "aire-libre"       => "Aire libre");
    
    
    // CUSTOMIZE FUNCTION DUPLICATE ROW
    /*
    public function duplicate_row($id, $new_parent_id = null) {

        $row = $this->get_row($id);
        $new_slug = $this->create_unique_slug($row->slug);

        $pos = $this->highest_pos() + 1;
        $this->lastPos = $pos;

        $created_by = $_SESSION['adminuser']->id;

        $this->query = "INSERT INTO $this->table
                        (parent_id, title, slug, subtitle, text, category, tags, date_from, date_to, created_by, featured, pos, status)
                    SELECT
                        parent_id, CONCAT(title, ' - Copia de $id'), '$new_slug', subtitle, text, category, tags, date_from, date_to, '$created_by', featured, '$pos', 'draft'
                    FROM
                        $this->table
                    WHERE
                        id = $id;";

        $sth 	= $this->db->prepare($this->query);

        $result = array("success" => 0, "error" => "", "message" => "", "row_id" => null);

        try {

            $bool 	            = $sth->execute();
            $result["success"]  = 1;
            $result["message"]  = "El registro fue duplicado correctamente.";
            $this->lastAffected = $this->db->lastInsertId();

            $media_obj = new media();
            $media_obj->duplicate_media_from_parent($id, $this->lastAffected);
        }

        catch(PDOException $e) {

            $result["error"] = $e->errorInfo[2];
        }

        return $result;

    }
*/

}

?>
