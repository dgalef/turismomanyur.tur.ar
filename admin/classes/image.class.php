<?php

class image {

    public $width;
    public $height;
    public $format;
    
    public $dest_ratio;
    public $original_ratio;
    
    public $src_image;
    public $original_pathname;
    
    //

    public function __construct($original_pathname) {
        
        $this->src_image = new Imagick($original_pathname);
        $this->src_image = $this->src_image->coalesceImages();

        $this->src_image->setImageCompressionQuality (80);
        $this->src_image->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        
        $this->format = $this->src_image->getImageFormat();
        
        $this->original_pathname = $original_pathname;
        
    }

    function createImage($size, $params)
    {
        
        $max_width      = $params['max_width'] ? $params['max_width'] : 0;
        $max_height     = $params['max_height'] ? $params['max_height'] : 0;
        $best_fit       = $params['best_fit'];
        $allow_scale_up = $params['allow_scale_up'];
        
        $dest_ratio     = ($max_width && $max_height) ? $max_width / $max_height : null;
        
        
        $image = $this->src_image;
        
        $original_width     = $image->getImageWidth();
        $original_height    = $image->getImageHeight();
        
        $original_ratio     = $original_width / $original_height;
        
        $this->dest_ratio = $dest_ratio;
        $this->original_ratio = $original_ratio;
        
        // SCALE, don´t crop
        if ( $best_fit ) {
            
            // Scale if original image is larger than dest or if scale up is allowed
            if (($max_width < $original_width || $max_height < $original_height) || $allow_scale_up) {
                
                foreach ($image as $frame)
                    $frame->scaleImage($max_width, $max_height, $best_fit);  
            }
        }
        
        // SCALE, don´t crop (match 'max_width' OR 'max_height' - whichever is not null)
        else if ( !$max_width || !$max_height) {

            // Scale to match max_width
            if ($max_width && ($max_width < $original_width || $allow_scale_up)) {
                
                foreach ($image as $frame)
                    $frame->scaleImage($max_width, $max_height);  
            }

            // Scale to match max_height
            if ($max_height && ($max_height < $original_height || $allow_scale_up)) {
                
                foreach ($image as $frame)
                    $frame->scaleImage($max_width, $max_height);  
            }
        }
        
        // SCALE, don´t crop (original and dest ratio are the same)
        else if ($original_ratio == $dest_ratio) {

            // Scale down (or scale up if allowed)
            if ($max_height < $original_height || $allow_scale_up) {
                
                foreach ($image as $frame)
                    $frame->scaleImage($max_width, $max_height);  
            }
        }
        
        // CROP 
        else {
            
            // Si la imagen destino tiene un ratio menor al de la imagen original
            if ($dest_ratio < $original_ratio)
            {

                // Scale image to match height and crop to match dest ratio
                if ($max_height < $original_height || $allow_scale_up) {
                    
                    foreach ($image as $frame) {

                        $frame->scaleImage(0, $max_height);
                        
                        $x      = round(($image->getImageWidth() - $max_width) / 2);
                        $y      = 0;

                        // Crop image
                        $image->cropImage($max_width, $max_height, $x, $y);
                        $frame->setImagePage($max_width, $max_height, 0, 0);
                    }
                }

                // Don´t scale, just crop to match dest ratio
                else {

                    $new_width  = ($original_height * $max_width) / $max_height;

                    $x          = round(($image->getImageWidth() - $new_width) / 2);
                    $y          = 0;

                    foreach ($image as $frame) {
                        $frame->cropImage($new_width, $original_height, $x, $y);
                        $frame->setImagePage($new_width, $original_height, 0, 0);
                    }
                }
            }

            // Si la imagen destino tiene un ratio mayor al de la imagen original
            else if ($dest_ratio > $original_ratio) {
                
                // Scale image to match width and crop to match dest ratio
                if ($max_width < $original_width || $allow_scale_up) {

                    foreach ($image as $frame) {

                        $frame->scaleImage($max_width, 0);

                        $x      = 0;
                        $y      = round(($image->getImageHeight() - $max_height) / 2);

                        // Crop image
                        $frame->cropImage($max_width, $max_height, $x, $y);
                        $frame->setImagePage($max_width, $max_height, 0, 0);
                    }
                }
                
                // Don´t scale, just crop to match dest ratio
                else {
                    
                    $new_height  = ($original_width * $max_height) / $max_width;

                    $x          = 0;
                    $y          = round(($image->getImageHeight() - $new_height) / 2);
                        
                    foreach ($image as $frame) {
                        $frame->cropImage($original_width, $new_height, $x, $y);
                        $frame->setImagePage($original_width, $new_height, 0, 0);
                    }
                }
            }
        }
        
        $this->width 	= $image->getImageWidth();
        $this->height 	= $image->getImageHeight();

        /*********/
        
        $dest_folder 	= str_replace('//', '/', $_SERVER['DOCUMENT_ROOT'] . "/media/images/" . $size);
        if (!file_exists($dest_folder)) mkdir($dest_folder, 0777, true);
        $dest_pathname 	= $dest_folder . '/' . substr($this->original_pathname, strrpos($this->original_pathname, "/")+1);;

        if ( strtolower($this->format) == "gif" )
            $image = $image->optimizeImageLayers();
        else
            $image = $image->deconstructImages();

        $image->writeImages($dest_pathname, true);

    }
}

?>