<?php
require_once "admin/php/config.php";

$slider_obj = newClass("slider");
$slider_data = $slider_obj->get_data("published");

$packages_obj = newClass("packages");
$packages_obj->where = "WHERE featured = 1";
$packages_obj->order = "ORDER BY pos ASC, id DESC";
$packages_data = $packages_obj->get_data("published", 3);

$destinations_obj = newClass("destinations");
$destinations_data = $destinations_obj->get_data("published");

?>

<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = "";
    $page_desc = "";
    require_once "inc/head.php"; 
    ?>
</head>

<body id="home-pg" data-page="home">

    <?php include("inc/header.php"); ?>

    <!-- Slider -->
    <div class="main-slider-wrapper">
        <div id="main-slider" class="swiper-container">
            <div class="swiper-wrapper">
               
                <?php
                foreach ( $slider_data as $s ) {
                    
                    $copete = ($s->subtitle) ? "<p class='copete'>$s->subtitle</p>" : "";
                    $cta_el = "";
                    if ( $s->cta_text && $s->cta_link)
                        $cta_el = "<a class='secondary button' href='$s->cta_link'>$s->cta_text</a>";
                    
                    $img_el = $slider_obj->get_img($s->id);
                    
                    echo "<div class='swiper-slide'>
                                <div class='slide-inner'>
                                    $copete         
                                    <h2>$s->title</h2>
                                    <div class='slide-footer'>
                                        <span>$s->subtitle_2</span>
                                        $cta_el
                                    </div>
                                </div>
                                <div class='slide-bgr'>
                                    $img_el
                                </div>
                            </div>";
                }
                ?>
                
            </div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
            <div class="swiper-pagination"></div>
        </div>
    </div>

    <?php if ( $packages_data ) { ?>
    <section id="featured-packages">
        <div class="grid-container">

            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">
                    <h2 class="underline">PAQUETES DESTACADOS</h2>    
                </div>
            </div>
            
            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">
                    
                    <div class="packages-grid large">
                        
                        <?php
                        foreach ( $packages_data as $p )
                            include "inc/package-item.php";
                        ?>
                    </div>
                    
                </div>
            </div>

        </div>
    </section>
    <?php } ?>
    
    <section id="destinations">
        <div class="grid-container">

            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">
                    <h2 class="underline">DESTINOS IMPERDIBLES</h2>    
                </div>
            </div>
            
            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">

                    <div class="destinations-grid">

                        <?php
                        foreach ( $destinations_data as $d )
                            include "inc/destination-item.php";
                        ?>
                    </div>

                </div>
            </div>

        </div>
    </section>
    
    <?php require_once "inc/newsletter.php"; ?>

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

</body>

</html>
