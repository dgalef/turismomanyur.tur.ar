<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link type="image/x-icon" rel="shortcut icon" href="/img/favicon.png">

<?php

$site_title = "Turismo Manyur";
$head_title = (isset($page_title) && $page_title) ? $site_title . " - " . $page_title : $site_title;
echo "<title>$head_title</title>";

$desc = (isset($page_desc) && $page_desc) ? $page_desc : "";
echo "<meta name='description' content='$desc'>";

?>

<link rel="stylesheet" href="/js/swiper-5.1/css/swiper.min.css">
<link rel="stylesheet" href="/css/app.css?v=6">

<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-204606841-1">
</script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-204606841-1');
</script>
