<footer>
    <div class="grid-container">
       
        <div class="grid-x">
            <div class="medium-12 cell">
                <div class="logo">
                    <img src="/img/logo-manyur-white.svg" alt="Turismo Manyur"><br>
                    <p>EVT. Leg.. 15320  /  Disp. 1770/2012</p>
                </div>
            </div>
        </div>
        <div class="grid-x">
            <div class="medium-5 cell">
                <ul class="contact-details">
                    <li>
                        <p>
                            <a href="tel:+5491150390861" target="_blank">11.5039.0861</a><br>
                            <a href="mailto:info@turismomanyur.tur.ar" target="_blank">info@turismomanyur.tur.ar</a>
                        </p>
                    </li>
                    <li><a href="https://es-la.facebook.com/pages/category/Travel-Agency/Turismo-Manyur-366740603418566/" target="_blank"><i class='fab fa-facebook-f'></i></a></li>
                    <li><a href="https://www.instagram.com/turismomanyurok/?hl=es" target="_blank"><i class='fab fa-instagram'></i></a></li>
                    <li><a href="https://turismomanyur.e-agencias.com.ar/" target="_blank"><i class='fas fa-shopping-cart'></i></a></li>
                    
                </ul>
            </div>
            <div class="medium-7 cell">
                <div class="footer-blocks">
                    <div class="destinations block">
                        <ul>
                            <?php
                            if (!isset($destinations_obj))
                                $destinations_obj = newClass("destinations");
                            $destinations_data = $destinations_obj->get_data();
                            
                            foreach ( $destinations_data as $d ) {
                                echo "<li><a href='/destinos/$d->slug'>$d->title</a></li>";
                            }
                            ?>
                        </ul>
                    </div>
                    <div class="other block">
                        <ul>
                            <?php
                            $static_pages_obj = newClass("static_pages");
                            $static_pages_data = $static_pages_obj->get_data("published");
                            foreach ($static_pages_data as $sp) {
                                if ($sp->status == "published")
                                    echo "<li><a href='/info/$sp->slug'>$sp->title</a></li>";
                            }
                            ?>
                            <li><a href='/contacto'>Contacto</a></li>
                        </ul>
                    </div>
                    <div class="afip block">
                        <a href="http://qr.afip.gob.ar/?qr=j1GT2DEV20Xqu6G29cAmOw,," target="_F960AFIPInfo"><img style="width:70px" src="http://www.afip.gob.ar/images/f960/DATAWEB.jpg" border="0"></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

<div class="bottom-bar">
    <div class="grid-container">
        <div class="grid-x grid-padding-x text-center">
            <div class="cell">
                Desarrollado por <a href="https://estudioestratega.com" target="_blank" title="Diseño gráfico y desarrollo web">Estudio Estratega</a>
            </div>
        </div>
    </div>
</div>


<div class="whatsapp-widget">
    <a href="https://wa.me/5491150390861" target="_blank" class=""><img src='/img/whatsappimg.png'></a>
</div>
