<?php

$img_el = $packages_obj->get_img($p->id, "srcset", "thumbnail");
$package_url = "/paquetes/$p->slug";

echo "<div class='package-item'>
            <a href='$package_url'>
                <div class='overlay'>
                    <div class='inner'>
                        <h3>$p->title</h3>
                        <p class='price'>$p->price</p>

                        <span class='small _white hollow button'>Más info</span>
                    </div>
                </div>
                <div class='img-wrapper'>
                    <div class='img'>
                        $img_el
                    </div>
                </div>
            </a>
        </div>";

?>