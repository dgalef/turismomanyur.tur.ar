<!--
    data-pos: relative, absolute, fixed
    data-fix-delay: false, [pixels scrolled] -- specify pixels scrolled to add 'is-fixed' class
    data-width: fullwidth, container
    data-mobile-menu: offcanvas, overlay
    
    Ofcanvas require add [data-toggle='offCanvas'] to button.hambureguer - Remove this to get Overlay Menu
-->
<header data-pos="relative" data-fix-delay="100" data-width="fullwidth" data-mobile-menu="overlay">

    <div class="header-container">

        <div class="header-inner">

            <!-- Desktop logo -->
            <div class="top-logo"><a href='/'><img src='/img/logo-manyur-black.svg'></a></div>

            <!-- Hambuerger Icon -->
            <button class="hamburger hamburger--collapse hide-for-large" type="button">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>

            <nav>

                <!-- Overlay menu logo (only add if it is different from the top-bar-logo) -->
                <div class="overlay-menu-logo hide"></div>

                <!-- Accordion large-dropdown menu -->
                <ul class="vertical large-horizontal menu main-menu accordion-menu" data-responsive-menu="accordion large-dropdown">
                   
                    <?php
                    require_once "admin/php/config.php";
                    $feat_destinations_obj = newClass("destinations");
                    $feat_destinations_obj->where = "WHERE featured = 1";
                    $feat_destinations_data = $feat_destinations_obj->get_data("published");
                    foreach( $feat_destinations_data as $fd ) {
                        
                        echo "<li><a href='/destinos/$fd->slug' data-page='$fd->slug'>$fd->title</a></li>";
                    }
                    ?>
                    
                </ul>
                
                <ul class="contact-details">
                    <li>
                        <p>
                            <a href="tel:+5491150390861" target="_blank">11.5039.0861</a><br>
                            <a href="mailto:info@turismomanyur.tur.ar" target="_blank">info@turismomanyur.tur.ar</a>
                        </p>
                    </li>
                    <li><a href="https://es-la.facebook.com/pages/category/Travel-Agency/Turismo-Manyur-366740603418566/" target="_blank"><i class='fab fa-facebook-f'></i></a></li>
                    <li><a href="https://www.instagram.com/turismomanyurok/?hl=es" target="_blank"><i class='fab fa-instagram'></i></a></li>
                    <li><a href="https://turismomanyur.e-agencias.com.ar/" target="_blank"><i class='fas fa-shopping-cart'></i></a></li>
                </ul>

            </nav>

        </div> <!-- /header-inner -->

    </div> <!-- /header-container -->

</header>