<?php

$img_el = $destinations_obj->get_img($d->id, "srcset", "thumbnail");
$destination_url = "/destinos/$d->slug";

echo "<div class='destination-item'>
            <a href='$destination_url'>
                <div class='overlay'>
                    <div class='inner'>
                        <h3>$d->title</h3>
                    </div>
                </div>
                <div class='img-wrapper'>
                    <div class='img'>
                        $img_el
                    </div>
                </div>
            </a>
        </div>";
?>