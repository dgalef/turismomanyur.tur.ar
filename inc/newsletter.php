<section id="newsletter">
    <div class="grid-container">

        <div class="grid-x grid-padding-x">
            <div class="large-12 cell">
                <div class="news">
                    <div class="nl-title">
                        <i class="fal fa-envelope-open"></i>
                        <span>Dejanos tu email<br>
                            y recibí todas las novedades</span>
                    </div>
                    <div class="nl-form">
                        <form novalidate data-abide>
                            <input type="email" name="email" placeholder="nombre@email.com" required>
                            <button id="nl-send" type="submit" class="secondary button ">ENVIAR</button>
                            <div class="response"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
</section>