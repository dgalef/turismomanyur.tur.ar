var delay                   = false,            // Pixels escrolled before header gets fixed.
    breakpoint              = 1024,             // Window width in pixles from where the nav gets visible
    pos                     = 'relative',
    is_fixed                = false,
    header_height,
    scrolled;

/**************************************/
/*      on Resize Event, recalculate  */
/*      Requires js/lodash.min.js     */
/**************************************/
function recalc() {
    header_height    = $(".header-container").outerHeight();
    $(".header-placeholder").css("height", header_height);    
}
recalc();
window.addEventListener('resize', _.throttle(recalc, 16), false);

$(function() {
    
    // 
    delay           = $("header").data("fix-delay");
    pos             = $("header").data("pos");
    
    // A header that is relative positioned and becomes fixed, jumps. This is prevented by adding a placeholder at the same height as header
    if(pos == 'relative' && delay) {
        $("<div class='header-placeholder'></div>").css("height", header_height).insertAfter("header");    
        $(".header-container").css("position", "absolute");
    }
    
    // If header must be moved 
    if(delay && pos != 'fixed') 
       $(".header-container").addClass("moveit")
    

    $(document).on("scroll", function() {

        scrolled = $(document).scrollTop();

        if(delay) {
            
            if (scrolled > delay && !is_fixed) {
                
                $("header").addClass("is-fixed");
                
                // If it is not fixed by default, move it
                if(pos != 'fixed') 
                    TweenMax.to(".header-container", .4, {y:0, ease: Power3.easeInOut});
                
            } else if (scrolled <= 1) {
                
                $("header").removeClass("is-fixed");
                
                // If it is not fixed by default, move it
                if(pos != 'fixed') 
                    TweenMax.set(".header-container", {clearProps:"y"});
                
                recalc();
            }
        }
        
        

    });
    
    /* ******************************************************************************** */

    /*
        Menu auto active
    */
    var currPathname =  window.location.pathname;
    $("[href='" + currPathname + "']").addClass("active");
    var pageId = $("body").data("page");
    $("[data-page='" + pageId + "']").addClass("active");
    
    /* ******************************************************************************** */

    /*
        HAMBURGER ICON
        Requires include hamburger_settings.scss & hamburger.scss in app.scss
    */

    
    $(".hamburger").on("click", this, function(e) {
        e.preventDefault();

        // OffCanvas Menu
        if($("header").data("mobile-menu") == "offcanvas") {
            $('#offCanvas').foundation('toggle');
        }
        
        // Overlay Menu
        else {
            
            if( $(this).is(".is-active") ) {
                resetMobileMenu();
            }

            else {
                $("body").addClass("show-menu");
                $("nav").addClass("on");
                $(this).addClass("is-active");

            }
        }
    });

    function resetMobileMenu() {
        $(".hamburger").removeClass("is-active");
        $("body").removeClass("show-menu");
        $("nav").removeClass("on");
    }

    /* ******************************************************************************** */

    /*
        SCROLL TO SECTION
        Requires TweenMax and ScrollTo plugin
    */

    $("[data-scroll]").on("click", function(e){

        var href 	= $(this).attr("href");
        var pos     = href.lastIndexOf("#");
        var hash	= href.substr(pos);

        // Check if $(hash) exists
        if ( $(hash).length > 0 ) {
            console.log("hay hash")
            // Don't load any page
            e.preventDefault();

            // Reset mobile menu
            if($(window).width() < breakpoint) resetMobileMenu();

            // Deactivate & activate menus as needed
            $("[data-scroll]").removeClass("active");
            $(this).addClass("active");

            // Scroll to section block
            var $root 	= $('html, body');
            var y       = $(hash).offset().top;
            y          -= 0;    // Fixed topbar height
            var offsetY = 0;    //header_height;
            TweenMax.to(window, 1, {scrollTo:{y:$(hash).offset().top, offsetY: offsetY, autoKill:false}, ease:Power3.easeInOut});

        }

        // Check If href starts with something else (not a hash)...
        else if ( pos >= 1 ) {
            // Go to URL

        }

        // No section to scroll to
        else {
            console.log("No existe hash")
            e.preventDefault();
        }


    });

    /* ******************************************************************************** */

});
