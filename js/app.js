$(document).foundation();

/**************************************/
/*      CSS vh property hack          */
/*      Requires js/lodash.min.js     */
/**************************************/
// Set vh in CSS (hack to get real vh on iOS Safari & Chrome)
function set_vh() {
    var vh = window.innerHeight * 0.01;
    document.documentElement.style.setProperty('--vh', `${vh}px`);
}
set_vh();
window.addEventListener('resize', _.throttle(set_vh, 16), false);

/********************/
/*    PRELOADER     */
/********************/
$(window).on("load", function() {
    console.log("loaded");
    $("#preloader").removeClass("on");

});


$(function(){
    
    /********************/
    /*      SWIPER      */
    /********************/
    
    var mainSlider = new Swiper("#main-slider", {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        speed: 1000,
        autoplay: {
            delay: 4000,
        },
        on: {
            touchMove: function() {
                this.params.speed = 300;
            },
            transitionEnd: function(swiper) {
                this.params.speed = 1000;

            }
        }
    });
    
    var packageSlider = new Swiper("#package-slider", {
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        speed: 1000,
        autoplay: {
            delay: 4000,
        },
        on: {
            touchMove: function() {
                this.params.speed = 300;
            },
            transitionEnd: function(swiper) {
                this.params.speed = 1000;

            }
        }
    });
    
    /********************/
    /*     WHATSAPP     */
    /********************/
    
    setTimeout(function(){
        $(".whatsapp-widget").addClass("show");
    }, 1000);
    
});


/********************/
/*      FORMS       */
/********************/
$(document)
    .on("forminvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + ev.target.id + " no es válido");
})
    .on("formvalid.zf.abide", function (ev, frm) {
    console.log("Form id " + frm.attr('id') + " es válido");

    grecaptcha.ready(function() {
        grecaptcha.execute('6LcDqpMbAAAAADKeZncII7pyksOktBmb9y6KAJk4', {action: 'contacto'}).then(function(token) {
            frm.prepend('<input type="hidden" name="token" value="' + token + '">');
            frm.prepend('<input type="hidden" name="action" value="contacto">');
            doSubmitForm( frm )

        });;
    });
})
    .on("submit", function (ev) {
    ev.preventDefault();
});

function doSubmitForm(frm) {
    frm.find("button").addClass("disabled loading").attr("disabled", "disabled");

    var formdata = false;
    if (window.FormData)
        formdata = new FormData(frm[0]);

    var jqxhr = $.ajax({
        url         : "/php/form-sender.php",
        data        : formdata ? formdata : frm.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){

            var resp	= $.parseJSON(data);

            var success	= resp.success;
            var mssg 	= resp.message;

            if(success == 1) {

                console.log("Formulario enviado")
                frm.find(".response").html(mssg)
                frm.find("button").removeClass("loading");

            } else {
                alert(mssg);
                frm.find("input[type='file']").val('');
                frm.find("button").removeClass("disabled loading").removeAttr("disabled");
            }
        }
    })
    .done(function() { })
    .fail(function( jqXHR, textStatus, errorThrown ) {
        console.log( "textStatus:" + textStatus + "\r\n errorThrown: " + errorThrown );
    })
    .always(function() { });
}