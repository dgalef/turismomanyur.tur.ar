<?php
require_once "admin/php/config.php";

if(isset($_GET["slug"])) $slug = $_GET["slug"];
else die("Slug not found");

$destination_obj   = newClass("destinations");
$destination_row   = $destination_obj->get_row(null, "slug", $slug);

// Chequear si existe y si está publicado
if(!$destination_row) die("Row not found");
if($destination_row->status != "published") die("Row not available");

$packages_obj = newClass("packages");
$packages_data = $packages_obj->get_arts_by_tag($destination_row->id, "destinations");

$img_el = $destination_obj->get_img($destination_row->id, "srcset", "banner");

?>

<!doctype html>
<html class="no-js" lang="es">

<head>
    <?php 
    $page_title = $destination_row->title;
    $page_desc = "";
    require_once "inc/head.php"; 
    ?>
</head>

<body id="destino-pg" data-page="destino">

    <?php include("inc/header.php"); ?>

    <div class="hero">
        <?php echo $img_el; ?> 
    </div>

    <section id="packages">
        <div class="grid-container">

            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">
                    <h2 class="underline">
                        <?php echo $destination_row->title; ?>
                    </h2>    
                </div>
            </div>
            
            <div class="grid-x grid-padding-x text-center">
                <div class="large-12 cell">
                    
                    <div class="packages-grid">
                        
                        <?php
                        foreach ( $packages_data as $p )
                            include "inc/package-item.php";
                        ?>
                    </div>
                    
                </div>
            </div>

        </div>
    </section>
    
    <?php require_once "inc/newsletter.php"; ?>

    <?php require_once "inc/footer.php"; ?>
    
    <div id="preloader" class="on"><div class="preinner"></div></div>
    
    <?php require_once "inc/scripts.php"; ?>

</body>

</html>
