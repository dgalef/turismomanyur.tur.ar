-- phpMyAdmin SQL Dump
-- version 4.9.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 10.0.10.79:3306
-- Tiempo de generación: 02-08-2021 a las 17:19:08
-- Versión del servidor: 10.3.23-MariaDB
-- Versión de PHP: 7.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `estratest_manyur`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_rols`
--

CREATE TABLE `admin_rols` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'inactive'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_rols`
--

INSERT INTO `admin_rols` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Administrador', 'administrador', NULL, NULL, NULL, NULL, '2019-11-12 18:15:13', NULL, NULL, NULL, 0, 1, 'published'),
(2, NULL, 'Editor', 'editor', NULL, NULL, NULL, NULL, '2019-11-12 21:19:18', NULL, NULL, NULL, 0, 2, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_users`
--

CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `first_name` char(60) DEFAULT NULL,
  `last_name` char(60) DEFAULT NULL,
  `email` char(80) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `rol_id` tinyint(1) DEFAULT NULL,
  `phone_number` char(30) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_users`
--

INSERT INTO `admin_users` (`id`, `parent_id`, `first_name`, `last_name`, `email`, `password`, `salt`, `rol_id`, `phone_number`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Alejandro', 'Ferreiro', 'dg.alef@gmail.com', '4d0d8e5c8907af4c1868e7ceced85b6e8e75d27f79a7bd767017ac53afef4f5b', '19dab902211ae42c', 1, NULL, '', '', NULL, '', NULL, NULL, '2019-11-12 23:38:09', NULL, '2020-11-27 18:27:35', '1', 0, 0, 'published'),
(2, NULL, 'Mauro', 'Mansilla', 'mmansilla@turismomanyur.tur.ar', '6f65a42d7ee603a9dfa84eca9986f41279aa6b18e29d356ddc33a6d5a7ef7e98', '7d20976b1503ae6e', 1, NULL, NULL, NULL, NULL, 'M@uro@Mans1lla', NULL, NULL, '2021-07-19 19:54:55', '1', '2021-07-19 19:56:41', '1', 0, 1, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinations`
--

CREATE TABLE `destinations` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `destinations`
--

INSERT INTO `destinations` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Argentina', 'argentina', '', '', NULL, NULL, NULL, NULL, '2021-07-13 14:04:36', '1', '2021-07-13 21:55:52', '1', 1, 1, 'published'),
(2, NULL, 'USA & Canadá', 'usa-canada', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:06:46', '1', '2021-07-16 22:09:07', '1', 1, 4, 'published'),
(3, NULL, 'Brasil', 'brasil', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:06:57', '1', '2021-07-13 21:55:53', '1', 1, 2, 'published'),
(4, NULL, 'Caribe', 'caribe', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:07:07', '1', '2021-07-13 21:55:54', '1', 1, 3, 'published'),
(5, NULL, 'Europa', 'europa', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:26:08', '1', '2021-07-13 21:55:55', '1', 1, 5, 'published'),
(6, NULL, 'Exóticos', 'exoticos', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:26:52', '1', '2021-07-16 22:08:13', '1', 1, 6, 'published'),
(7, NULL, 'Salidas grupales', 'salidas-grupales', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:27:00', '1', '2021-07-16 22:09:58', '1', 1, 7, 'published'),
(8, NULL, 'Cruceros', 'cruceros', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:27:12', '1', '2021-07-16 22:08:37', '1', 1, 8, 'published'),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 17:57:16', '2', NULL, NULL, 0, 9, 'temp'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 17:58:25', '2', NULL, NULL, 0, 11, 'temp'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 18:01:07', '2', NULL, NULL, 0, 12, 'temp'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 18:03:13', '2', NULL, NULL, 0, 13, 'temp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

CREATE TABLE `media` (
  `id` int(11) NOT NULL,
  `parent_class` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `file_name` varchar(150) DEFAULT NULL,
  `file_source_name` varchar(150) DEFAULT NULL,
  `file_ext` char(5) DEFAULT NULL,
  `file_type` char(20) DEFAULT NULL,
  `file_sizes` varchar(300) DEFAULT NULL,
  `media_title` text DEFAULT NULL,
  `media_description` text DEFAULT NULL,
  `label` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT 0,
  `pos` int(11) DEFAULT NULL,
  `status` char(20) DEFAULT 'published'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `media`
--

INSERT INTO `media` (`id`, `parent_class`, `parent_id`, `file_name`, `file_source_name`, `file_ext`, `file_type`, `file_sizes`, `media_title`, `media_description`, `label`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(3, 'destinations', 1, 'argentina_0.jpg', 'argentina.jpg', 'jpg', 'images', '{\"src\":\"3187x630\",\"xlarge\":\"2880x569\",\"large\":\"1920x380\",\"medium\":\"1280x253\",\"small\":\"640x127\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:18:06', '1', NULL, NULL, 0, 3, 'published'),
(4, 'destinations', 1, 'argentinathum.jpg', 'argentina_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:18:14', '1', NULL, NULL, 0, 4, 'published'),
(5, 'destinations', 3, 'brasil_0.jpg', 'brasil.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:18:36', '1', NULL, NULL, 0, 5, 'published'),
(6, 'destinations', 3, 'brasilthum.jpg', 'brasil_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:18:43', '1', NULL, NULL, 0, 6, 'published'),
(7, 'destinations', 4, 'caribe.jpg', 'caribe.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:26:02', '1', NULL, NULL, 0, 7, 'published'),
(8, 'destinations', 4, 'caribethum.jpg', 'caribe_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:26:06', '1', NULL, NULL, 0, 8, 'published'),
(9, 'destinations', 5, 'europa.jpg', 'europa.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:26:20', '1', NULL, NULL, 0, 9, 'published'),
(10, 'destinations', 5, 'europathum.jpg', 'europa_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:26:25', '1', NULL, NULL, 0, 10, 'published'),
(17, 'slider', 1, 'fitz-roy.jpg', 'Fitz Roy.jpg', 'jpg', 'images', '{\"src\":\"2000x1369\",\"xlarge\":\"2000x1369\",\"large\":\"1920x1314\",\"medium\":\"1280x876\",\"small\":\"640x438\",\"tiny\":\"200x137\"}', '', '', 'slide', '2021-07-13 19:25:33', '1', NULL, NULL, 0, 17, 'published'),
(18, 'packages', 1, 'pnt-seleccion-56_0.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'thumbnail', '2021-07-13 20:05:02', '1', NULL, NULL, 0, 18, 'published'),
(19, 'packages', 1, 'pnt-seleccion-56_1.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'gallery', '2021-07-13 20:05:24', '1', NULL, NULL, 0, 19, 'published'),
(20, 'packages', 1, 'talampaya-2-argentinayelmundo_0.jpg', 'Talampaya 2 @argentinayelmundo.jpg', 'jpg', 'images', '{\"src\":\"1080x1080\",\"xlarge\":\"1080x1080\",\"large\":\"1080x1080\",\"medium\":\"1080x1080\",\"small\":\"640x640\",\"tiny\":\"200x200\"}', '', '', 'gallery', '2021-07-13 20:05:26', '1', NULL, NULL, 0, 20, 'published'),
(21, 'packages', 1, 'talampaya-5-facundocarrizo24_0.jpg', 'Talampaya 5 @facundo_carrizo24.jpg', 'jpg', 'images', '{\"src\":\"1080x1080\",\"xlarge\":\"1080x1080\",\"large\":\"1080x1080\",\"medium\":\"1080x1080\",\"small\":\"640x640\",\"tiny\":\"200x200\"}', '', '', 'gallery', '2021-07-13 20:05:27', '1', NULL, NULL, 0, 21, 'published'),
(22, 'slider', 2, 'pnt-seleccion-56_2.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'slide', '2021-07-13 23:00:42', '1', NULL, NULL, 0, 22, 'published'),
(28, 'packages', 2, 'puerto-madryn.jpg', 'Puerto-Madryn.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 21:43:33', '1', NULL, NULL, 0, 28, 'published'),
(29, 'packages', 2, 'puerto-madryn_0.jpg', 'Puerto-Madryn.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'gallery', '2021-07-16 21:43:41', '1', NULL, NULL, 0, 29, 'published'),
(30, 'destinations', 6, 'bali.jpg', 'bali.jpg', 'jpg', 'images', '{\"src\":\"4320x2880\",\"xlarge\":\"2880x1920\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-16 22:08:13', '1', NULL, NULL, 0, 30, 'published'),
(32, 'destinations', 2, 'lake-tahoe-kayaking-tours.jpg', 'lake-tahoe-kayaking-tours.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 22:09:05', '1', NULL, NULL, 0, 32, 'published'),
(33, 'destinations', 7, 'iguazu.jpg', 'iguazu.jpg', 'jpg', 'images', '{\"src\":\"4608x3072\",\"xlarge\":\"2880x1920\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-16 22:09:57', '1', NULL, NULL, 0, 33, 'published'),
(34, 'destinations', 8, 'carnaval-en-crucero-costa-fascinosa.jpg', 'Carnaval-en-Crucero-Costa-Fascinosa.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 22:10:30', '1', NULL, NULL, 0, 34, 'published'),
(41, 'packages', 3, 'images.jpg', 'images.jpg', 'jpg', 'images', '{\"src\":\"259x194\",\"xlarge\":\"259x194\",\"large\":\"259x194\",\"medium\":\"259x194\",\"small\":\"259x194\",\"tiny\":\"200x150\"}', '', '', 'thumbnail', '2021-07-20 17:03:14', '2', NULL, NULL, 0, 35, 'published'),
(42, 'packages', 3, 'bigplaya-azeda-buzios-04.jpg', 'big_playa-azeda-buzios-04.jpg', 'jpg', 'images', '{\"src\":\"840x460\",\"xlarge\":\"840x460\",\"large\":\"840x460\",\"medium\":\"840x460\",\"small\":\"640x350\",\"tiny\":\"200x110\"}', '', '', 'gallery', '2021-07-20 17:03:27', '2', NULL, NULL, 0, 36, 'published'),
(43, 'packages', 3, 'bigplaya-joao-fernandes-buzios-03.jpg', 'big_playa-joao-fernandes-buzios-03.jpg', 'jpg', 'images', '{\"src\":\"840x460\",\"xlarge\":\"840x460\",\"large\":\"840x460\",\"medium\":\"840x460\",\"small\":\"640x350\",\"tiny\":\"200x110\"}', '', '', 'gallery', '2021-07-20 17:03:30', '2', NULL, NULL, 0, 37, 'published'),
(44, 'packages', 3, 'buzios-rua-das-pedras0.jpg', 'buzios-rua-das-pedras_0.jpg', 'jpg', 'images', '{\"src\":\"750x390\",\"xlarge\":\"750x390\",\"large\":\"750x390\",\"medium\":\"750x390\",\"small\":\"640x333\",\"tiny\":\"200x104\"}', '', '', 'gallery', '2021-07-20 17:03:32', '2', NULL, NULL, 0, 38, 'published'),
(45, 'packages', 3, 'la-tranquilidad-de-buzios-760x500.jpg', 'La-tranquilidad-de-Buzios-760x500.jpg', 'jpg', 'images', '{\"src\":\"760x500\",\"xlarge\":\"760x500\",\"large\":\"760x500\",\"medium\":\"760x500\",\"small\":\"640x421\",\"tiny\":\"200x132\"}', '', '', 'gallery', '2021-07-20 17:03:34', '2', NULL, NULL, 0, 39, 'published'),
(46, 'packages', 5, 'bigplaya-azeda-buzios-04_0.jpg', 'big_playa-azeda-buzios-04.jpg', 'jpg', 'images', '{\"src\":\"840x460\",\"xlarge\":\"840x460\",\"large\":\"840x460\",\"medium\":\"840x460\",\"small\":\"640x350\",\"tiny\":\"200x110\"}', '', '', 'thumbnail', '2021-07-20 18:07:38', '2', NULL, NULL, 0, 40, 'published'),
(47, 'packages', 5, 'bigplaya-azeda-buzios-04_1.jpg', 'big_playa-azeda-buzios-04.jpg', 'jpg', 'images', '{\"src\":\"840x460\",\"xlarge\":\"840x460\",\"large\":\"840x460\",\"medium\":\"840x460\",\"small\":\"640x350\",\"tiny\":\"200x110\"}', '', '', 'gallery', '2021-07-20 18:07:43', '2', NULL, NULL, 0, 41, 'published'),
(48, 'packages', 5, 'bigplaya-joao-fernandes-buzios-03_0.jpg', 'big_playa-joao-fernandes-buzios-03.jpg', 'jpg', 'images', '{\"src\":\"840x460\",\"xlarge\":\"840x460\",\"large\":\"840x460\",\"medium\":\"840x460\",\"small\":\"640x350\",\"tiny\":\"200x110\"}', '', '', 'gallery', '2021-07-20 18:07:46', '2', NULL, NULL, 0, 42, 'published'),
(49, 'packages', 5, 'buzios-rua-das-pedras0_0.jpg', 'buzios-rua-das-pedras_0.jpg', 'jpg', 'images', '{\"src\":\"750x390\",\"xlarge\":\"750x390\",\"large\":\"750x390\",\"medium\":\"750x390\",\"small\":\"640x333\",\"tiny\":\"200x104\"}', '', '', 'gallery', '2021-07-20 18:07:50', '2', NULL, NULL, 0, 43, 'published'),
(50, 'packages', 5, 'images_0.jpg', 'images.jpg', 'jpg', 'images', '{\"src\":\"259x194\",\"xlarge\":\"259x194\",\"large\":\"259x194\",\"medium\":\"259x194\",\"small\":\"259x194\",\"tiny\":\"200x150\"}', '', '', 'gallery', '2021-07-20 18:07:50', '2', NULL, NULL, 0, 44, 'published'),
(51, 'packages', 5, 'la-tranquilidad-de-buzios-760x500_0.jpg', 'La-tranquilidad-de-Buzios-760x500.jpg', 'jpg', 'images', '{\"src\":\"760x500\",\"xlarge\":\"760x500\",\"large\":\"760x500\",\"medium\":\"760x500\",\"small\":\"640x421\",\"tiny\":\"200x132\"}', '', '', 'gallery', '2021-07-20 18:07:54', '2', NULL, NULL, 0, 45, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

CREATE TABLE `packages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `price` varchar(200) DEFAULT NULL,
  `legend` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `legals` text DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packages`
--

INSERT INTO `packages` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `price`, `legend`, `text`, `legals`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'NOA', 'noa', 'Acá puede ir un subtítulo libre', 'desde $ 25.000', '2 noches c/desayuno', '<h4>Incluye:</h4>\r\n<ul>\r\n<li>Bus ida y regreso</li>\r\n<li>Paseo por el barrio de Barracas, conociendo su historia, &eacute;poca de esplendor, iglesias, misterios y leyendas</li>\r\n<li>Merienda en BAHAMAS CLUB en COSTANERA</li>\r\n<li>Coordinaci&oacute;n</li>\r\n</ul>\r\n<h4>Itinerario</h4>\r\n<p>Parque Lezama, F&aacute;brica Canale, F&aacute;brica Alpargatas, Iglesia de Santa Felicitas, Casa de los leones, Ex Hospital Casa Cuna, Iglesia del Inmaculado Coraz&oacute;n de Mar&iacute;a, F&aacute;brica Aguila Saint, Estaci&oacute;n Yrigoyen, Bas&iacute;lica del Sagrado Coraz&oacute;n de Jes&uacute;s, Hospital Borda y Moyano, Pasaje Lan&iacute;n, Hospital Malbr&aacute;n, Ribera, Viejo Puente Puerred&oacute;n, Barracas</p>\r\n<h4>Men&uacute;</h4>\r\n<ul>\r\n<li>Una infusion: caf&eacute; americano, caf&eacute; con leche, t&eacute; con leche o &nbsp;capuchino</li>\r\n<li>Una medialuna</li>\r\n<li>Un sandwich de miga mixto</li>\r\n<li>Tostadas completas con manteca, mermelada, queso crema</li>\r\n<li>Crocantes de manzana &oacute; brownies</li>\r\n</ul>\r\n<p>No se suspende por lluvia.</p>\r\n<p><strong>General $ 2.200</strong></p>\r\n<p>Puntos de ascenso: Villa Ballester, San Mart&iacute;n, Av San Mart&iacute;n y Gral Paz.</p>\r\n<p>-------<br /><em>*** Fecha de salida sujeta a la finalizaci&oacute;n del aislamiento social, preventivo y obligatorio ***</em></p>', '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>', NULL, NULL, NULL, NULL, '2021-07-13 14:48:56', '1', '2021-07-20 15:13:08', '2', 1, 2, 'published'),
(2, NULL, 'Puerto Madryn', 'puerto-madryn', 'Acá puede ir un subtítulo libre', 'desde $ 25.000', '2 noches c/desayuno', '<h4>Incluye:</h4>\r\n<ul>\r\n<li>Bus ida y regreso</li>\r\n<li>Paseo por el barrio de Barracas, conociendo su historia, &eacute;poca de esplendor, iglesias, misterios y leyendas</li>\r\n<li>Merienda en BAHAMAS CLUB en COSTANERA</li>\r\n<li>Coordinaci&oacute;n</li>\r\n</ul>\r\n<h4>Itinerario</h4>\r\n<p>Parque Lezama, F&aacute;brica Canale, F&aacute;brica Alpargatas, Iglesia de Santa Felicitas, Casa de los leones, Ex Hospital Casa Cuna, Iglesia del Inmaculado Coraz&oacute;n de Mar&iacute;a, F&aacute;brica Aguila Saint, Estaci&oacute;n Yrigoyen, Bas&iacute;lica del Sagrado Coraz&oacute;n de Jes&uacute;s, Hospital Borda y Moyano, Pasaje Lan&iacute;n, Hospital Malbr&aacute;n, Ribera, Viejo Puente Puerred&oacute;n, Barracas</p>\r\n<h4>Men&uacute;</h4>\r\n<ul>\r\n<li>Una infusion: caf&eacute; americano, caf&eacute; con leche, t&eacute; con leche o &nbsp;capuchino</li>\r\n<li>Una medialuna</li>\r\n<li>Un sandwich de miga mixto</li>\r\n<li>Tostadas completas con manteca, mermelada, queso crema</li>\r\n<li>Crocantes de manzana &oacute; brownies</li>\r\n</ul>\r\n<p>No se suspende por lluvia.</p>\r\n<p><strong>General $ 2.200</strong></p>\r\n<p>Puntos de ascenso: Villa Ballester, San Mart&iacute;n, Av San Mart&iacute;n y Gral Paz.</p>\r\n<p>-------<br /><em>*** Fecha de salida sujeta a la finalizaci&oacute;n del aislamiento social, preventivo y obligatorio ***</em></p>', '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>', NULL, NULL, NULL, NULL, '2021-07-13 14:54:03', '1', '2021-07-20 21:15:09', '2', 1, 3, 'published'),
(3, NULL, 'Buzios', 'buzios', 'Verano 2022', 'Desde USD 799', 'Verano 2022', '<p>&nbsp;</p>\r\n<p>Incluye:</p>\r\n<ul>\r\n<li>Aereo directo de GOL a RIO con equipaje despachado</li>\r\n<li>Traslados de llegada y salida</li>\r\n<li>7 noches de alojamiento en <strong>Pousada Aguazul</strong> en hab std con desayuno</li>\r\n<li>Todos los impuestos</li>\r\n</ul>\r\n<p>Valor final por persona en base doble: usd 799.-</p>\r\n<p>&nbsp;</p>', '', NULL, NULL, NULL, NULL, '2021-07-16 21:40:53', '1', '2021-07-20 21:15:08', '2', 0, 1, 'published'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 18:03:42', '2', NULL, NULL, 0, 4, 'temp'),
(5, NULL, 'Iguazu', 'iguazu', 'Vacaciones de invierno', 'Desde $ 8000', 'Super promo', '<table border=\"0\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\">\r\n<tbody>\r\n<tr>\r\n<td class=\"textoInterfaz\" valign=\"top\">Incluye:&nbsp;&nbsp;</td>\r\n<td class=\"textoInterfaz\" valign=\"top\" width=\"100%\">&middot;Pasaje a&eacute;reo de&nbsp;AEROLINEAS ARGENTINAS AEP/IGR/AEP&nbsp;en clase&nbsp;V&nbsp;con equipaje de bodega incluido (1 maleta hasta 15kg) y venta anticipada de 2 d&iacute;as\r\n<p>&middot;*** SALIDAS: Consultar d&iacute;as de salida seg&uacute;n programaci&oacute;n de Aerol&iacute;neas Argentinas***</p>\r\n<p>&middot;Traslados en servicio privado, de acuerdo con los horarios de check in/out</p>\r\n<p>&middot;3 noches de alojamiento en&nbsp;AWASI IGUAZU, exclusivas&nbsp;villas privadas, con r&eacute;gimen de \"todo incluido\" (*)</p>\r\n<p>&middot;Excursiones&nbsp;privadas: veh&iacute;culo 4x4 y un gu&iacute;a privado asignado para dise&ntilde;ar las excursiones seg&uacute;n gustos y preferencias del pasajero. Podr&aacute;&nbsp;elegir entre excursiones de d&iacute;a completo con almuerzo incluido (entre 4 y 7 horas aprox.) o bien dos excursiones de medio d&iacute;a (entre 2 y 5 horas aprox.), regresando a almorzar a Awasi entre ambas excursiones</p>\r\n<p>&middot;Asistencia al viajero&nbsp;UNIVERSAL ASSISTANCE NACIONAL 100K&nbsp;con cobertura de $100.000 para pasajeros con edad de hasta 70 a&ntilde;os cumplidos. A partir de los 71 a&ntilde;os se deber&aacute;&nbsp;pagar un suplemento. VER CONDICIONES GENERALES EN EL BOTON DE ASISTENCIA AL VIAJERO</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>(*)&nbsp;COMIDAS: Como parte de Relais &amp; Chateaux, destacan los sabores e ingredientes locales. Trabajan con productores de la zona para crear un men&uacute;&nbsp;con recetas de temporada y respete el patrimonio culinario de la regi&oacute;n.&nbsp;BEBIDAS: Con y sin alcohol, incluyen una amplia selecci&oacute;n de vinos argentinos. Bar abierto incluido</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>Check in 3:00 PM.</p>\r\n<p>Check out 12:00 PM.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>** El hotel s&oacute;lo acepta ni&ntilde;os a partir de los 10 a&ntilde;os **</p>\r\n<p>&nbsp;</p>\r\n<p>** CONSULTAR CONDICIONES ESPECIALES DE VENTA **</p>\r\n<p>&nbsp;</p>\r\n<p>************************************************************************************************</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>TARIFA AEREA: EMISION A LAS 24 HORAS DE EFECTUADA LA RESERVA.</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>SUPLEMENTOS AEREOS</p>\r\n<p>&nbsp;</p>\r\n<p>Clase N: $ 1905</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>**NO VALIDO PARA FINES DE SEMANA LARGO, FERIADOS, FERIAS Y CONVENCIONES**</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<p>****************************************&nbsp;OBSERVACIONES&nbsp;****************************************</p>\r\n<p>No incluye:&nbsp;Entrada al Parque Nacional ni Tasa de Turismo Municipal</p>\r\n<p>&nbsp;</p>\r\n<p>ENTRADA AL PARQUE NACIONAL: se compra de manera online a trav&eacute;s de la siguiente web:&nbsp;https://www.argentina.gob.ar/parquesnacionales</p>\r\n<p>&nbsp;</p>\r\n<p>TASA DE TURISMO MUNICIPAL DE IGUAZU:&nbsp;Les informamos que est&aacute;&nbsp;en vigencia la Ordenanza Municipal N&ordm;&nbsp;54/15, Resoluci&oacute;n N&ordm;&nbsp;66, donde toda persona que ingresa&nbsp;a la Ciudad de Puerto Iguaz&uacute;&nbsp;debe abonar la nueva Tasa de Turismo, la cual es de $ 180 por persona, por estad&iacute;a, a partir de los 12 a&ntilde;os, ya sea para turistas nacionales como extranjeros.</p>\r\n<p>El pago ser&aacute;&nbsp;requerido en destino, sobre la ruta de acceso a Puerto Iguaz&uacute;&nbsp;o en el hotel.</p>\r\n<p>&nbsp;</p>\r\n<p>Tarifas exclusivas para pasajeros Argentinos o Residentes.</p>\r\n<p>&nbsp;</p>\r\n<p>Tarifas sujetas a cambio por parte de la compa&ntilde;&iacute;a a&eacute;rea sin previo aviso. No tienen devoluci&oacute;n bajo ninguna circunstancia.</p>\r\n<p>&nbsp;</p>\r\n<p>**********************************************************************************************</p>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>', '', NULL, NULL, NULL, NULL, '2021-07-20 18:03:51', '2', '2021-07-20 21:15:06', '2', 1, 5, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_blocks`
--

CREATE TABLE `page_blocks` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `page` varchar(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `text_alt` text DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'published'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `page_blocks`
--

INSERT INTO `page_blocks` (`id`, `parent_id`, `page`, `title`, `subtitle`, `text`, `text_alt`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, NULL, 'ACERCA DE NOSOTROS', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, '2021-07-19 19:26:42', '1', '2021-07-19 19:27:05', '1', 0, 1, 'published'),
(2, NULL, NULL, 'TÉRMINOS Y CONDICIONES', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, '2021-07-19 19:27:10', '1', '2021-07-19 19:27:27', '1', 0, 2, 'draft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `isonum` smallint(6) DEFAULT NULL,
  `iso2` char(2) DEFAULT NULL,
  `iso3` char(3) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` int(11) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT 0,
  `status` char(10) NOT NULL DEFAULT 'published',
  `pos` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `isonum`, `iso2`, `iso3`, `title`, `title_en`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `status`, `pos`) VALUES
(4, 276, 'DE', 'DEU', 'Alemania', 'Germany', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(13, 32, 'AR', 'ARG', 'Argentina', 'Argentina', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(19, 44, 'BS', 'BHS', 'Bahamas', 'Bahamas', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(29, 68, 'BO', 'BOL', 'Bolivia', 'Bolivia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(33, 76, 'BR', 'BRA', 'Brasil', 'Brazil', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(35, 100, 'BG', 'BGR', 'Bulgaria', 'Bulgary', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(46, 152, 'CL', 'CHL', 'Chile', 'Chile', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(52, 170, 'CO', 'COL', 'Colombia', 'Colombia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(60, 188, 'CR', 'CRI', 'Costa Rica', 'Costa Rica', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(62, 192, 'CU', 'CUB', 'Cuba', 'Cuba', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(66, 218, 'EC', 'ECU', 'Ecuador', 'Ecuador', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(68, 222, 'SV', 'SLV', 'El Salvador', 'El Salvador', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(73, 724, 'ES', 'ESP', 'España', 'Spain', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(75, 840, 'US', 'USA', 'Estados Unidos', 'United States', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(82, 250, 'FR', 'FRA', 'Francia', 'France', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(90, 300, 'GR', 'GRC', 'Grecia', 'Greece', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(94, 320, 'GT', 'GTM', 'Guatemala', 'Guatemala', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(102, 340, 'HN', 'HND', 'Honduras', 'Honduras', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(109, 372, 'IE', 'IRL', 'Irlanda', 'Ireland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(112, 380, 'IT', 'ITA', 'Italia', 'Italy', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(146, 484, 'MX', 'MEX', 'México', 'Mexico', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(149, 492, 'MC', 'MCO', 'Mónaco', 'Monaco', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(157, 558, 'NI', 'NIC', 'Nicaragua', 'Nicaragua', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(162, 578, 'NO', 'NOR', 'Noruega', 'Norway', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(164, 554, 'NZ', 'NZL', 'Nueva Zelanda', 'New Zeland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(170, 591, 'PA', 'PAN', 'Panamá', 'Panama', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(172, 600, 'PY', 'PRY', 'Paraguay', 'Paraguay', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(173, 604, 'PE', 'PER', 'Perú', 'Peru', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(177, 620, 'PT', 'PRT', 'Portugal', 'Portugal', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(178, 630, 'PR', 'PRI', 'Puerto Rico', 'Puerto Rico', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(180, 826, 'GB', 'GBR', 'Reino Unido', 'United Kingdom', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(184, 643, 'RU', 'RUS', 'Rusia', 'Russia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(197, 891, 'CS', 'SCG', 'Serbia y Montenegro', 'Serbia and Montenegro', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(207, 752, 'SE', 'SWE', 'Suecia', 'Sweeden', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(208, 756, 'CH', 'CHE', 'Suiza', 'Switzerland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(229, 858, 'UY', 'URY', 'Uruguay', 'Uruguay', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(232, 862, 'VE', 'VEN', 'Venezuela', 'Venezuela', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(233, 376, 'IL', 'ISR', 'Israel', 'Israel', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(234, 214, 'DO', 'DOM', 'República Dominicana', 'Dominican Republic', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(235, 710, 'ZA', 'ZAF', 'Sudáfrica', 'South Africa', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `art_ref` char(50) DEFAULT NULL COMMENT 'Tabla relacionada 1',
  `art_id` int(11) NOT NULL,
  `tag_ref` char(50) DEFAULT NULL COMMENT 'Tabla relacionada 2',
  `tag_id` int(11) NOT NULL,
  `rel_ref` varchar(100) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `relations`
--

INSERT INTO `relations` (`id`, `art_ref`, `art_id`, `tag_ref`, `tag_id`, `rel_ref`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, 'packages', 1, 'destinations', 1, NULL, '2021-07-13 14:49:36', '1', NULL, NULL, 0, 1, 'draft'),
(2, 'packages', 2, 'destinations', 1, NULL, '2021-07-13 14:54:03', '', NULL, NULL, 0, 3, 'draft'),
(3, 'packages', 3, 'destinations', 3, NULL, '2021-07-16 21:41:27', '1', NULL, NULL, 0, 4, 'draft'),
(4, 'packages', 5, 'destinations', 1, NULL, '2021-07-20 18:05:42', '2', NULL, NULL, 0, 5, 'draft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `subtitle_2` varchar(100) DEFAULT NULL,
  `cta_text` varchar(50) DEFAULT NULL,
  `cta_link` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `vimeo_url` varchar(100) DEFAULT NULL,
  `youtube_url` varchar(100) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `subtitle_2`, `cta_text`, `cta_link`, `text`, `vimeo_url`, `youtube_url`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, '3 noches en Bariloche<br>con desayuno y excursiones ', '3-noches-en-calafate-con-desayuno-y-excursiones-', 'Saludas grupales', 'Desde $ 25.000', 'Ver más', '/paquetes/iguazu', '', NULL, NULL, NULL, NULL, '2021-07-13 19:16:21', '1', '2021-07-20 19:56:15', '2', 0, 1, 'published'),
(2, NULL, 'Un título más corto', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 23:00:15', '1', '2021-07-20 21:17:56', '2', 0, 2, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `static_pages`
--

CREATE TABLE `static_pages` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `static_pages`
--

INSERT INTO `static_pages` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Acerca de nosotros', 'acerca-de-nosotros', NULL, '<p>No solo somos una agencia de viajes habilitada por el Ministerio de Turismo de la Nacion hace ya casi 9 a&ntilde;os, si, 9 a&ntilde;os de haber iniciado este sue&ntilde;o de ser independiente y tener la libertad que el viajero necesita, porque de eso se trata viajar, de sentirse libre y asi nos sentimos desde el 1&deg; de Noviembre del 2012 y asi seguiremos manteniendo nuestro deseo.</p>\r\n<p>Transitamos tormentas, tempestades fuertes y un monton de cosas mas pero aqui estamos y seguiremos estando, porque amamos lo que hacemos, porque no hay nada mas lindo que ver a la gente ser feliz con uno mismo y con sus seres queridos, viajando, nutriendose, conociendose, enamorandose y eso es lo que nos motiva a seguir adelante.</p>\r\n<p>Muchos ya nos conocen y otros estan por conocernos, a todos ustedes MUCHAS GRACIAS y A VIAJAR!!!</p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '2021-07-19 19:31:55', '1', '2021-07-20 12:48:27', '2', 0, 1, 'published'),
(2, NULL, 'Términos y condiciones', 'terminos-y-condiciones', NULL, '<p><strong>CONDICIONES GENERALES</strong></p>\r\n<p>Les pedimos leer atentamente y ante cualquier inquietud que pudiera surgir, nos consulte. Estamos a su disposici&oacute;n para asesorarle y brindarle la informaci&oacute;n que sea necesaria.</p>\r\n<p><strong>INFORMES DE SERVICIOS Y TARIFAS</strong></p>\r\n<p>En todos los casos son orientativos y no revisten confirmaci&oacute;n. Las solicitudes de servicios se regir&aacute;n por las condiciones que a continuaci&oacute;n se detallan:</p>\r\n<p><strong>SOLICITUDES Y PAGOS</strong></p>\r\n<p>1) Los pagos antes de la confirmaci&oacute;n definitiva de los servicios por la Agencia son en concepto de reserva a cuenta de mayor cantidad. La confirmaci&oacute;n definitiva de los servicios y precios finales respectivos se producir&aacute; con la cancelaci&oacute;n total del precio convenido, la emisi&oacute;n de pasajes y/u &oacute;rdenes de servicios (vouchers) y la emisi&oacute;n de la facturaci&oacute;n correspondiente. 2) El precio y/o reservaci&oacute;n de los servicios que componen el tour o los servicios individuales requeridos quedan sujetos a modificaciones sin previo aviso cuando se produzca una alteraci&oacute;n en los servicios, modificaciones en los costos o en los tipos de cambio previstos, por causas no imputables a las partes. 3) Las operaciones a cr&eacute;dito deber&aacute;n satisfacer los requisitos propios fijados para ellas. El interesado deber&aacute; cumplimentar el pago del precio y de los saldos en los plazos y condiciones establecidos en la contrataci&oacute;n con las entidades bancarias que las financien. 4) Al momento de cotizar, el sitio y/o el ejecutivo de ventas mostrar&aacute;, desplegar&aacute; y/u ofrecer&aacute; la tarifa m&aacute;s econ&oacute;mica disponible al momento de la cotizaci&oacute;n encontrada en el GDS (Sistema global de distribuci&oacute;n de reservas a&eacute;reas y/o de servicios terrestres) en el momento de la consulta: esas tarifas, en general, no permiten cambios ni reembolsos y, a criterio exclusivo del prestador, podr&iacute;an admitir cambios con penalidades y/o diferencias tarifarias o reembolsos con penalidades que no dependen de la voluntad de esta compa&ntilde;&iacute;a. A partir de 2020, en virtud de haberse creado tarifas promocionales denominadas &ldquo;superflex&rdquo; o similar, el pasajero reconoce que adquiere dicha tarifa promocional con las condiciones y limitaciones que dispone el transportador u operador o el prestador de servicios. 5) Las cotizaciones de los servicios ser&aacute;n en moneda extranjera cuando los mismos se presten en el exterior (cfr. Res. 7/2002), igual que los vuelos por block off o cupo y/o charters. Los pagos se recibir&aacute;n en pesos al tipo de cambio vigente en el horario del d&iacute;a de pago, y se deducir&aacute;n como pago a cuenta del total cotizado en la moneda extranjera. El saldo, se abonar&aacute; en pesos a la cotizaci&oacute;n vigente en el horario del d&iacute;a de su efectiva acreditaci&oacute;n en las cuentas informadas por esta compa&ntilde;&iacute;a. Para el caso que el pasajero opte por pagar en moneda extranjera, ante la eventualidad de desistimiento o cancelaci&oacute;n de los servicios, los reembolsos ser&aacute;n en la moneda de curso legal (cfr. Art. 765 CCyCN). 6) Los pasajeros que paguen en dinero en efectivo, quedar&aacute;n sometidos a las disposiciones de la Res. 3825/2015 AFIP y a las que en el futuro puedan dictarse en ese sentido. Los que abonen servicios que se presten en el exterior, toman conocimiento de las restricciones cambiarias por medidas gubernamentales ajenas a esta compa&ntilde;&iacute;a. Se deber&aacute; atender a las normas previstas en la Res. Gral AFIP 4815, en materia de percepciones impositivas.</p>\r\n<p><strong>LOS PRECIOS INCLUYEN</strong></p>\r\n<p>Los servicios que se especifican en el itinerario correspondiente a lo contratado. Transporte de ida y vuelta, cuando este servicio est&eacute; expresamente incluido en el detalle de servicios, con el tipo, caracter&iacute;sticas y categor&iacute;a que conste en dicho detalle, de acuerdo a si el servicio es regular, ch&aacute;rter o con sistema de block off (bloqueo) y de acuerdo a los destino con o sin escalas y/o conexiones; asistencia al viajero conforme las condiciones del servicio en caso de corresponder y/o su modalidad est&eacute; expresamente indicada en el voucher respectivo; alojamiento en los hoteles mencionados en los itinerarios u otros de igual o mayor categor&iacute;a &mdash;en caso de cambio&mdash;, en habitaciones simples, dobles o triples, seg&uacute;n la cantidad de pasajeros alojados, con ba&ntilde;o privado e impuestos incluidos, salvo estipulaci&oacute;n expresa en contrario y/o salvo en ciudades y/o pa&iacute;ses que cobren tasas de pernocte directamente al pasajero; r&eacute;gimen de comidas, seg&uacute;n se indique en cada oportunidad; visitas y excursiones que se mencionen. Traslados hasta y desde aeropuertos, terminales y/u hoteles, cuando se indique. La cantidad de d&iacute;as de alojamiento prevista en el voucher de servicios, considerando que el d&iacute;a de alojamiento hotelero se computa desde las quince horas y finaliza a las doce horas del d&iacute;a siguiente, independientemente de la hora de llegada y de salida y/o de la utilizaci&oacute;n completa o fraccionada del servicio de hoteler&iacute;a. Atento que la legislaci&oacute;n hotelera es de &iacute;ndole local, si por motivos inherentes a ella el horario de finalizaci&oacute;n del alojamiento fuere anterior a las doce horas, las habitaciones podr&aacute;n ser ocupadas hasta la hora de salida que indique el hotel al hacer el ingreso, y esta compa&ntilde;&iacute;a no podr&aacute; efectuar modificaci&oacute;n alguna sobre dicha circunstancia, ni atender situaciones especiales. Pasado el l&iacute;mite horario, el pasajero deber&aacute; abonar al hotel la tarifa correspondiente, conforme las que se encuentren vigentes y/o rack rate o tarifa de mostrador que podr&aacute; variar respecto de la abonada a la agencia. La duraci&oacute;n del tour ser&aacute; indicada en cada caso tomando como primer d&iacute;a el establecido en la documentaci&oacute;n de viaje, y como &uacute;ltimo, el d&iacute;a de salida del destino, independientemente de los horarios de salida o de llegada, de origen y a destino, respectivamente.&nbsp;<u>Servicios de hoteler&iacute;a:</u>&nbsp;en general, los establecimientos hoteleros poseen escasa cantidad de habitaciones con disponibilidad triple; por lo que es corriente que ocurra que las habitaciones triples se conformen con una habitaci&oacute;n doble a las que se le agrega una cama adicional de tipo desarmable, lo que podr&iacute;a limitar la comodidad funcional de la misma, dicha limitaci&oacute;n es aceptada por el pasajero, liberando al hotel, a esta empresa y/u al operador mayorista de cualquier responsabilidad al respecto.&nbsp;<u>Servicios en Estados Unidos de Am&eacute;rica y M&eacute;xico:</u>&nbsp;las habitaciones disponen de dos camas matrimoniales tipo Queen, en las que se permite acomodar de una a cuatro personas a la misma tarifa; por lo que cuando se indica DBLFP, tanto en M&eacute;xico como en Estados Unidos de Am&eacute;rica, se entiende que son habitaciones que albergan hasta a 2 adultos y hasta 2 menores de hasta 12 a&ntilde;os, en una misma habitaci&oacute;n con dos camas de 2 plazas tipo Queen, todo ello en tanto las medidas sanitarias as&iacute; lo autoricen y hasta nuevo aviso. La categor&iacute;a de los hoteles que se incluyan en los itinerarios son las oficiales otorgadas por las autoridades tur&iacute;sticas del sitio geogr&aacute;fico en el que se encuentran y su otorgamiento y control es administrativo. La Empresa no asume responsabilidad alguna sobre los criterios que rijan ese control y otorgamiento.</p>\r\n<p><strong>SERVICIOS O RUBROS NO INCLUIDOS</strong></p>\r\n<p>1) Extras, bebidas, comidas, lavado y planchado de ropa, propinas, gastos de &iacute;ndole personal, exceso de equipaje, llamadas telef&oacute;nicas, tasas de embarque, penalidad por falta de realizaci&oacute;n de check-in on line, peajes, cuando no est&eacute; especificado lo contrario, tasas sobre servicios, IVA y/u otros impuestos, aranceles aduaneros, migratorios o por gesti&oacute;n de reservas, costo y/o gastos por vacunas, test y/o an&aacute;lisis bioqu&iacute;micos y/o todo gasto derivados de los requisitos sanitarios exigidos para el ingreso al destino o el regreso a Argentina, retenciones o percepciones, actuales y/o futuros, costos y/o gastos derivados de la obtenci&oacute;n urgente o express de documentaci&oacute;n para viajar, ni ning&uacute;n otro servicio que no se encuentre expresamente indicado en la orden de servicio emitida por esta compa&ntilde;&iacute;a; 2) Entradas a museos, sitios arqueol&oacute;gicos, atracciones, parques nacionales, excursiones opcionales, impuestos y/o tasas locales de turismo, comunicaciones, gastos adicionales producidos por cancelaciones, demoras en las salidas o llegadas de los medios de transporte, o por razones imprevistas ajenas a esta compa&ntilde;&iacute;a; 3) Alimentaci&oacute;n en ruta, excepto aquellas que estuviesen expresamente incluidas en los programas. 4) Los gastos e intereses en las operaciones a cr&eacute;dito. 5) Costos por visados y/o autorizaciones de ingreso al destino elegido cuando as&iacute; se lo requiera. 6) Los gastos por prolongaci&oacute;n de servicios o estad&iacute;as por deseo voluntario de los pasajeros, o por caso fortuito, fuerza mayor o situaciones fuera del control razonable del organizador y en general de ning&uacute;n concepto que no se encuentre espec&iacute;ficamente detallado en el itinerario correspondiente, tampoco situaciones de encontrarse varado por causas sanitarias dispuesto por autoridades gubernamentales; 7) En caso de alquiler de rodados, no est&aacute;n incluidos los gastos de combustible, los peajes, GPS, impuestos y seguros optativos u obligatorios, salvo indicaci&oacute;n expresa y descripci&oacute;n de los mismos en sentido contrario. 8) No est&aacute;n incluidos los cargos por elecci&oacute;n de asientos en aviones. La agencia se limitar&aacute; a solicitar a la aerol&iacute;nea la ubicaci&oacute;n preferida por el pasajero -que puede o no tener un costo adicional-, pero no garantiza que la aerol&iacute;nea adjudique los asientos solicitados; 9) Servicio de asistencia al viajero. Es indispensable que los pasajeros contraten un servicio de asistencia al viajero de acuerdo con el tipo y caracter&iacute;sticas del viaje, destino, coberturas exigidas (cfr. Tratado Schengen, en caso de viajeros a Europa zona Euro) u otras zonas y/o pa&iacute;ses que as&iacute; lo requieran; puntualmente que ampare situaciones pand&eacute;micas de asistencia y de cancelaci&oacute;n intempestiva de viaje por cierre de fronteras. Juli&aacute; Tours no responder&aacute; por situaciones que puedan estar amparadas por servicios de asistencia al viajero. El pasajero deber&aacute; elegir entre las coberturas que mejor le amparen su situaci&oacute;n etaria y sanitaria.</p>\r\n<p><strong>LIMITACIONES AL DERECHO DE PERMANENCIA</strong></p>\r\n<p>Esta compa&ntilde;&iacute;a -o los operadores locales- tendr&aacute;n el derecho de hacer que abandone el tour y/o los servicios tur&iacute;sticos en cualquier punto del itinerario todo pasajero cuya conducta disruptiva, modo de obrar, estado de salud y/u otras razones graves a juicio de esta compa&ntilde;&iacute;a -o de los prestadores locales en cada destino- provocaren peligro y/o causaren molestias a los restantes viajeros y/o que pudiere malograr el &eacute;xito de la excursi&oacute;n y/o su normal desarrollo. En esos casos, se aplicar&aacute;n las penalidades establecidas en el cap&iacute;tulo &ldquo;Alternaciones o Modificaciones&rdquo;. En todos los casos, es condici&oacute;n esencial que los pasajeros obren de buena fe, respeto por las personas que componen el grupo, sus bienes, las instalaciones de los establecimientos hoteleros, medios de transporte, lugares de visita o excursiones y a los gu&iacute;as, todo de acuerdo con est&aacute;ndares de conducta que permitan y faciliten la convivencia grupal en todo momento. Asimismo, el pasajero se obliga a adoptar todas las medidas de seguridad sanitaria que dispongan las autoridades del lugar de destino, los establecimientos hoteleros, las aerol&iacute;neas, los transportistas, los restaurantes, etc. El pasajero se obliga y toma a su cargo hacer los tests sanitarios que exijan autoridades o empresas vinculadas con el viaje. Siempre deber&aacute; consultar si el prestador o las autoridades tienen normas sanitarias exigidas como condici&oacute;n de ingreso, de acuerdo con su nacionalidad o condici&oacute;n etaria.</p>\r\n<p><strong>DOCUMENTACI&Oacute;N</strong></p>\r\n<p>Para los viajes al exterior el pasajero debe atender especialmente la legislaci&oacute;n vigente en cada caso respecto del destino elegido, siendo responsabilidad exclusiva del pasajero contar con la documentaci&oacute;n personal que exijan las autoridades en cada caso y destino. Es obligaci&oacute;n inexcusable del pasajero obtener y presentar la documentaci&oacute;n en los momentos en que le sea requerida por la autoridad migratoria, policial y/o sanitaria y/o quien correspondiera de acuerdo con el tipo de viaje elegido. El pasajero se obliga, a su cargo, a tramitar el pasaporte sanitario que exigiere el origen y/o destino de viaje.&nbsp; El pasajero es responsable de informarse adecuadamente sobre la documentaci&oacute;n necesaria, visados y vacunas, as&iacute; como los requisitos migratorios de los pa&iacute;ses extranjeros. Por haber sido informado y deber de informarse, no se asume responsabilidad por deficiencias de cualquier naturaleza en la documentaci&oacute;n, tramitaci&oacute;n y/o falta de visados, errores en la emisi&oacute;n de los documentos personales, vigencia de pasaportes y/u otros documentos de viaje, permisos para viajes con menores etc. En caso de documentaci&oacute;n no presentada adecuadamente, visados y/o vacunas que impida al pasajero salir, entrar, permanecer y/o transitar en alg&uacute;n pa&iacute;s, se aplican las condiciones establecidas en el apartado &ldquo;Alternaciones o Modificaciones&rdquo;. Aquellos pasajeros que no cuenten con documentos de viaje en regla podr&aacute;n tramitar su pasaporte y/u otro documento conforme las precisiones que se indican en el sitio&nbsp;<a title=\"V&iacute;nculo a Argentina tramite de pasaporte\" href=\"https://www.argentina.gob.ar/tramitar-el-pasaporte\" target=\"_blank\" rel=\"noopener\">https://www.argentina.gob.ar/tramitar-el-pasaporte</a>. En algunos aeropuertos argentinos se pueden tramitar el pasaporte al instante. Consulte:&nbsp;<a title=\"V&iacute;nculo a sitio de Ministerio del Interior\" href=\"http://www.mininterior.gov.ar/NuevoPasaporte/al-instante/al-instante.php\" target=\"_blank\" rel=\"noopener\">http://www.mininterior.gov.ar/NuevoPasaporte/al-instante/al-instante.php</a>. Consulte en el sitio&nbsp;<a title=\"V&iacute;nculo a sitio de canciller&iacute;a\" href=\"https://www.cancilleria.gob.ar/es/servicios/documentacion/visas-para-argentinos\" target=\"_blank\" rel=\"noopener\">https://www.cancilleria.gob.ar/es/servicios/documentacion/visas-para-argentinos</a>&nbsp;&nbsp; para conocer si Ud. requiere visado de ingreso al pa&iacute;s de destino. En caso de viajar con menores, los pasajeros deber&aacute;n llevar toda la documentaci&oacute;n exigida por la autoridad migratoria. Al respecto, deber&aacute; consultar el sitio web:&nbsp;<a title=\"V&iacute;nculo a sitio de Migraciones\" href=\"http://www.migraciones.gov.ar/accesible/indexA.php?doc_pais\" target=\"_blank\" rel=\"noopener\">http://www.migraciones.gov.ar/accesible/indexA.php?doc_pais</a>&nbsp;(solapa Procedimiento para Ni&ntilde;os, Ni&ntilde;as y Adolescentes). Se sugiere consultar espec&iacute;ficamente los servicios oficiales de informaci&oacute;n sobre medicina del viajero&nbsp;<a title=\"V&iacute;nculo a Ministerio de Salud\" href=\"http://msal.gob.ar/viajeros\" target=\"_blank\" rel=\"noopener\">http://msal.gob.ar/viajeros</a>&nbsp; y de la Canciller&iacute;a Argentina respecto de las medidas y requisitos a tener en cuenta para cada destino. Es responsabilidad del pasajero atender especialmente a las informaciones ofrecidas por el Agente de viajes sobre el destino elegido y sobre los protocolos de seguridad de los establecimientos hoteleros, navieras y compa&ntilde;&iacute;as a&eacute;reas especialmente post-pandemia en tanto las medidas pueden variar sustantivamente de acuerdo con la evoluci&oacute;n de la situaci&oacute;n sanitaria en cada pa&iacute;s. Es responsabilidad del pasajero informar debida y correctamente a la Agencia de viajes la totalidad de sus datos personales y de las personas que viajen con &eacute;l, tanto en cuanto a nombres completos y correctos, nacionalidad, n&uacute;meros y tipo de documento requeridos, y dem&aacute;s datos que se le soliciten de acuerdo al destino; deber&aacute; informar si los pasaportes y/u otra documentaci&oacute;n exigida para viajar est&aacute;n debidamente actualizados y encontrarse en condiciones adecuadas, legibles y en buen estado, adem&aacute;s de los datos de contacto necesarios y solicitados por las aerol&iacute;neas y/o transportadores y/u operadores, por cuestiones de seguridad y conforme las normas internacionales vigentes en materia de PNR. Es obligaci&oacute;n del pasajero informar por escrito a la Agencia de viajes sobre necesidades especiales que requiriera tanto en vuelo como en aeropuertos y excursiones. De acuerdo con el destino de viaje elegido, podr&iacute;an existir sitios, excursiones y/o lugares con dificultad o imposibilidad de acceso para personas con movilidad reducida, raz&oacute;n por la cual podr&iacute;a ocurrir que alguna/s excursi&oacute;n/es no puedan ser brindadas por el operador tur&iacute;stico local por imposibilidad f&aacute;ctica. En tales casos, de conformidad con lo establecido en la ley 25.643, se informar&aacute; sobre cu&aacute;l o cu&aacute;les paseos o excursiones podr&iacute;an verse impedidos de realizaci&oacute;n y/o afectados en su desarrollo. Los servicios no prestados por dicha circunstancia no son reembolsables.</p>\r\n<p>Para viajar al exterior de la Rep&uacute;blica Argentina, los menores de 18 a&ntilde;os de edad deber&aacute;n tener autorizaci&oacute;n expresa, conforme la Ley 26.994 y Disposiciones de la DNM, otorgada por ambos progenitores ante Escribano P&uacute;blico Nacional con la Legalizaci&oacute;n del Colegio P&uacute;blico correspondiente (para C.A.B.A. ver registro electr&oacute;nico de Autorizaciones de menores de edad en&nbsp;<a title=\"V&iacute;nculo a sitio de Argentina\" href=\"https://www.argentina.gob.ar/procedimiento-para-ninos-ninas-y-adolescentes\" target=\"_blank\" rel=\"noopener\">https://www.argentina.gob.ar/procedimiento-para-ninos-ninas-y-adolescentes</a>); C&oacute;nsules argentinos y extranjeros. Debe estar legalizado ante el Ministerio de Relaciones Exteriores y Culto o Apostillado. Consultar Pagina del Ministerio de Relaciones Exteriores y Culto (Autorizaciones de viaje de menores de edad exceptuadas de legalizaci&oacute;n consular. Son las expedidas en Brasil, Chile, Paraguay y Uruguay); Autoridades del Registro de Estado Civil y Capacidad de las Personas, Agentes de esta Direcci&oacute;n Nacional, Jueces de paz; otras Autoridades Administrativas y Judiciales habilitadas a certificar firmas con la acreditaci&oacute;n de la norma que le otorga tal habilitaci&oacute;n, Jueces competentes.</p>\r\n<p>Si el menor viajase con ambos progenitores, adem&aacute;s de los documentos de identidad y visas de todo el grupo familiar, tambi&eacute;n deber&aacute;n exhibir el original de la libreta de matrimonio (original) o la partida de nacimiento legalizada a fin de la justificaci&oacute;n de los v&iacute;nculos filiatorios, testimonio Judicial de adopci&oacute;n u otro instrumento p&uacute;blico que d&eacute; plena fe del v&iacute;nculo que invocan o de la Tutela o Curatela o DNI de formato digital donde figuren los datos filiatorios de ambos padres.</p>\r\n<p>Si el menor viajase con uno solo de los progenitores o tutores, adem&aacute;s de los requisitos antes mencionados, deber&aacute; exhibir, ante la autoridad migratoria nacional, la autorizaci&oacute;n del progenitor que no viaja, otorgada tambi&eacute;n por ante Escribano P&uacute;blico Nacional, con su firma legalizada, y/o en las dependencias y organismos arriba mencionados. Si uno de los progenitores hubiese fallecido, deber&aacute; aportarse, adem&aacute;s, certificado o partida de defunci&oacute;n.</p>\r\n<p>Para m&aacute;s informaci&oacute;n, el pasajero deber&aacute; consultar la siguiente p&aacute;gina web:&nbsp;<a title=\"V&iacute;nculo a sitio web de migraciones Argentina\" href=\"http://www.migraciones.gov.ar/accesible/indexA.php?doc_pais\" target=\"_blank\" rel=\"noopener\">http://www.migraciones.gov.ar/accesible/indexA.php?doc_pais</a>&nbsp;(solapa Procedimiento para Ni&ntilde;os, Ni&ntilde;as y Adolescentes)</p>\r\n<p>Debido a normas internacionales, todos los documentos de viaje deben tener fotograf&iacute;a del menor a que corresponde el documento de identidad, actualizada. En los casos de permisos de viaje para hacerlo a pa&iacute;ses de habla inglesa, el permiso de salida de Argentina debe estar traducido por traductor p&uacute;blico nacional y su firma debidamente legalizada ante el Colegio de Traductores de la localidad de matriculaci&oacute;n del traductor. Los sitios mencionados para obtener informaci&oacute;n en cada caso son solo sugerencias que podr&aacute;n modificarse sin previo aviso. La Agencia y el pasajero son responsables de obtener la informaci&oacute;n en los organismos oficiales pertinentes. A partir del d&iacute;a 7 de septiembre de 2020, la Direcci&oacute;n Nacional de Migraciones ha dispuesto nuevas condiciones para ingresar y egresar del pa&iacute;s, todos los pasajeros y pasajeras deben completar la DDJJ que deben descargar del siguiente enlace:&nbsp;<a title=\"V&iacute;nculo a sitio de Migraciones Argentina\" href=\"http://ddjj.migraciones.gob.ar/app\" target=\"_blank\" rel=\"noopener\">http://ddjj.migraciones.gob.ar/app</a></p>\r\n<p>Esta empresa no ser&aacute; responsable por la tramitaci&oacute;n y vigencia de la documentaci&oacute;n del viajero y sus acompa&ntilde;antes como tampoco respecto de los inconvenientes que por tal causa pudiera sufrir, y ser&aacute; a cargo del pasajero todos los gastos que correspondan por demoras y/o abandono del viaje motivaos por falta o deficiencia de la documentaci&oacute;n necesaria para su realizaci&oacute;n.</p>\r\n<p><strong>CANCELACIONES</strong></p>\r\n<p>1) Cuando se trate de desistimientos que afecten a servicios contratados en firme por la Agencia, el reembolso pedido antes del viaje estar&aacute; sujeto a las condiciones contractuales bajo las cuales presten sus servicios las empresas respectivas, hoteles y/o los operadores en destino. En todos los casos en que hubiera que efectuar reembolsos, la agencia podr&aacute; retener el precio de los gastos incurridos m&aacute;s una comisi&oacute;n del diez por ciento de los servicios contratados con terceros, cfr. Art. 21 Dec. 2182/72. Los plazos para las cancelaciones comenzar&aacute;n a contarse a partir del momento en que esta compa&ntilde;&iacute;a reciba aviso fehaciente de la comunicaci&oacute;n de anulaci&oacute;n del viaje. Las sumas que resulten de la aplicaci&oacute;n de penalidades por anulaciones en cualquier circunstancia o las se&ntilde;as por inscripci&oacute;n al tour o circuito no ser&aacute;n reembolsables, no se compensar&aacute;n ni aplicar&aacute;n&nbsp;a ulteriores contrataciones. Para el caso de venta de pasajes a&eacute;reos, rigen inexorablemente las normas del Contrato de Transporte A&eacute;reo y, especialmente las condiciones establecidas por cada l&iacute;nea a&eacute;rea en la&nbsp;base tarifaria&nbsp;adquirida por el pasajero, teniendo en consideraci&oacute;n que siempre se cotiza sobre la base de la tarifa m&aacute;s baja con restricciones. 2) En caso de desistimientos de operaciones a cr&eacute;dito no tendr&aacute;n reembolso los importes abonados a la agencia de viajes en concepto de informes, gastos administrativos, sellados e intereses, si hubiere. 3) En todos los casos se deber&aacute;n atender especialmente a las condiciones de contrataci&oacute;n del respectivo servicio (v.gr. hoteler&iacute;a, cruceros, centros de ski, seguros, a&eacute;reos nacionales o internacionales, charteados o regulares, etc., as&iacute; como si se tratase de eventos especiales -Reveill&oacute;n, Carnaval, Mardi Grass, Boat Show, Ferias, Congresos y/o cualquier evento de trascendencia, etc.- toda vez que, en cada caso y cada prestador, impone sus determinadas condiciones de contrataci&oacute;n. En &eacute;poca de eventos especiales, el servicio de hoteler&iacute;a que se cancelase no ser&aacute; reembolsable. 4) En el supuesto de vuelos no regulares, ch&aacute;rter o vuelos en los que la Agencia de Viajes tenga BLOCK OFF, se aplicara en cada caso el r&eacute;gimen que establezca el transportador, conforme las normas del contrato de transporte aerocomercial respectivo (Conv. Montreal, Res. 1532/98 M.E.O.y S.P. y Decreto Nacional 1.470/97).</p>\r\n<p>En caso de viaje grupal, los t&eacute;rminos y condiciones ser&aacute;n informados previamente a la agencia y los viajeros respecto de las fechas l&iacute;mite y modalidades de pago que establezca el transportista. En caso de demoras en salidas de vuelos, se aplican las normas espec&iacute;ficas del Contrato de Transporte Aerocomercial. Si el pasajero no se presentase para embarcar y/o tomar los servicios establecidos el d&iacute;a, hora y lugar indicados, el pasajero ser&aacute; considerado como &ldquo;NO SHOW&rdquo; y perder&aacute; el valor total del servicio a&eacute;reo, conforme normativa aerocomercial. Asimismo, si no se presentase a tomar servicios terrestres o de hoteler&iacute;a, cualquiera sea el motivo, se aplicar&aacute;n las condiciones de contrataci&oacute;n respectivas. 5) En los casos de cancelaciones producidas por no llegar a formar el grupo m&iacute;nimo de pasajeros previstos para que la excursi&oacute;n se lleve a cabo, o por cualquier otra causa justificada, los pasajeros inscriptos tendr&aacute;n &uacute;nicamente el derecho a la devoluci&oacute;n de las sumas pagadas hasta el momento de la notificaci&oacute;n. Podr&aacute; anularse una excursi&oacute;n, si se configurare alguna de las circunstancias previstas en el Art. 24 del Decreto 2182/72. 6) Los pasajeros que durante el viaje en forma voluntaria desistieren de utilizar alg&uacute;n/uno de los servicios contratados, no tendr&aacute;n derecho a exigir devoluci&oacute;n de suma alguna, ni compensaci&oacute;n por los servicios desistidos voluntariamente. 7) Las devoluciones o reembolsos en caso de desistimiento de viaje por motivos inherentes al pasajero regir&aacute;n las siguientes penalidades, salvo que el prestador determine otras condiciones respecto del servicio especialmente contratado, lo que ser&aacute; informado al contratar:</p>\r\n<ul>\r\n<li>m&aacute;s de 30 d&iacute;as corridos de la fecha de iniciaci&oacute;n de los servicios: 40% del importe total de la compra.</li>\r\n<li>entre 29 y 15 d&iacute;as corridos de la fecha de iniciaci&oacute;n de los servicios: 50% del importe total de la compra.</li>\r\n<li>entre 14 y 5 d&iacute;as corridos de la fecha de iniciaci&oacute;n de los servicios: 70% del importe total de la compra.</li>\r\n<li>entre 4 d&iacute;as corridos antes, y la fecha de finalizaci&oacute;n de los servicios: 100% del importe total de la compra.</li>\r\n</ul>\r\n<p>Las presentes penalidades rigen sin perjuicio de las que establezca el operador y/o prestador del servicio en sus Condiciones Generales y las que se informen en el caso de servicios con penalidades diferentes a las enunciadas. Las sumas que resulten de la aplicaci&oacute;n de penalidades por anulaciones en cualquier circunstancia o los importes dados en concepto de reserva no ser&aacute;n reembolsables y no se compensar&aacute;n ni aplicar&aacute;n a ulteriores contrataciones. Para el caso de venta de pasajes a&eacute;reos, rigen las normas del Contrato de Transporte a&eacute;reo y, especialmente las condiciones establecidas por las l&iacute;neas a&eacute;reas en la base tarifaria adquirida por el pasajero.&nbsp; Para el caso de viajes en los que se contraten servicios mar&iacute;timos, se aplican adem&aacute;s, las condiciones establecidas por el Agente Armador y/o explotador del buque, en todo lo relativo a las condiciones de viaje y penalidades por cancelaci&oacute;n.</p>\r\n<p><strong>TRANSPORTE NO REGULAR / CHARTERS</strong></p>\r\n<p>Rige lo estipulado en el punto anterior. Sin perjuicio de ello, en estos casos s&oacute;lo se reintegrar&aacute; la proporci&oacute;n del precio correspondiente a los servicios terrestres en caso de corresponder (hoteler&iacute;a, pensi&oacute;n, excursiones) tal como determine el organizador, seg&uacute;n la modalidad con que opere el prestador del servicio. Toda vez que el pasajero ha sido debidamente informado por la Agencia de Viajes de todo el detalle del viaje en el primer documento o informaci&oacute;n emitida por esta empresa, y que los prestadores a&eacute;reos y/o transportistas pueden por razones de mejor servicio producir alteraciones en los horarios, postergaciones y/o cancelaciones, comodidades y/o equipos utilizados, etc. la Juli&aacute; Tours no asume responsabilidad alguna, m&aacute;s all&aacute; de la debida informaci&oacute;n, y deja constancia que en el caso de transporte aerocomercial se aplican las normas sobre el contrato de transporte a&eacute;reo y sus limitaciones, conforme Convenio de Montreal, Ley 26.451, Res. 1532/98 MEOySP y sus modificatorias, Decreto Nacional 1.470/97; en el caso de transporte terrestre, las normas de del C&oacute;digo Civil y Comercial de la Naci&oacute;n, ley 26.994. Se informa que el billete de transporte a&eacute;reo tiene una vigencia de un a&ntilde;o desde la fecha de su emisi&oacute;n, independientemente de las fechas de partida y regreso y que, emitido, constituye el &uacute;nico contrato entre las transportadoras y el pasajero. Es perentorio que el pasajero contrate juntamente con la reserva y pago de los dem&aacute;s servicios un seguro cuya cobertura incluya los cargos de cancelaci&oacute;n del transporte. Para hacerla efectiva, el pasajero deber&aacute; efectuar el reclamo pertinente ante la aseguradora en cuesti&oacute;n.</p>\r\n<p>Conforme las leyes vigentes,&nbsp;cuando el transporte se lleve a cabo por medios a&eacute;reos, terrestres, de locomoci&oacute;n, lacustre, fluviales o mar&iacute;timos, el viajero se somete expresamente a las normas propias de cada contrataci&oacute;n, por lo que las indemnizaciones a que hubiere lugar y que pudieren pagar los responsables, ser&aacute;n abonadas a los beneficiarios, interesados o representantes legales directamente, en la moneda, oportunidad y lugar que determine el responsable del servicio, con las limitaciones que en cada caso las normas establezcan. Los aeropuertos est&aacute;n sujetos a condiciones de tr&aacute;fico, operatividad, clima y condiciones sanitarias; los horarios son aproximados, sin perjuicio de lo cual el pasajero deber&aacute; cumplir estrictamente los horarios pautados por las l&iacute;neas a&eacute;reas y/o transportistas en cuanto a su presentaci&oacute;n ante los mostradores de embarque. En consecuencia, el pasajero se notifica expresamente que los horarios de partida y arribo son tentativos y podr&iacute;an modificarse conforme decisi&oacute;n de la transportadora o de las autoridades aeroportuarias. Por tanto, Juli&aacute; Tours est&aacute; exenta de toda responsabilidad debido a dicha contingencia y sus consecuencias en el desarrollo del itinerario del pasajero. Juli&aacute; Tours no es responsable en ning&uacute;n caso por retrasos, cambios de horarios, o anulaciones de los vuelos, y tampoco de los gastos que dicha situaci&oacute;n origine, atento la especialidad de la legislaci&oacute;n en materia de transporte aerocomercial. Los pasajeros que no se presenten a embarcar, perder&aacute;n inexorablemente el monto abonado y la posibilidad de efectuar cambios y/o reclamaciones respecto de las reservas con bloqueos y/o charters. Toda reclamaci&oacute;n en virtud de servicios a&eacute;reos deber&aacute; canalizarse a trav&eacute;s de&nbsp;<a title=\"V&iacute;nculo a sitio web de ANAC\" href=\"http://www.anac.gov.ar/anac/web/index.php/2/185/tramites/reclamos-quejas\" target=\"_blank\" rel=\"noopener\">http://www.anac.gov.ar/anac/web/index.php/2/185/tramites/reclamos-quejas</a>. El equipaje permitido en vuelo, es el que permita la tarifa que haya elegido el pasajero y en los circuitos, ser&aacute; de una maleta por pasajero, y estar&aacute; sujeto a las limitaciones y reglamentaciones en materia de pesos y medidas que impongan los transportistas conforme el medio de transporte utilizado; y en todos los casos, es transportado por cuenta y riesgo de aqu&eacute;l. Se sugiere controlar el peso permitido por las aerol&iacute;neas transportadoras, puesto que en caso de utilizar diferentes transportadoras &eacute;stos podr&iacute;an variar el peso permitido a los pasajeros y/o las dimensiones del equipaje permitido tanto en cabina como en bodega, m&aacute;xime si los vuelos son de diferentes compa&ntilde;&iacute;as o no se encuentran todos registrados en una misma reserva. En tanto, el pasajero tiene el deber de custodiar su equipaje de mano y/o equipaje no despachado durante todo el tiempo que dure la transportaci&oacute;n por no ser responsables las transportadoras, conforme legislaci&oacute;n vigente. No se responde por da&ntilde;os en el equipaje, p&eacute;rdida o deterioro de equipajes.</p>\r\n<p><strong>CESI&Oacute;N Y TRANSFERENCIA</strong></p>\r\n<p>El derecho que confiere al cliente el contrato de servicios tur&iacute;sticos podr&aacute; ser cedido o transferido a otras personas hasta 30 d&iacute;as antes de la fecha de salida, siempre que no se opongan a ello las prescripciones del transportista, del hotelero o del prestador de los servicios y el pasajero comunique tal decisi&oacute;n de manera fehaciente a la empresa con una anticipaci&oacute;n no menor a 30 d&iacute;as a la fecha de salida. En los supuestos que los pasajeros sean de distintas edades (mayores-menores), se ajustar&aacute; el precio tarifas vigentes al momento de la solicitud. En todos los casos de cesi&oacute;n o transferencia, Juli&aacute; Tours podr&aacute; percibir un cargo del 10% del monto convenido. Por normas internacionales de seguridad, los billetes que conforman el contrato de transporte a&eacute;reo y/o mar&iacute;timo no son transferibles a terceros, como tampoco todos aquellos servicios que por disposici&oacute;n del prestador no permitan tal cesi&oacute;n.</p>\r\n<p><strong>RESPONSABILIDAD</strong></p>\r\n<p>1) Esta empresa podr&aacute; actuar como mayorista o minorista, seg&uacute;n el caso; pero siempre ser&aacute; intermediaria en la reserva o contrataci&oacute;n de los distintos servicios vinculados e incluidos en el respectivo tour o reservaci&oacute;n de servicios: hoteles, restaurantes, medios de transportes u otros prestadores. No obstante ello, la responsabilidad est&aacute; determinada conforme las disposiciones contenidas en la ley 18.829, su decreto reglamentario 2172/82, Resoluciones del Ministerio de Turismo y Deporte de la Naci&oacute;n y modificatorias en caso de corresponder, Ley 24.240 y CCCN. 2) Esta compa&ntilde;&iacute;a no es responsable por los hechos fortuitos o de fuerza mayor, fen&oacute;menos clim&aacute;ticos, de la naturaleza, pand&eacute;micos y epid&eacute;micos, situaciones de conflicto b&eacute;lico que acontezcan previo o durante el desarrollo del tour y que impidan, demoren o de cualquier modo obstaculicen la ejecuci&oacute;n total o parcial de las prestaciones comprometidas por esta empresa, todo ello de conformidad con las disposiciones del C&oacute;digo Civil y Comercial de la Naci&oacute;n. Se deja a salvo que todo viaje queda supeditado a las disposiciones sanitarias y/o migratorias del pa&iacute;s de origen y destino de cada viaje en el momento preciso en que deba iniciar o finalizar el viaje. Se deja expresa constancia que esta compa&ntilde;&iacute;a no conforma ning&uacute;n grupo econ&oacute;mico ni tiene asociaci&oacute;n alguna con empresas navieras, l&iacute;neas a&eacute;reas, hoteles ni organizadoras de excursiones, concursos, promociones, resorts u otros organizadores de tours, en el pa&iacute;s ni en el exterior, como tampoco con la Agencia minorista de viajes que comercialice sus servicios y/o productos. 3) Los pasajeros deben obligatoriamente contratar asistencia al viajero acorde con su rango etario y geolocalizaci&oacute;n del destino elegido, la cual deber&aacute; cubrir la totalidad del tiempo de permanencia acorde con el viaje y los valores m&iacute;nimos de prestaci&oacute;n exigidos en el lugar de destino de viaje. El pasajero deber&aacute; consultar si en su destino de viaje existen exigencias especiales a este respecto. De no hacerlo, se entiende que asumen en forma personal todos los riesgos de cualquier naturaleza que se pudiesen presentar sobre su persona, sus bienes y/o de terceros. Toda asistencia al viajero, aun la que no se contrate a trav&eacute;s de la intermediaci&oacute;n de esta agencia de viajes debe amparar la contingencia &ldquo;pandemia&rdquo; por asistencia y cancelaci&oacute;n o suspensi&oacute;n del viaje. 4) Los datos personales contenidos en las reservas de viaje contratadas a trav&eacute;s de esta empresa son tratados conforme establece la Ley 25.326 y ser&aacute;n conservados el menor tiempo necesario. S&oacute;lo ser&aacute;n divulgados aquellos datos indispensables para concretar las reservas de los servicios elegidos. En materia de datos personales: el titular de los datos personales tiene la facultad de ejercer el derecho de acceso a los mismos en forma gratuita a intervalos no inferiores a seis meses, salvo que acredite un inter&eacute;s leg&iacute;timo (cfr. Art. 14, inc. 3&deg; Ley N&ordm; 25.326). La Direcci&oacute;n Nacional de Protecci&oacute;n de Datos Personales tiene la atribuci&oacute;n de atender las denuncias y reclamos que se interpongan con relaci&oacute;n al incumplimiento de las normas sobre protecci&oacute;n de datos personales. El titular podr&aacute; en cualquier momento solicitar el retiro o el bloqueo de su nombre de los bancos de datos en los que se hallen sus datos. En toda comunicaci&oacute;n con fines de publicidad que se realice por correo, tel&eacute;fono, correo electr&oacute;nico, internet u otro medio a distancia se deber&aacute; indicar, en forma expresa y destacada, la posibilidad del titular del dato de solicitar el retiro o bloqueo, total o parcial, de su nombre de la base de datos. A pedido del interesado, se deber&aacute; informar el nombre del responsable o usuario del banco de datos que provey&oacute; la informaci&oacute;n.</p>\r\n<p><strong>ALTERACIONES O MODIFICACIONES</strong></p>\r\n<p>1) Los prestadores se reservan el derecho, por razones t&eacute;cnicas, operativas y/o de fuerza mayor -incluso por causas epid&eacute;micas o pand&eacute;micas y medidas sanitarias y migratorias de los diferentes pa&iacute;ses en que se desarrolle la prestaci&oacute;n tur&iacute;stica-, de alterar total o parcialmente el ordenamiento diario y/o de servicios que componen el tour, antes o durante su ejecuci&oacute;n. 2) Salvo condici&oacute;n expresa en contrario, los hoteles estipulados podr&aacute;n ser cambiados por otro de igual o mayor categor&iacute;a dentro del mismo n&uacute;cleo urbano sin cargo alguno para el pasajero. Respecto de estas variaciones, en tanto sean efectuadas por razones de mejor confort, servicio y/o fuerza mayor el pasajero no tendr&aacute; derecho a compensaci&oacute;n alguna. 3) La agencia de viajes tendr&aacute; el derecho de anular cualquier tour cuando se configure alguna de las circunstancias previstas en el art. 24 del Decreto N&deg; 2182/72. 4) Una vez comenzado el viaje, la suspensi&oacute;n, modificaci&oacute;n o interrupci&oacute;n de los servicios por parte del pasajero por razones personales de cualquier &iacute;ndole, no dar&aacute; lugar a reclamo, reembolso o devoluci&oacute;n alguna. La interrupci&oacute;n de los servicios por causa imputable al pasajero acarrea costos. En todos los casos la agencia de viajes se compromete a prestar asistencia t&eacute;cnica para que el pasajero pueda continuar su viaje en las mejores condiciones posibles conforme su alcance. El pasajero deber&aacute; asumir los costos por regreso anticipado, conforme las penalidades que cobren las l&iacute;neas a&eacute;reas.</p>\r\n<p><strong>CL&Aacute;USULA DE ARBITRAJE</strong></p>\r\n<p>Toda cuesti&oacute;n que surja con motivo de la celebraci&oacute;n, cumplimiento, incumplimiento, pr&oacute;rroga o rescisi&oacute;n del presente, podr&aacute; ser sometida por las partes a la resoluci&oacute;n del Servicio Nacional de Arbitraje de Consumo, puntualmente el servicio de Arbitraje Tur&iacute;stico implementado por la Resoluci&oacute;n 65/2018. Al respecto ver:&nbsp;<a title=\"V&iacute;nculo a sitio de Producci&oacute;n Argentina\" href=\"https://www.produccion.gob.ar/tramites/sistema-nacional-de-arbitraje-del-consumo-50052\" target=\"_blank\" rel=\"noopener\">https://www.produccion.gob.ar/tramites/sistema-nacional-de-arbitraje-del-consumo-50052</a>&nbsp;y/o el Tribunal Arbitral de la Federaci&oacute;n Argentina de Empresas de Viajes y Turismo y/o de los Tribunales Arbitrales que funcionen en sus respectivas Asociaciones Regionales en tanto &eacute;stos fueren conformados. En caso de sometimiento a dicha jurisdicci&oacute;n, los contratantes se sujetan y dan por aceptadas todas las condiciones establecidas por la Reglamentaci&oacute;n del Tribunal Arbitral que les ser&aacute; entregada por &eacute;ste, oportunamente.</p>\r\n<p><strong>NORMAS DE APLICACI&Oacute;N</strong></p>\r\n<p>El presente, y en su caso la prestaci&oacute;n de los servicios se regir&aacute; por estas condiciones generales, por la Ley N&deg; 18.829, su decreto reglamentario y normas concordantes, as&iacute; como el C&oacute;digo Civil y Comercial de la Naci&oacute;n y la Ley de Defensa del Consumidor, conocida por las partes. Toda la documentaci&oacute;n que se genere a favor del pasajero y se entregue a la Agencia como consecuencia del viaje, conformar&aacute; el Contrato de Viaje, y es informaci&oacute;n confidencial, protegida por La Ley de Protecci&oacute;n de Datos Personales &ndash; Ley 25326. En materia aerocomercial, rigen las estipulaciones del C&oacute;digo Aeron&aacute;utico, Conv. Montreal y Res. 1532/98 M.E.O.y S.P. Respecto de los pagos, rigen las Comunicaciones de BCRA relativas a giro de divisas al exterior vigentes a la fecha y las normas del C&oacute;digo Civil y Comercial de la Naci&oacute;n en materia de obligaciones de dar dinero cuando se trate de moneda de curso legal o de dar cosas cuando se trate de moneda extranjera.</p>\r\n<p><strong>ACEPTACI&Oacute;N DE LOS T&Eacute;RMINOS DE CONTRATACI&Oacute;N</strong></p>\r\n<p>El hecho de efectuar el pago del derecho de inscripci&oacute;n o la reserva del viaje, implica por parte, tanto de la agencia como del/de los pasajeros, el pleno conocimiento y total conformidad con estas Condiciones Generales. Todos los servicios ofrecidos se encuentran sujetos a las modalidades, condiciones de cancelaci&oacute;n y/o de devoluci&oacute;n que establezcan los operadores tur&iacute;sticos, las compa&ntilde;&iacute;as a&eacute;reas y fundamentalmente las disposiciones sanitarias dispuestas por las autoridades gubernamentales y/o migratorias de cada uno de los destinos, sean interiores o en viajes al exterior. Estas condiciones generales se informan presencialmente, o por medios digitales (correo electr&oacute;nico, whatsapp) o bien por cualquier otro medio consentido por las partes para la contrataci&oacute;n y asimismo, se encuentran publicadas en el sitio web www.juliatours.tur.ar, conforme dispone el art&iacute;culo 4to de la Ley 24.240, modificado por el art. 169 del decreto 27/2018 y Res. 915-E/2017. JULIA TOURS ARGENTINA EVT. Leg 1784&nbsp; Resol. 086/80-302-85 Disp. 1693, con domicilio social en SUIPACHA 570 Piso 1&deg; Of. &ldquo;A&rdquo;, Argentina, Te. (011) 4021-5800, info@juliatours.com.ar, CUIT 30-57685676-4.</p>\r\n<p>Las partes se valen de t&eacute;cnicas de la comunicaci&oacute;n e informaci&oacute;n, conforme el art. 1106 CCCN, y la instrumentaci&oacute;n del contrato de viaje se har&aacute; presencial o por medios electr&oacute;nicos, conforme autoriza el art. 287 CCCN, de conformidad con la forma de contrataci&oacute;n que las partes opten.</p>\r\n<p>Es parte de los t&eacute;rminos de esta contrataci&oacute;n que la oferta est&aacute; sujeta a las correspondientes autorizaciones gubernamentales y/o sanitarias nacionales y/o extranjeras vigentes a la fecha del comienzo o finalizaci&oacute;n del viaje/estad&iacute;a. Las aerol&iacute;neas/navieras y dem&aacute;s prestadores vinculados a su viaje podr&aacute;n disponer protocolos que deber&aacute;n ser necesariamente observados por los pasajeros, los que se informar&aacute;n previo al inicio de su viaje/estad&iacute;a. El pasajero deber&aacute; estar atento a la informaci&oacute;n din&aacute;mica de las autoridades de los diferentes pa&iacute;ses destino de su viaje.</p>\r\n<p><strong>INCUMPLIMIENTO</strong></p>\r\n<p>En caso de efectiva divergencia entre los servicios ofrecidos y los servicios contratados, el pasajero podr&aacute; recurrir al Ministerio de Turismo de la Naci&oacute;n, sito en Suipacha 1111, Ciudad Aut&oacute;noma de Buenos Aires, Tel.: 4316-1600 y/o a la Direcci&oacute;n Nacional de Defensa del Consumidor, Julio A. Roca 651, Ciudad Aut&oacute;noma de Buenos Aires y/o a las Oficinas del consumidor correspondientes seg&uacute;n su domicilio.&nbsp;<em>If the tourist services aren&acute;t as you&acute;ve paid for, you&acute;re allowed to contact Argentina Tourist Authority, at 1111 Suipacha St., 7th floor, Buenos Aires, Argentina or Consumer Defense Bureau at Julio A. Roca 651, Ciudad Aut&oacute;noma de Buenos Aires (cfr. Ley 25.651) or to the corresponding consumer offices according to your address</em>.</p>\r\n<p>&nbsp;</p>', NULL, NULL, NULL, NULL, '2021-07-19 19:32:07', '1', '2021-07-20 19:40:49', '2', 0, 2, 'published'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-07-20 14:15:08', '2', NULL, NULL, 0, 3, 'temp'),
(4, NULL, 'Oficina Virtual', 'oficina-virtual', NULL, '<p><img src=\"/media/tinymce/5dfb2955655644a28e43383d1f0472b3-0001.jpg\" alt=\"\" /></p>', NULL, NULL, NULL, NULL, '2021-07-20 14:15:10', '2', '2021-07-20 12:15:35', '2', 0, 4, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `email` char(80) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `categorias` varchar(100) DEFAULT NULL,
  `telefono` char(20) DEFAULT NULL,
  `ciudad` varchar(200) DEFAULT NULL,
  `provincia` varchar(200) DEFAULT NULL,
  `pais` varchar(200) DEFAULT NULL,
  `documento_tipo` char(15) DEFAULT NULL,
  `documento_numero` char(15) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `parent_id`, `email`, `password`, `salt`, `apellido`, `nombre`, `categorias`, `telefono`, `ciudad`, `provincia`, `pais`, `documento_tipo`, `documento_numero`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, NULL, '5e22e76b348392e3875fbea6395bf0461a4c639386d26cc436c08be521dbecf6', '24fe6f4571a300e7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-19 22:56:27', '1', NULL, NULL, 0, 1, 'temp'),
(2, NULL, NULL, 'c9faa3262e4a6117248805828865b9a0fcbed8bc3f21b698a18bb77e2c3f6957', '52f2ddf857bc20d1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-01 01:14:13', '1', NULL, NULL, 0, 2, 'temp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_base`
--

CREATE TABLE `_base` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text DEFAULT NULL,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT current_timestamp(),
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `pos` int(11) NOT NULL DEFAULT 0,
  `status` char(20) NOT NULL DEFAULT 'temp'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `admin_rols`
--
ALTER TABLE `admin_rols`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `destinations`
--
ALTER TABLE `destinations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `packages`
--
ALTER TABLE `packages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `page_blocks`
--
ALTER TABLE `page_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`),
  ADD KEY `nombre` (`title`);

--
-- Indices de la tabla `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `art_id` (`art_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indices de la tabla `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `static_pages`
--
ALTER TABLE `static_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indices de la tabla `_base`
--
ALTER TABLE `_base`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `admin_rols`
--
ALTER TABLE `admin_rols`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `destinations`
--
ALTER TABLE `destinations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `media`
--
ALTER TABLE `media`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `packages`
--
ALTER TABLE `packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `page_blocks`
--
ALTER TABLE `page_blocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=236;

--
-- AUTO_INCREMENT de la tabla `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `static_pages`
--
ALTER TABLE `static_pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `_base`
--
ALTER TABLE `_base`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
