-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 19-07-2021 a las 19:57:22
-- Versión del servidor: 5.7.19
-- Versión de PHP: 7.4.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `manyur_db`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_rols`
--

DROP TABLE IF EXISTS `admin_rols`;
CREATE TABLE IF NOT EXISTS `admin_rols` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'inactive',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_rols`
--

INSERT INTO `admin_rols` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Administrador', 'administrador', NULL, NULL, NULL, NULL, '2019-11-12 18:15:13', NULL, NULL, NULL, 0, 1, 'published'),
(2, NULL, 'Editor', 'editor', NULL, NULL, NULL, NULL, '2019-11-12 21:19:18', NULL, NULL, NULL, 0, 2, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
CREATE TABLE IF NOT EXISTS `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `first_name` char(60) DEFAULT NULL,
  `last_name` char(60) DEFAULT NULL,
  `email` char(80) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `rol_id` tinyint(1) DEFAULT NULL,
  `phone_number` char(30) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `admin_users`
--

INSERT INTO `admin_users` (`id`, `parent_id`, `first_name`, `last_name`, `email`, `password`, `salt`, `rol_id`, `phone_number`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Alejandro', 'Ferreiro', 'dg.alef@gmail.com', '4d0d8e5c8907af4c1868e7ceced85b6e8e75d27f79a7bd767017ac53afef4f5b', '19dab902211ae42c', 1, NULL, '', '', NULL, '', NULL, NULL, '2019-11-12 23:38:09', NULL, '2020-11-27 18:27:35', '1', 0, 0, 'published'),
(2, NULL, 'Mauro', 'Mansilla', 'mmansilla@turismomanyur.tur.ar', '6f65a42d7ee603a9dfa84eca9986f41279aa6b18e29d356ddc33a6d5a7ef7e98', '7d20976b1503ae6e', 1, NULL, NULL, NULL, NULL, 'M@uro@Mans1lla', NULL, NULL, '2021-07-19 19:54:55', '1', '2021-07-19 19:56:41', '1', 0, 1, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

DROP TABLE IF EXISTS `categories`;
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `text` text,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `destinations`
--

DROP TABLE IF EXISTS `destinations`;
CREATE TABLE IF NOT EXISTS `destinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `destinations`
--

INSERT INTO `destinations` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Argentina', 'argentina', '', '', NULL, NULL, NULL, NULL, '2021-07-13 14:04:36', '1', '2021-07-13 21:55:52', '1', 1, 1, 'published'),
(2, NULL, 'USA & Canadá', 'usa-canada', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:06:46', '1', '2021-07-16 22:09:07', '1', 1, 4, 'published'),
(3, NULL, 'Brasil', 'brasil', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:06:57', '1', '2021-07-13 21:55:53', '1', 1, 2, 'published'),
(4, NULL, 'Caribe', 'caribe', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:07:07', '1', '2021-07-13 21:55:54', '1', 1, 3, 'published'),
(5, NULL, 'Europa', 'europa', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:26:08', '1', '2021-07-13 21:55:55', '1', 1, 5, 'published'),
(6, NULL, 'Exóticos', 'exoticos', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:26:52', '1', '2021-07-16 22:08:13', '1', 1, 6, 'published'),
(7, NULL, 'Salidas grupales', 'salidas-grupales', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:27:00', '1', '2021-07-16 22:09:58', '1', 1, 7, 'published'),
(8, NULL, 'Cruceros', 'cruceros', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 14:27:12', '1', '2021-07-16 22:08:37', '1', 1, 8, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_class` varchar(100) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `file_name` varchar(150) DEFAULT NULL,
  `file_source_name` varchar(150) DEFAULT NULL,
  `file_ext` char(5) DEFAULT NULL,
  `file_type` char(20) DEFAULT NULL,
  `file_sizes` varchar(300) DEFAULT NULL,
  `media_title` text,
  `media_description` text,
  `label` varchar(50) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT '0',
  `pos` int(11) DEFAULT NULL,
  `status` char(20) DEFAULT 'published',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `media`
--

INSERT INTO `media` (`id`, `parent_class`, `parent_id`, `file_name`, `file_source_name`, `file_ext`, `file_type`, `file_sizes`, `media_title`, `media_description`, `label`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(3, 'destinations', 1, 'argentina_0.jpg', 'argentina.jpg', 'jpg', 'images', '{\"src\":\"3187x630\",\"xlarge\":\"2880x569\",\"large\":\"1920x380\",\"medium\":\"1280x253\",\"small\":\"640x127\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:18:06', '1', NULL, NULL, 0, 3, 'published'),
(4, 'destinations', 1, 'argentinathum.jpg', 'argentina_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:18:14', '1', NULL, NULL, 0, 4, 'published'),
(5, 'destinations', 3, 'brasil_0.jpg', 'brasil.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:18:36', '1', NULL, NULL, 0, 5, 'published'),
(6, 'destinations', 3, 'brasilthum.jpg', 'brasil_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:18:43', '1', NULL, NULL, 0, 6, 'published'),
(7, 'destinations', 4, 'caribe.jpg', 'caribe.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:26:02', '1', NULL, NULL, 0, 7, 'published'),
(8, 'destinations', 4, 'caribethum.jpg', 'caribe_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:26:06', '1', NULL, NULL, 0, 8, 'published'),
(9, 'destinations', 5, 'europa.jpg', 'europa.jpg', 'jpg', 'images', '{\"src\":\"2000x395\",\"xlarge\":\"2000x395\",\"large\":\"1920x379\",\"medium\":\"1280x253\",\"small\":\"640x126\",\"tiny\":\"200x40\"}', '', '', 'banner', '2021-07-13 14:26:20', '1', NULL, NULL, 0, 9, 'published'),
(10, 'destinations', 5, 'europathum.jpg', 'europa_thum.jpg', 'jpg', 'images', '{\"src\":\"540x360\",\"xlarge\":\"540x360\",\"large\":\"540x360\",\"medium\":\"540x360\",\"small\":\"540x360\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-13 14:26:25', '1', NULL, NULL, 0, 10, 'published'),
(17, 'slider', 1, 'fitz-roy.jpg', 'Fitz Roy.jpg', 'jpg', 'images', '{\"src\":\"2000x1369\",\"xlarge\":\"2000x1369\",\"large\":\"1920x1314\",\"medium\":\"1280x876\",\"small\":\"640x438\",\"tiny\":\"200x137\"}', '', '', 'slide', '2021-07-13 19:25:33', '1', NULL, NULL, 0, 17, 'published'),
(18, 'packages', 1, 'pnt-seleccion-56_0.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'thumbnail', '2021-07-13 20:05:02', '1', NULL, NULL, 0, 18, 'published'),
(19, 'packages', 1, 'pnt-seleccion-56_1.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'gallery', '2021-07-13 20:05:24', '1', NULL, NULL, 0, 19, 'published'),
(20, 'packages', 1, 'talampaya-2-argentinayelmundo_0.jpg', 'Talampaya 2 @argentinayelmundo.jpg', 'jpg', 'images', '{\"src\":\"1080x1080\",\"xlarge\":\"1080x1080\",\"large\":\"1080x1080\",\"medium\":\"1080x1080\",\"small\":\"640x640\",\"tiny\":\"200x200\"}', '', '', 'gallery', '2021-07-13 20:05:26', '1', NULL, NULL, 0, 20, 'published'),
(21, 'packages', 1, 'talampaya-5-facundocarrizo24_0.jpg', 'Talampaya 5 @facundo_carrizo24.jpg', 'jpg', 'images', '{\"src\":\"1080x1080\",\"xlarge\":\"1080x1080\",\"large\":\"1080x1080\",\"medium\":\"1080x1080\",\"small\":\"640x640\",\"tiny\":\"200x200\"}', '', '', 'gallery', '2021-07-13 20:05:27', '1', NULL, NULL, 0, 21, 'published'),
(22, 'slider', 2, 'pnt-seleccion-56_2.jpg', 'PNT-seleccion-56.jpg', 'jpg', 'images', '{\"src\":\"2048x1296\",\"xlarge\":\"2048x1296\",\"large\":\"1920x1215\",\"medium\":\"1280x810\",\"small\":\"640x405\",\"tiny\":\"200x127\"}', '', '', 'slide', '2021-07-13 23:00:42', '1', NULL, NULL, 0, 22, 'published'),
(23, 'packages', 3, 'porto-galinhas-1.jpg', 'porto-galinhas-1.jpg', 'jpg', 'images', '{\"src\":\"1024x1024\",\"xlarge\":\"1024x1024\",\"large\":\"1024x1024\",\"medium\":\"1024x1024\",\"small\":\"640x640\",\"tiny\":\"200x200\"}', '', '', 'thumbnail', '2021-07-16 21:42:03', '1', NULL, NULL, 0, 23, 'published'),
(24, 'packages', 3, 'piscinas-naturales.jpg', 'Piscinas-Naturales.jpg', 'jpg', 'images', '{\"src\":\"2048x1365\",\"xlarge\":\"2048x1365\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'gallery', '2021-07-16 21:42:18', '1', NULL, NULL, 0, 24, 'published'),
(25, 'packages', 3, 'playa-de-porto-de-galinhas.jpg', 'playa-de-porto-de-galinhas.jpg', 'jpg', 'images', '{\"src\":\"2048x1365\",\"xlarge\":\"2048x1365\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'gallery', '2021-07-16 21:42:21', '1', NULL, NULL, 0, 25, 'published'),
(26, 'packages', 3, 'portodegalinhas-003.jpg', 'Porto_de_Galinhas-003.jpg', 'jpg', 'images', '{\"src\":\"2000x1500\",\"xlarge\":\"2000x1500\",\"large\":\"1920x1440\",\"medium\":\"1280x960\",\"small\":\"640x480\",\"tiny\":\"200x150\"}', '', '', 'gallery', '2021-07-16 21:42:23', '1', NULL, NULL, 0, 26, 'published'),
(27, 'packages', 3, 'porto-de-gal-1.jpg', 'porto-de-gal-1.jpg', 'jpg', 'images', '{\"src\":\"4368x2912\",\"xlarge\":\"2880x1920\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'gallery', '2021-07-16 21:42:29', '1', NULL, NULL, 0, 27, 'published'),
(28, 'packages', 2, 'puerto-madryn.jpg', 'Puerto-Madryn.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 21:43:33', '1', NULL, NULL, 0, 28, 'published'),
(29, 'packages', 2, 'puerto-madryn_0.jpg', 'Puerto-Madryn.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'gallery', '2021-07-16 21:43:41', '1', NULL, NULL, 0, 29, 'published'),
(30, 'destinations', 6, 'bali.jpg', 'bali.jpg', 'jpg', 'images', '{\"src\":\"4320x2880\",\"xlarge\":\"2880x1920\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-16 22:08:13', '1', NULL, NULL, 0, 30, 'published'),
(32, 'destinations', 2, 'lake-tahoe-kayaking-tours.jpg', 'lake-tahoe-kayaking-tours.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 22:09:05', '1', NULL, NULL, 0, 32, 'published'),
(33, 'destinations', 7, 'iguazu.jpg', 'iguazu.jpg', 'jpg', 'images', '{\"src\":\"4608x3072\",\"xlarge\":\"2880x1920\",\"large\":\"1920x1280\",\"medium\":\"1280x853\",\"small\":\"640x427\",\"tiny\":\"200x133\"}', '', '', 'thumbnail', '2021-07-16 22:09:57', '1', NULL, NULL, 0, 33, 'published'),
(34, 'destinations', 8, 'carnaval-en-crucero-costa-fascinosa.jpg', 'Carnaval-en-Crucero-Costa-Fascinosa.jpg', 'jpg', 'images', '{\"src\":\"1920x1080\",\"xlarge\":\"1920x1080\",\"large\":\"1920x1080\",\"medium\":\"1280x720\",\"small\":\"640x360\",\"tiny\":\"200x113\"}', '', '', 'thumbnail', '2021-07-16 22:10:30', '1', NULL, NULL, 0, 34, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `packages`
--

DROP TABLE IF EXISTS `packages`;
CREATE TABLE IF NOT EXISTS `packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `price` varchar(200) DEFAULT NULL,
  `legend` varchar(200) DEFAULT NULL,
  `text` text,
  `legals` text,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `packages`
--

INSERT INTO `packages` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `price`, `legend`, `text`, `legals`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'NOA', 'noa', 'Acá puede ir un subtítulo libre', 'desde $ 25.000', '2 noches c/desayuno', '<h4>Incluye:</h4>\r\n<ul>\r\n<li>Bus ida y regreso</li>\r\n<li>Paseo por el barrio de Barracas, conociendo su historia, &eacute;poca de esplendor, iglesias, misterios y leyendas</li>\r\n<li>Merienda en BAHAMAS CLUB en COSTANERA</li>\r\n<li>Coordinaci&oacute;n</li>\r\n</ul>\r\n<h4>Itinerario</h4>\r\n<p>Parque Lezama, F&aacute;brica Canale, F&aacute;brica Alpargatas, Iglesia de Santa Felicitas, Casa de los leones, Ex Hospital Casa Cuna, Iglesia del Inmaculado Coraz&oacute;n de Mar&iacute;a, F&aacute;brica Aguila Saint, Estaci&oacute;n Yrigoyen, Bas&iacute;lica del Sagrado Coraz&oacute;n de Jes&uacute;s, Hospital Borda y Moyano, Pasaje Lan&iacute;n, Hospital Malbr&aacute;n, Ribera, Viejo Puente Puerred&oacute;n, Barracas</p>\r\n<h4>Men&uacute;</h4>\r\n<ul>\r\n<li>Una infusion: caf&eacute; americano, caf&eacute; con leche, t&eacute; con leche o &nbsp;capuchino</li>\r\n<li>Una medialuna</li>\r\n<li>Un sandwich de miga mixto</li>\r\n<li>Tostadas completas con manteca, mermelada, queso crema</li>\r\n<li>Crocantes de manzana &oacute; brownies</li>\r\n</ul>\r\n<p>No se suspende por lluvia.</p>\r\n<p><strong>General $ 2.200</strong></p>\r\n<p>Puntos de ascenso: Villa Ballester, San Mart&iacute;n, Av San Mart&iacute;n y Gral Paz.</p>\r\n<p>-------<br /><em>*** Fecha de salida sujeta a la finalizaci&oacute;n del aislamiento social, preventivo y obligatorio ***</em></p>', '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>', NULL, NULL, NULL, NULL, '2021-07-13 14:48:56', '1', '2021-07-16 22:01:09', '1', 0, 1, 'published'),
(2, NULL, 'Puerto Madryn', 'puerto-madryn', 'Acá puede ir un subtítulo libre', 'desde $ 25.000', '2 noches c/desayuno', '<h4>Incluye:</h4>\r\n<ul>\r\n<li>Bus ida y regreso</li>\r\n<li>Paseo por el barrio de Barracas, conociendo su historia, &eacute;poca de esplendor, iglesias, misterios y leyendas</li>\r\n<li>Merienda en BAHAMAS CLUB en COSTANERA</li>\r\n<li>Coordinaci&oacute;n</li>\r\n</ul>\r\n<h4>Itinerario</h4>\r\n<p>Parque Lezama, F&aacute;brica Canale, F&aacute;brica Alpargatas, Iglesia de Santa Felicitas, Casa de los leones, Ex Hospital Casa Cuna, Iglesia del Inmaculado Coraz&oacute;n de Mar&iacute;a, F&aacute;brica Aguila Saint, Estaci&oacute;n Yrigoyen, Bas&iacute;lica del Sagrado Coraz&oacute;n de Jes&uacute;s, Hospital Borda y Moyano, Pasaje Lan&iacute;n, Hospital Malbr&aacute;n, Ribera, Viejo Puente Puerred&oacute;n, Barracas</p>\r\n<h4>Men&uacute;</h4>\r\n<ul>\r\n<li>Una infusion: caf&eacute; americano, caf&eacute; con leche, t&eacute; con leche o &nbsp;capuchino</li>\r\n<li>Una medialuna</li>\r\n<li>Un sandwich de miga mixto</li>\r\n<li>Tostadas completas con manteca, mermelada, queso crema</li>\r\n<li>Crocantes de manzana &oacute; brownies</li>\r\n</ul>\r\n<p>No se suspende por lluvia.</p>\r\n<p><strong>General $ 2.200</strong></p>\r\n<p>Puntos de ascenso: Villa Ballester, San Mart&iacute;n, Av San Mart&iacute;n y Gral Paz.</p>\r\n<p>-------<br /><em>*** Fecha de salida sujeta a la finalizaci&oacute;n del aislamiento social, preventivo y obligatorio ***</em></p>', '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>', NULL, NULL, NULL, NULL, '2021-07-13 14:54:03', '1', '2021-07-16 21:44:02', '1', 0, 2, 'published'),
(3, NULL, 'Buzios', 'buzios', '', 'usd 2000', '6 noches c/desayuno', '', '', NULL, NULL, NULL, NULL, '2021-07-16 21:40:53', '1', '2021-07-16 21:42:29', '1', 0, 3, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `page_blocks`
--

DROP TABLE IF EXISTS `page_blocks`;
CREATE TABLE IF NOT EXISTS `page_blocks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `page` varchar(100) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `text_alt` text,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'published',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `page_blocks`
--

INSERT INTO `page_blocks` (`id`, `parent_id`, `page`, `title`, `subtitle`, `text`, `text_alt`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, NULL, 'ACERCA DE NOSOTROS', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, '2021-07-19 19:26:42', '1', '2021-07-19 19:27:05', '1', 0, 1, 'published'),
(2, NULL, NULL, 'TÉRMINOS Y CONDICIONES', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, '2021-07-19 19:27:10', '1', '2021-07-19 19:27:27', '1', 0, 2, 'draft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

DROP TABLE IF EXISTS `paises`;
CREATE TABLE IF NOT EXISTS `paises` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `isonum` smallint(6) DEFAULT NULL,
  `iso2` char(2) DEFAULT NULL,
  `iso3` char(3) DEFAULT NULL,
  `title` varchar(80) DEFAULT NULL,
  `title_en` varchar(100) DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` int(11) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `featured` tinyint(1) DEFAULT '0',
  `status` char(10) NOT NULL DEFAULT 'published',
  `pos` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `nombre` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=236 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `isonum`, `iso2`, `iso3`, `title`, `title_en`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `status`, `pos`) VALUES
(4, 276, 'DE', 'DEU', 'Alemania', 'Germany', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(13, 32, 'AR', 'ARG', 'Argentina', 'Argentina', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(19, 44, 'BS', 'BHS', 'Bahamas', 'Bahamas', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(29, 68, 'BO', 'BOL', 'Bolivia', 'Bolivia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(33, 76, 'BR', 'BRA', 'Brasil', 'Brazil', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(35, 100, 'BG', 'BGR', 'Bulgaria', 'Bulgary', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(46, 152, 'CL', 'CHL', 'Chile', 'Chile', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(52, 170, 'CO', 'COL', 'Colombia', 'Colombia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(60, 188, 'CR', 'CRI', 'Costa Rica', 'Costa Rica', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(62, 192, 'CU', 'CUB', 'Cuba', 'Cuba', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(66, 218, 'EC', 'ECU', 'Ecuador', 'Ecuador', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(68, 222, 'SV', 'SLV', 'El Salvador', 'El Salvador', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(73, 724, 'ES', 'ESP', 'España', 'Spain', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(75, 840, 'US', 'USA', 'Estados Unidos', 'United States', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(82, 250, 'FR', 'FRA', 'Francia', 'France', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(90, 300, 'GR', 'GRC', 'Grecia', 'Greece', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(94, 320, 'GT', 'GTM', 'Guatemala', 'Guatemala', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(102, 340, 'HN', 'HND', 'Honduras', 'Honduras', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(109, 372, 'IE', 'IRL', 'Irlanda', 'Ireland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(112, 380, 'IT', 'ITA', 'Italia', 'Italy', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(146, 484, 'MX', 'MEX', 'México', 'Mexico', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(149, 492, 'MC', 'MCO', 'Mónaco', 'Monaco', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(157, 558, 'NI', 'NIC', 'Nicaragua', 'Nicaragua', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(162, 578, 'NO', 'NOR', 'Noruega', 'Norway', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(164, 554, 'NZ', 'NZL', 'Nueva Zelanda', 'New Zeland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(170, 591, 'PA', 'PAN', 'Panamá', 'Panama', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(172, 600, 'PY', 'PRY', 'Paraguay', 'Paraguay', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(173, 604, 'PE', 'PER', 'Perú', 'Peru', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(177, 620, 'PT', 'PRT', 'Portugal', 'Portugal', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(178, 630, 'PR', 'PRI', 'Puerto Rico', 'Puerto Rico', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(180, 826, 'GB', 'GBR', 'Reino Unido', 'United Kingdom', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(184, 643, 'RU', 'RUS', 'Rusia', 'Russia', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(197, 891, 'CS', 'SCG', 'Serbia y Montenegro', 'Serbia and Montenegro', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(207, 752, 'SE', 'SWE', 'Suecia', 'Sweeden', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(208, 756, 'CH', 'CHE', 'Suiza', 'Switzerland', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(229, 858, 'UY', 'URY', 'Uruguay', 'Uruguay', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(232, 862, 'VE', 'VEN', 'Venezuela', 'Venezuela', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(233, 376, 'IL', 'ISR', 'Israel', 'Israel', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(234, 214, 'DO', 'DOM', 'República Dominicana', 'Dominican Republic', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0),
(235, 710, 'ZA', 'ZAF', 'Sudáfrica', 'South Africa', '2021-01-13 22:28:41', NULL, NULL, NULL, 0, 'published', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `relations`
--

DROP TABLE IF EXISTS `relations`;
CREATE TABLE IF NOT EXISTS `relations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `art_ref` char(50) DEFAULT NULL COMMENT 'Tabla relacionada 1',
  `art_id` int(11) NOT NULL,
  `tag_ref` char(50) DEFAULT NULL COMMENT 'Tabla relacionada 2',
  `tag_id` int(11) NOT NULL,
  `rel_ref` varchar(100) DEFAULT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'draft',
  PRIMARY KEY (`id`),
  KEY `art_id` (`art_id`),
  KEY `tag_id` (`tag_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `relations`
--

INSERT INTO `relations` (`id`, `art_ref`, `art_id`, `tag_ref`, `tag_id`, `rel_ref`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, 'packages', 1, 'destinations', 1, NULL, '2021-07-13 14:49:36', '1', NULL, NULL, 0, 1, 'draft'),
(2, 'packages', 2, 'destinations', 1, NULL, '2021-07-13 14:54:03', '', NULL, NULL, 0, 3, 'draft'),
(3, 'packages', 3, 'destinations', 3, NULL, '2021-07-16 21:41:27', '1', NULL, NULL, 0, 4, 'draft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slider`
--

DROP TABLE IF EXISTS `slider`;
CREATE TABLE IF NOT EXISTS `slider` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `subtitle_2` varchar(100) DEFAULT NULL,
  `cta_text` varchar(50) DEFAULT NULL,
  `cta_link` varchar(200) DEFAULT NULL,
  `text` text,
  `vimeo_url` varchar(100) DEFAULT NULL,
  `youtube_url` varchar(100) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `slider`
--

INSERT INTO `slider` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `subtitle_2`, `cta_text`, `cta_link`, `text`, `vimeo_url`, `youtube_url`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, '3 noches en Bariloche<br>con desayuno y excursiones ', '3-noches-en-calafate-con-desayuno-y-excursiones-', 'Saludas grupales', 'Desde $ 25.000', 'Ver más', '/destinos/argentina', '', NULL, NULL, NULL, NULL, '2021-07-13 19:16:21', '1', '2021-07-13 23:15:30', '1', 0, 1, 'published'),
(2, NULL, 'Un título más corto', NULL, '', '', '', '', NULL, NULL, NULL, NULL, NULL, '2021-07-13 23:00:15', '1', '2021-07-14 12:51:15', '1', 0, 2, 'draft');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `static_pages`
--

DROP TABLE IF EXISTS `static_pages`;
CREATE TABLE IF NOT EXISTS `static_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `static_pages`
--

INSERT INTO `static_pages` (`id`, `parent_id`, `title`, `slug`, `subtitle`, `text`, `category`, `tags`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, 'Acerca de nosotros', 'acerca-de-nosotros', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, NULL, NULL, NULL, '2021-07-19 19:31:55', '1', '2021-07-19 19:52:20', '1', 0, 1, 'published'),
(2, NULL, 'Términos y condiciones', 'terminos-y-condiciones', NULL, '<p>Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique. Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.Lorem ipsum dolor sit amet, usu dicat nonumes percipitur at. Latine fabulas senserit ne ius, ex vero nostro percipitur usu. Ex quo ullum animal, ea est munere laoreet appellantur. Vis an omnium aliquip corpora, nam nihil iuvaret at. Mei ex posse aliquip. Verear timeam oporteat usu cu, no dico consul per, duo eu noster debitis praesent.</p>\r\n<p>Ei ornatus conclusionemque per, quis ludus reprimique cu est. Cu vis habeo summo persius. Nibh dictas eos cu, ius nihil splendide torquatos at, omnis legimus placerat ad his. Paulo nihil consequuntur an his. Ea volumus intellegat his, no has modo veri illum, cum alii nihil accusata an. Eam sonet nihil aliquid te, eos ne graeci saperet tibique.</p>', NULL, NULL, NULL, NULL, '2021-07-19 19:32:07', '1', '2021-07-19 19:35:15', '1', 0, 2, 'published');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `email` char(80) DEFAULT NULL,
  `password` char(64) DEFAULT NULL,
  `salt` char(16) DEFAULT NULL,
  `apellido` varchar(200) DEFAULT NULL,
  `nombre` varchar(200) DEFAULT NULL,
  `categorias` varchar(100) DEFAULT NULL,
  `telefono` char(20) DEFAULT NULL,
  `ciudad` varchar(200) DEFAULT NULL,
  `provincia` varchar(200) DEFAULT NULL,
  `pais` varchar(200) DEFAULT NULL,
  `documento_tipo` char(15) DEFAULT NULL,
  `documento_numero` char(15) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `parent_id`, `email`, `password`, `salt`, `apellido`, `nombre`, `categorias`, `telefono`, `ciudad`, `provincia`, `pais`, `documento_tipo`, `documento_numero`, `title`, `slug`, `subtitle`, `text`, `date_from`, `date_to`, `created_on`, `created_by`, `last_modified_on`, `last_modified_by`, `featured`, `pos`, `status`) VALUES
(1, NULL, NULL, '5e22e76b348392e3875fbea6395bf0461a4c639386d26cc436c08be521dbecf6', '24fe6f4571a300e7', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-11-19 22:56:27', '1', NULL, NULL, 0, 1, 'temp'),
(2, NULL, NULL, 'c9faa3262e4a6117248805828865b9a0fcbed8bc3f21b698a18bb77e2c3f6957', '52f2ddf857bc20d1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-01 01:14:13', '1', NULL, NULL, 0, 2, 'temp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `_base`
--

DROP TABLE IF EXISTS `_base`;
CREATE TABLE IF NOT EXISTS `_base` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `title` varchar(200) DEFAULT NULL,
  `slug` varchar(200) DEFAULT NULL,
  `subtitle` varchar(200) DEFAULT NULL,
  `text` text,
  `category` varchar(200) DEFAULT NULL,
  `tags` varchar(200) DEFAULT NULL,
  `date_from` timestamp NULL DEFAULT NULL,
  `date_to` timestamp NULL DEFAULT NULL,
  `created_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `created_by` char(70) DEFAULT NULL,
  `last_modified_on` timestamp NULL DEFAULT NULL,
  `last_modified_by` char(70) DEFAULT NULL,
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `pos` int(11) NOT NULL DEFAULT '0',
  `status` char(20) NOT NULL DEFAULT 'temp',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
